import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iexist/main.dart';
import 'package:iexist/src/common/color_constants.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/camera/capture_image.dart';
import 'package:iexist/src/screens/login/validateOTP.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:share_extend/share_extend.dart';
import 'package:iexist/src/screens/login/loginLanding.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:path_provider/path_provider.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:csv/csv.dart';
import 'package:path/path.dart' show join;
import 'package:toast/toast.dart';
import 'package:pedantic/pedantic.dart';
// import 'package:permission_handler/permission_handler.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();

  static Future<void> logOut() async {
    UserProvider userProvider = UserProvider();
    if (null != userProvider.token) {
      await userProvider.reset();

      unawaited(navigatorKey.currentState.pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginLanding()),
          (route) => false));
    }
  }
}

class _ProfileScreenState extends State<ProfileScreen> {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool picSaving = false;
  bool loading = false;
  String tempValue;
  final picker = FilePicker;

  _asyncMethod() async {}

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _asyncMethod();
    });
  }

  List<Widget> _buildProfileListTiles() {
    return <Widget>[
      ListTile(
        title: Text(
          "Name",
          // style: TextStyle(color: Colors.black54, fontSize: 13),
        ),
        subtitle: Text(
          _userProvider.userNameFullName ?? '',
          // style: TextStyle(color: Colors.black87),
        ),
        leading: CircleAvatar(
          backgroundColor: backgroundColor,
          child: Icon(
            Icons.person,
            color: Color(ColorConstants.PRIMARY_COLOR),
          ),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.edit,
            size: 18,
          ),
          onPressed: () {
            _editVal(context, 'name', _userProvider.userNameFullName);
          },
        ),
      ),
      ListTile(
        title: Text(
          "Phone",
          // style: TextStyle(color: Colors.black54, fontSize: 13),
        ),
        subtitle: Text(
          _userProvider.userPhone ?? '',
        ),
        leading: CircleAvatar(
          backgroundColor: backgroundColor,
          child: Icon(
            Icons.phone,
            color: Color(ColorConstants.PRIMARY_COLOR),
          ),
        ),
        // trailing: IconButton(
        //   icon: Icon(
        //     Icons.edit,
        //     size: 18,
        //   ),
        //   onPressed: () {
        //     _editVal(context, 'phone', _userProvider.userPhone);
        //   },
        // ),
      ),
      ListTile(
        title: Text(
          "Email",
          // style: TextStyle(color: Colors.black54, fontSize: 13),
        ),
        subtitle: Text(
          _userProvider.userEmail ?? '',
          // style: TextStyle(color: Colors.black87),
        ),
        leading: CircleAvatar(
          backgroundColor: backgroundColor,
          child: Icon(
            Icons.email,
            color: Color(ColorConstants.PRIMARY_COLOR),
          ),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.edit,
            size: 18,
          ),
          onPressed: () {
            _editVal(context, 'email', _userProvider.userEmail);
          },
        ),
      ),
      ListTile(
        title: Text(
          "Pincode",
        ),
        subtitle: Text(
          _userProvider.pincode ?? '',
        ),
        leading: CircleAvatar(
          backgroundColor: backgroundColor,
          child: Icon(
            Icons.location_on,
            color: Color(ColorConstants.PRIMARY_COLOR),
          ),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.edit,
            size: 18,
          ),
          onPressed: () {
            _editVal(context, 'pincode', _userProvider.pincode);
          },
        ),
      ),
      ListTile(
        title: Text(
          "Address",
        ),
        subtitle: Text(
          _userProvider.address ?? '',
        ),
        leading: CircleAvatar(
          backgroundColor: backgroundColor,
          child: Icon(
            Icons.home,
            color: Color(ColorConstants.PRIMARY_COLOR),
          ),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.edit,
            size: 18,
          ),
          onPressed: () {
            _editVal(context, 'address', _userProvider.address);
          },
        ),
      ),
      SwitchListTile(
        title: const Text('Auto Download Media'),
        value: _userProvider.autoDownload,
        onChanged: (bool value) {
          setState(() {
            _userProvider.setVal('autoDownload', value);
          });
        },
      ),
      SwitchListTile(
        title: const Text('Disable Notifications'),
        value: _userProvider.disableNotification,
        onChanged: (bool value) {
          setState(() {
            _userProvider.setVal('disableNotification', value);
          });
        },
      ),
    ];
  }

  Future getImage(pickedFile) async {
    if (pickedFile != null) {
      var imageFile = File(pickedFile);

      try {
        File croppedFile = await ImageCropper.cropImage(
            sourcePath: imageFile.path,
            aspectRatioPresets: [
              CropAspectRatioPreset.square,
              CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.original,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio16x9
            ],
            androidUiSettings: AndroidUiSettings(
                toolbarTitle: 'Crop Image',
                toolbarColor: primaryColor,
                toolbarWidgetColor: Colors.white,
                activeControlsWidgetColor: primaryColor,
                initAspectRatio: CropAspectRatioPreset.original,
                lockAspectRatio: false),
            iosUiSettings: IOSUiSettings(
              minimumAspectRatio: 1.0,
            ));

        if (croppedFile != null) {
          await saveProfilePic(croppedFile);
        }
      } catch (e) {
        PrintString("Crop error");
      }
    } else {
      print('No image selected.');
    }
  }

  Future saveProfilePic(File croppedFile) async {
    picSaving = true;
    try {
      Dio dio = Dio();
      dio.options.headers['content-Type'] = 'application/json';
      dio.options.headers["Authorization"] = _userProvider.token;

      var p = croppedFile.path.split('/');
      String fileName = p[p.length - 1];

      FormData formData = FormData.fromMap({
        'data': null,
        'file': await MultipartFile.fromFile(croppedFile.path,
            filename: '${DateTime.now().millisecondsSinceEpoch}_$fileName')
      });

      var resp = await dio.postExtended(
        "$profilePicUpdateApi/${_userProvider.userId}",
        data: formData,
      );

      Map respData = resp.data;

      if (respData.containsKey('success') && respData['success']) {
        await _userProvider.setPic(profilePic: respData['url']);
      } else {
        Toast.show(
          respData['message'],
          context,
          backgroundColor: Colors.red,
        );
      }
    } catch (e) {
      PrintString(e);
    }

    picSaving = false;
  }

  _imgFromCamera() async {
    final cameras = await availableCameras();

    final firstCamera = cameras.first;

    String imagePath = await Navigator.of(context).push(MaterialPageRoute(
        builder: (_) =>
            TakePictureScreen(camera: firstCamera, preview: false)));

    if (null != imagePath) {
      await getImage(imagePath);
    }
  }

  _imgFromGallery() async {
    String pickedFile = await FilePicker.getFilePath(type: FileType.image);
    await getImage(pickedFile);
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    List<Widget> profileTiles = _buildProfileListTiles();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("Profile"), actions: <Widget>[
        IconButton(
          icon: Icon(Icons.swap_horiz),
          onPressed: () {
            _switchOrganization();
          },
        ),
        PopupMenuButton<String>(
          onSelected: (value) {
            if (value == 'export_log') {
              _exportLogFile();
            } else if (value == 'logout') {
              ProfileScreen.logOut();
            }
          },
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem<String>(
                value: "export_log",
                child: Text("Share Log File"),
              ),
              PopupMenuItem<String>(
                value: "logout",
                child: Text("Logout"),
              ),
            ];
          },
        )
      ]),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Stack(fit: StackFit.loose, children: <Widget>[
            CircleAvatar(
              backgroundColor: Colors.grey[200],
              child: picSaving
                  ? CircularProgressIndicator()
                  : (_userProvider.profilePic == null
                      ? Icon(
                          Icons.person,
                          size: 144,
                          color: Colors.grey[300],
                        )
                      : Container(
                          decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: NetworkImage(
                              _userProvider.profilePic,
                            ),
                          ),
                        ))),
              radius: 86.0,
            ),
            Positioned(
              bottom: 0.0,
              right: 0.0,
              child: CircleAvatar(
                radius: 24,
                backgroundColor: primaryColor,
                child: IconButton(
                  icon: Icon(
                    Icons.camera_alt,
                    size: 24,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    _showPicker(context);
                  },
                ),
              ),
            ),
          ]),
          Expanded(
            child: ListView.separated(
              shrinkWrap: true,
              physics: const AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.only(right: 15.0, left: 15.0, top: 10.0),
              itemBuilder: (_, index) {
                return profileTiles[index];
              },
              itemCount: profileTiles.length,
              separatorBuilder: (_, __) => Divider(indent: 24),
            ),
          ),
        ],
      ),
    );
  }

  void _editVal(context, field, value) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: ListTile(
                        title: TextFormField(
                          autofocus: true,
                          keyboardType: field == 'pincode'
                              ? TextInputType.number
                              : TextInputType.text,
                          minLines: field == 'address' ? 3 : 1,
                          maxLines: field == 'address' ? 3 : 1,
                          initialValue: value,
                          validator: (value) {
                            if (['name', 'phone'].contains(field) &&
                                value.isEmpty) {
                              return 'Name is required';
                            } else if (field == 'phone' &&
                                !RegExp(r'^\d{10}$').hasMatch(value)) {
                              return 'Invalid Phone number';
                            } else if (field == 'email' &&
                                !RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                                    .hasMatch(value)) {
                              return 'Invalid email';
                            }
                            _formKey.currentState.save();

                            FocusScope.of(context).unfocus();
                            return null;
                          },
                          onSaved: (value) {
                            this.tempValue = value;
                          },
                        ),
                        trailing: loading
                            ? CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(primaryColor),
                              )
                            : IconButton(
                                color: primaryColor,
                                icon: Icon(
                                  Icons.check,
                                  size: 18,
                                  color: primaryColor,
                                ),
                                onPressed: () {
                                  _validate(field);
                                },
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  //  Used to render a bottom sheet
  void _switchOrganization() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return ListView.separated(
          shrinkWrap: true,
          itemCount: _userProvider.data['organizations'].length,
          itemBuilder: (context, index) {
            var org = _userProvider.data['organizations'][index];
            return ListTile(
              onTap: () {
                _setSelectedOrganization(org);
              },
              title: Text('${org['organization_name']}'),
              trailing: Visibility(
                visible: org['_id'].toString() ==
                    _appProvider.selectedOrganization['_id'],
                child: CircleAvatar(
                  radius: 6,
                ),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Divider();
          },
        );
      },
    );
  }

  //  Used to select an organization
  void _setSelectedOrganization(Map data) async {
    await _appProvider.setSelectedOrganization(data['_id']);
    // _userProvider.setFirstSyncStatus(false);
    _userProvider.setSyncLatestUpdatesClick(true);
    Navigator.pop(context);
  }

  void _exportLogFile() async {
    PermissionStatus permissionStatus = await Permission.storage.request();
    if (permissionStatus != PermissionStatus.granted) return;

    List logMessages = await DBHelper.instance.getLogMessages();
    List<List> csvLog = [
      ['Timestamp', 'Level', 'User ID', 'Org ID', 'Message']
    ];
    for (var row in logMessages) {
      csvLog.add([
        row['time_stamp'],
        row['level'],
        row['user_id'],
        row['org_id'],
        row['message'],
      ]);
    }

    // final status = await Permission
    final path = await getStoragePath() + '.csv';

    File file = File(path);

    String csv = const ListToCsvConverter().convert(csvLog);
    await file.writeAsString(csv);
    await ShareExtend.share(path, "file");
  }

  void _validate(field) async {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formKey.currentState.save();

      if (field == 'phone') {
        bool success = true;

        success = await checkOTP(success);

        if (!success) {
          setState(() {
            loading = false;
          });

          return;
        }
      }

      await _save(field);
    }
  }

  Future<bool> checkOTP(bool success) async {
    try {
      var body = {
        "user_cred": tempValue,
      };

      var resp = await Dio().postExtended(generateOTP, data: body);
      Map respData = resp.data;

      if (respData['success']) {
        bool valid = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ValidateOTP(
              phone: tempValue,
              isUpdating: true,
            ),
          ),
        );

        if (valid == null) {
          success = false;
        }
      } else {
        Toast.show(respData['message'], context, backgroundColor: Colors.red);
        success = false;
      }
    } catch (e) {
      Toast.show('Something went wrong...', context,
          backgroundColor: Colors.red);
      success = false;
    }
    return success;
  }

  _save(field) async {
    if (!mounted) return;
    setState(() {
      loading = true;
    });

    var body = _userProvider.getProfileBody();

    body[field] = tempValue;
    body['is_new_user'] = false;

    try {
      Dio dio = Dio();
      dio.options.headers['content-Type'] = 'application/json';
      dio.options.headers["Authorization"] = _userProvider.token;

      var resp = await dio.postExtended(createOrEditUser, data: body);
      Map respData = resp.data;
      PrintString(respData);
      if (!mounted) return;

      if (respData['code'] == 'SUC') {
        _userProvider.setVal(field, tempValue);
        Navigator.of(context).pop();
      } else {
        Toast.show(respData['message'], context, backgroundColor: Colors.red);
      }
    } catch (e) {
      if (!mounted) return;
      Toast.show('Something went wrong...', context,
          backgroundColor: Colors.red);
      PrintString(e);
    }
    setState(() {
      loading = false;
    });
  }
}
