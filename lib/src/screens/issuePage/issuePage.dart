import 'package:flutter/material.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/createNewIssueGroup.dart';
import 'package:iexist/src/screens/issuePage/issueTabScreen.dart';
import 'package:iexist/src/screens/issuePage/searchIssuePage.dart';
import 'package:iexist/src/syncFunction/latestDataSyncFunction.dart';
import 'package:iexist/src/utilities/user_permission_map_keys.dart';
import 'package:provider/provider.dart';
import 'package:synchronized/synchronized.dart' as synchronized;
import 'package:toast/toast.dart';

class IssuePage extends StatefulWidget {
  @override
  _IssuePageState createState() => _IssuePageState();
}

class _IssuePageState extends State<IssuePage> with TickerProviderStateMixin {
  final _dbHelper = DBHelper.instance;
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  ChatScreenProvider _chatProvider = ChatScreenProvider();
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  TabController _tabController;
  static final synchronized.Lock lock = synchronized.Lock();

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    _chatProvider = Provider.of<ChatScreenProvider>(context);
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);

    return DefaultTabController(
      length: _appProvider.selectedOrganizationTab.length,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0.0,
          title: Text("COLLABFLO"),
          actions: <Widget>[
            IconButton(
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchIssuePage()))
              },
              icon: Icon(Icons.search),
            ),
            IconButton(
              onPressed: () => {
                if (_appProvider.isTheCurrentUserPrivilegedFor(
                    UserPermissionMapKeys.canAddChatGroup, _userProvider.data))
                  {createIssue()}
                else
                  {
                    Toast.show(
                      'You do not have the privilege to create an issue',
                      context,
                      backgroundColor: Colors.red,
                    )
                  }
              },
              icon: Icon(Icons.chat),
            ),
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                _userProvider.setSyncLatestUpdatesClick(true);
              },
            ),
          ],
          bottom: TabBar(
            labelPadding: EdgeInsets.symmetric(
              horizontal: 50,
            ),
            controller: _tabController,
            isScrollable: true,
            indicatorColor: Colors.white,
            labelStyle:
                TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
            tabs: List.generate(
              _appProvider.selectedOrganizationTab.length,
              (index) => Tab(
                child: Text(
                  '${_appProvider.selectedOrganizationTab[index]['name']}',
                ),
              ),
            ),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: TabBarView(
                children: List.generate(
                  _appProvider.selectedOrganizationTab.length,
                  (index) => IssueTabScreen(
                    groupList: _appProvider.issueGroupList,
                    status:
                        '${_appProvider.selectedOrganizationTab[index]['name']}',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _getGroupChatsFromLocalDb();
      LatestDataSyncFunction(_appProvider, _userProvider, _chatProvider);
    });
  }

  void _getGroupChatsFromLocalDb() async {
    await _appProvider.updateIssueGroupList();
  }

  createIssue() {
    _newIssuePro.clearAllData(_appProvider);
    _newIssuePro.init(_appProvider, _userProvider);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CreateNewIssueGroup()),
    );
  }
}
