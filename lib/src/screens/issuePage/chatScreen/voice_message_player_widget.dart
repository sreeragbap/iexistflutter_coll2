import 'package:flutter/material.dart';
import 'package:iexist/src/utilities/colors.dart';

import 'voice_message_player.dart';

class VoiceMessagePlayerWidget extends StatefulWidget {
  final String url;
  VoiceMessagePlayerWidget({Key key, this.url}) : super(key: key);

  @override
  _VoiceMessagePlayerWidgetState createState() =>
      _VoiceMessagePlayerWidgetState();
}

class _VoiceMessagePlayerWidgetState extends State<VoiceMessagePlayerWidget> {
  VoiceMessagePlayer _voiceMessagePlayer = VoiceMessagePlayer();

  bool get _isPlaying {
    return _voiceMessagePlayer.url == widget.url &&
        _voiceMessagePlayer.isPlaying;
  }

  bool get _isPaused {
    return _voiceMessagePlayer.url == widget.url &&
        _voiceMessagePlayer.isPaused;
  }

  Duration get _duration {
    if (_voiceMessagePlayer.url == widget.url)
      return _voiceMessagePlayer.duration;

    return Duration();
  }

  Duration get _position {
    if (_voiceMessagePlayer.url == widget.url)
      return _voiceMessagePlayer.position;

    return Duration();
  }

  get _durationText => _duration?.toString()?.split('.')?.first ?? '';
  get _positionText => _position?.toString()?.split('.')?.first ?? '';

  @override
  void initState() {
    VoiceMessagePlayer().addListener(() {
      if (mounted) {
        setState(() {});
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (_isPaused || !_isPlaying)
              IconButton(
                padding: EdgeInsets.all(2.0),
                key: Key('play_button'),
                onPressed: _isPlaying
                    ? null
                    : () => _voiceMessagePlayer.play(widget.url),
                iconSize: 36.0,
                icon: Icon(Icons.play_arrow),
                color: theme.disabledColor,
              ),
            if (_isPlaying)
              IconButton(
                padding: EdgeInsets.all(2.0),
                key: Key('pause_button'),
                onPressed:
                    _isPlaying ? () => _voiceMessagePlayer.pause() : null,
                iconSize: 36.0,
                icon: Icon(Icons.pause),
                color: theme.disabledColor,
              ),
            SizedBox(
                width: 160,
                child: SliderTheme(
                  data: SliderTheme.of(context).copyWith(
                      trackShape: RoundedRectSliderTrackShape(),
                      trackHeight: 2.0,
                      thumbShape: RoundSliderThumbShape(
                          disabledThumbRadius: 6.0, enabledThumbRadius: 6.0)),
                  child: Slider(
                    activeColor: buttonColor,
                    inactiveColor: Theme.of(context).disabledColor,
                    onChanged: (v) {
                      _voiceMessagePlayer.seek(widget.url, v);
                    },
                    value: (_position != null &&
                            _duration != null &&
                            _position.inMilliseconds > 0 &&
                            _position.inMilliseconds < _duration.inMilliseconds)
                        ? _position.inMilliseconds / _duration.inMilliseconds
                        : 0.0,
                  ),
                )),
          ],
        ),
        if (_isPlaying || _isPaused)
          Text(
            _position != null
                ? '${_positionText ?? ''} / ${_durationText ?? ''}'
                : _duration != null ? _durationText : '',
            style: TextStyle(fontSize: 10.0),
          ),
        // Text('State: $_audioPlayerState'),
        // Text(widget.url)
      ],
    );
  }
}
