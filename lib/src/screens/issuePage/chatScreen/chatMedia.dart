import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'chatStatus.dart';

// Chat Image and Text chat type
abstract class ChatMedia extends StatefulWidget {
  final Map message;
  final String mediaType;

  const ChatMedia({Key key, @required this.message, @required this.mediaType})
      : super(key: key);

  @override
  _ChatMediaState createState() => _ChatMediaState();

  Widget getLocalMediaRenderer(
      BuildContext context, String localMediaPath, isCurrentUser);
  Widget getRemoteMediaRenderer(BuildContext context, String remoteMediaPath);
  VoidCallback getLocalMediaOnTap(BuildContext context, String localMediaPath);
  VoidCallback getRemoteMediaOnTap(
      BuildContext context, String remoteMediaPath);
  double get mediaContainerHeight;
  double get mediaContainerWidth;
}

class _ChatMediaState extends State<ChatMedia> {
  ChatScreenProvider _chatScreenProvider = ChatScreenProvider();
  // AppProvider _appProvider = AppProvider();
  UserProvider _userProvider = UserProvider();
  // bool _loading = true;
  bool _isPending = false;
  bool _isDownloading = false;

  @override
  Widget build(BuildContext context) {
    // _appProvider = Provider.of<AppProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _chatScreenProvider = Provider.of<ChatScreenProvider>(context);
    _isPending =
        widget.message['sync_status'].toString().toLowerCase() != 'complete'
            ? true
            : false;

    bool isCurrentUser = _isCurrentUser();
    return Container(
      color: _chatScreenProvider.isMessageSelected(widget.message)
          ? selectionColor
          : Colors.transparent,
      alignment: isCurrentUser ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        margin: EdgeInsets.only(
            right: isCurrentUser ? 0 : 100,
            left: isCurrentUser ? 100 : 0,
            bottom: 8),
        child: Bubble(
          // padding: BubbleEdges.all(3),
          nip: isCurrentUser ? BubbleNip.rightTop : BubbleNip.leftTop,
          stick: isCurrentUser ? false : true,
          color: Color.fromRGBO(225, 255, 199, 1.0),
          child: Column(
            crossAxisAlignment: isCurrentUser
                ? CrossAxisAlignment.end
                : CrossAxisAlignment.start,
            children: <Widget>[
              if (!isCurrentUser)
                Padding(
                  padding: EdgeInsets.only(bottom: 3),
                  child: Text(
                    '${widget.message['user_fullname']}',
                    style: TextStyle(
                      color: primaryColor,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              widget.message['local_path'] != null
                  ? InkWell(
                      onTap: widget.getLocalMediaOnTap(
                          context, widget.message['local_path']),
                      child: Container(
                        height: widget.mediaContainerHeight,
                        width: widget.mediaContainerWidth,
                        child: Stack(
                          children: <Widget>[
                            widget.getLocalMediaRenderer(context,
                                widget.message['local_path'], isCurrentUser),
                            if (_isPending)
                              Container(
                                color: Colors.black38,
                              ),
                            if (_isPending)
                              Center(
                                  child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.white,
                                ),
                              ))
                          ],
                        ),
                      ),
                    )
                  : InkWell(
                      // Used to identify user events
                      onTap: widget.getRemoteMediaOnTap(
                          context, widget.message['file']),
                      child: Container(
                        height: widget.mediaContainerHeight,
                        width: widget.mediaContainerWidth,
                        child: Stack(
                          children: <Widget>[
                            widget.getRemoteMediaRenderer(
                                context, widget.message['file']),
                            if (_isDownloading ||
                                widget.message['local_path'] == null)
                              Container(
                                color: Colors.black38,
                              ),
                            Center(
                              child: _isDownloading
                                  ? CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white,
                                      ),
                                    )
                                  : Visibility(
                                      visible:
                                          widget.message['local_path'] == null,
                                      child: FlatButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        color: Colors.white,
                                        onPressed: () {
                                          _downloadImage();
                                        },
                                        child: Text('Download'),
                                      ),
                                    ),
                            )
                          ],
                        ),
                      ),
                    ),
              ChatStatus(
                message: widget.message,
                alignRight: isCurrentUser,
                showStatus: isCurrentUser,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      PrintString(
          'added - ${widget.message['sync_status'].toString().toLowerCase()}');
      PrintString(widget.message);
      fileCheck();
    });
  }

  Future fileCheck() async {
    if (widget.message['local_path'] == null) return;

    bool fileExists = await File(widget.message['local_path']).exists();
    if (!fileExists) {
      _updateDownloadedImagePathMessage(widget.message, null);
    }
  }

  _isCurrentUser() {
    return widget.message['user_id'] == _userProvider.userId ? true : false;
  }

  _downloadImage() async {
    if (!mounted) return;
    setState(() {
      _isDownloading = true;
    });
    try {
      String remotePath = widget.message['file'].toString();
      var path = await download(remotePath, widget.message['type']);

      if (widget.message['file'] == 'VIDEO') {
        await setThumbnailWithFile(widget.message['_id'], path);
      }

      _updateDownloadedImagePathMessage(widget.message, path);
      if (!mounted) return;
      setState(() {
        _isDownloading = false;
      });
    } catch (e) {
      if (!mounted) return;
      setState(() {
        _isDownloading = false;
      });
      PrintString(e);
    }
  }

  // Used to update downloaded message's URL with local path so as to view the image offline
  _updateDownloadedImagePathMessage(data, String path) async {
    DBHelper dbHelper = DBHelper.instance;
    await dbHelper.updateChatMessage(
      oldId: data['_id'],
      newMessageId: data['_id'],
      file: data['file'],
      type: widget.mediaType,
      userId: data['user_id'],
      userName: data['username'],
      userFullName: data['user_fullname'],
      chatGroupId: data['chat_group_id'],
      text: null,
      syncStatus: 'complete',
      localPath: path,
    );

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString(data['_id'], path);

    await getChatHistory();
  }

  // Used to get all chats in the DB
  getChatHistory() async {
    DBHelper _dbHelper = DBHelper.instance;
    List chatList = await _dbHelper
        .getChatMessageByGroupIdData('${widget.message['chat_group_id']}');
    PrintString(chatList);
    _chatScreenProvider.setChatMessageList(chatList);
  }
}
