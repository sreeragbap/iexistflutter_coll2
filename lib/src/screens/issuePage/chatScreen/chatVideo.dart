import 'dart:io';

import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/playVideo.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/utilities/shared.dart';

import 'chatMedia.dart';

class ChatVideo extends ChatMedia {
  const ChatVideo({Key key, Map message})
      : super(key: key, message: message, mediaType: "VIDEO");

  @override
  Widget getLocalMediaRenderer(
      BuildContext context, String localMediaPath, isCurrentUser) {
    return Stack(children: [
      Image.file(
        File(getThumbnailWithFile(localMediaPath)),
        fit: BoxFit.cover,
        height: mediaContainerHeight,
        width: mediaContainerWidth,
      ),
      Container(
        alignment: Alignment.center,
        child: FlatButton(
          shape: CircleBorder(),
          color: Colors.black,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
            child: Icon(
              Icons.play_arrow,
              color: Colors.white,
            ),
          ),
        ),
      ),
    ]);
  }

  getThumbnailWithFile(String path) {
    var key = message['_id'] + "_thumbnail";
    var sharedPreferences = AppProvider().sharedPreferences;

    if (sharedPreferences != null && sharedPreferences.containsKey(key)) {
      return sharedPreferences.getString(key);
    } else {
      setThumbnailWithFile(message['_id'], path);
      return '';
    }
  }

  @override
  Widget getRemoteMediaRenderer(BuildContext context, String remoteMediaPath) {
    return Center(
        child: Icon(
      Icons.play_arrow,
      color: Colors.white,
    ));
  }

  @override
  VoidCallback getLocalMediaOnTap(BuildContext context, String localMediaPath) {
    return () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PlayVideo(
            videoPath: localMediaPath,
            play: true,
          ),
        ),
      );
    };
  }

  @override
  VoidCallback getRemoteMediaOnTap(
      BuildContext context, String remoteMediaPath) {
    return () {
      PrintString(remoteMediaPath);
    };
  }

  @override
  double get mediaContainerHeight => 200;

  @override
  double get mediaContainerWidth => 250;
}
