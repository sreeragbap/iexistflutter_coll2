import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/voice_message_player_widget.dart';

import 'chatMedia.dart';

// Chat Image and Text chat type
class ChatAudio extends ChatMedia {
  const ChatAudio({Key key, Map message})
      : super(key: key, message: message, mediaType: "AUDIO");

  @override
  Widget getLocalMediaRenderer(BuildContext context, String localMediaPath, isCurrentUser) {
    return Container(
      height: mediaContainerHeight,
      width: mediaContainerWidth,
      margin: EdgeInsets.all(2),
      child: Container(
        color: Colors.black.withOpacity(0.05),
        child: VoiceMessagePlayerWidget(url: localMediaPath),
      ),
    );
  }

  @override
  Widget getRemoteMediaRenderer(BuildContext context, String remoteMediaPath) {
    return Container(
      height: mediaContainerHeight,
      width: mediaContainerWidth,
      // child: PlayerWidget(url: remoteMediaPath, mode: PlayerMode.LOW_LATENCY),
    );
  }

  @override
  VoidCallback getLocalMediaOnTap(BuildContext context, String localMediaPath) {
    return () {};
  }

  @override
  VoidCallback getRemoteMediaOnTap(
      BuildContext context, String remoteMediaPath) {
    return () {};
  }

  @override
  double get mediaContainerHeight => 65;

  @override
  double get mediaContainerWidth => 250;
}
