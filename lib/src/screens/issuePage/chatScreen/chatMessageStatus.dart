import 'package:flutter/material.dart';
import 'package:iexist/src/common/color_constants.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ChatMessageStatus extends StatefulWidget {
  final Map message;

  const ChatMessageStatus({this.message});

  @override
  _ChatMessageStatusState createState() => _ChatMessageStatusState();
}

class _ChatMessageStatusState extends State<ChatMessageStatus>
    with SingleTickerProviderStateMixin {
  ChatScreenProvider _chatScreenProvider = ChatScreenProvider();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    _chatScreenProvider = Provider.of<ChatScreenProvider>(context);
    // List status = await _chatScreenProvider.getStatus(message);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Read Status'),
      ),
      body: FutureBuilder(
        future: _chatScreenProvider.getStatus(widget.message),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(child: CircularProgressIndicator());
          } else {
            List status = snapshot.data;

            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Container(
                      color: Color(0xffF2F4F4),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                              left: 0,
                              top: 10,
                            ),
                            child: Card(
                              child: Column(
                                children: [
                                  ListTile(
                                    title: Text(
                                      "Read by",
                                      style: TextStyle(
                                        color: primaryColor,
                                      ),
                                    ),
                                  ),
                                  ...status
                                      .where((element) =>
                                          element['read_time'] != null)
                                      .map((stat) {
                                    return ListTile(
                                      leading: CircleAvatar(
                                        backgroundColor: backgroundColor,
                                        child: Icon(
                                          Icons.person,
                                          color: Color(
                                              ColorConstants.PRIMARY_COLOR),
                                        ),
                                      ),
                                      title: Text(
                                        stat['user_full_name'],
                                        style: TextStyle(
                                          color: Colors.black,
                                        ),
                                      ),
                                      subtitle: Text(
                                        DateFormat.MMMd().add_jm().format(
                                            DateTime.parse(stat['read_time'])
                                                .toLocal()),
                                        style: TextStyle(),
                                      ),
                                    );
                                  }),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: 0,
                              top: 10,
                            ),
                          //   child: Card(
                          //     child: Column(
                          //       children: [
                          //         ListTile(
                          //           title: Text(
                          //             "Delivered to",
                          //             style: TextStyle(
                          //               color: primaryColor,
                          //             ),
                          //           ),
                          //         ),
                          //         ...status
                          //             .where((element) =>
                          //                 element['sent_time'] != null)
                          //             .map((stat) {
                          //           return ListTile(
                          //             leading: CircleAvatar(
                          //               backgroundColor: backgroundColor,
                          //               child: Icon(
                          //                 Icons.person,
                          //                 color: Color(
                          //                     ColorConstants.PRIMARY_COLOR),
                          //               ),
                          //             ),
                          //             title: Text(
                          //               stat['user_full_name'],
                          //               style: TextStyle(
                          //                 color: Colors.black,
                          //               ),
                          //             ),
                          //             subtitle: Text(
                          //               DateFormat.MMMd().add_jm().format(
                          //                   DateTime.parse(stat['sent_time'])
                          //                       .toLocal()),
                          //               style: TextStyle(),
                          //             ),
                          //           );
                          //         }),
                          //       ],
                          //     ),
                          //   ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
      // ),
    );
  }
}
