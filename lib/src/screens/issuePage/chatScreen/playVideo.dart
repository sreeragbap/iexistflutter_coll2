import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

// Used to display the video inf ullscreen
class PlayVideo extends StatefulWidget {
  final videoPath;
  final bool preview;
  final bool remote;
  final bool play;

  const PlayVideo(
      {Key key,
      this.videoPath,
      this.remote = false,
      this.preview = false,
      this.play = false})
      : super(key: key);

  @override
  _PlayVideoState createState() => _PlayVideoState();
}

class _PlayVideoState extends State<PlayVideo> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  var icon = Icons.play_arrow;

  @override
  void initState() {
    // Create and store the VideoPlayerController. The VideoPlayerController
    // offers several different constructors to play videos from assets, files,
    // or the internet.
    _controller = widget.remote
        ? VideoPlayerController.network(widget.videoPath)
        : VideoPlayerController.file(File(widget.videoPath));
    // Initialize the controller and store the Future for later use.
    _initializeVideoPlayerFuture = _controller.initialize();
    // Use the controller to loop the video.
    _controller.setLooping(false);

    _controller.addListener(checkVideo);

    if (widget.play) {
      _controller.play();
      icon = Icons.pause;
    }
    super.initState();
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    _controller.dispose();
    super.dispose();
  }

  void checkVideo() async {
    if (_controller.value.position ==
        Duration(seconds: 0, minutes: 0, hours: 0)) {
      print('video Started');
    }

    if (_controller.value.position == _controller.value.duration) {
      print('video Ended');

      await _controller.seekTo(Duration.zero);
      await _controller.pause();
      icon = Icons.play_arrow;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Container(
                    color: Colors.black,
                    alignment: Alignment.center,
                    child: AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        child: Container(
                            alignment: Alignment.center,
                            child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  VideoPlayer(_controller),
                                  VideoProgressIndicator(_controller,
                                      allowScrubbing: true),
                                ]))));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
          Container(
            alignment: Alignment.center,
            child: !widget.preview
                ? FlatButton(
                    shape: CircleBorder(),
                    color: Colors.black.withOpacity(0.05),
                    onPressed: () async {
                      if (_controller.value.isPlaying) {
                        await _controller.pause();
                        icon = Icons.play_arrow;
                        setState(() {});
                      } else {
                        await _controller.play();
                        icon = Icons.pause;
                        setState(() {});
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 20),
                      child: Icon(
                        icon,
                        color: Colors.white,
                      ),
                    ),
                  )
                : Icon(
                    Icons.play_arrow,
                    color: Colors.white,
                  ),
          ),
        ],
      ),
    );
  }
}
