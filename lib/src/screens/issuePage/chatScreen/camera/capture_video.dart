import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/playVideo.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' show join;
import 'package:provider/provider.dart';
import 'package:synchronized/synchronized.dart' as synchronized;
import 'package:video_player/video_player.dart';

class CaptureVideoScreen extends StatefulWidget {
  final CameraDescription camera;

  const CaptureVideoScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  CaptureVideoScreenState createState() => CaptureVideoScreenState();
}

class CaptureVideoScreenState extends State<CaptureVideoScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  AppProvider _appProvider = AppProvider();
  synchronized.Lock _lock = synchronized.Lock();
  String videoPath;
  VideoPlayerController videoController;
  VoidCallback videoPlayerListener;
  bool enableAudio = true;
  String imagePath;
  int videoStart;
  double maxVideo;

  @override
  void initState() {
    super.initState();

    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _appProvider = Provider.of<AppProvider>(context);
    maxVideo = _appProvider.config["video_max_sec"];

    return Scaffold(
      // Wait until the controller is initialized before displaying the
      // camera preview. Use a FutureBuilder to display a loading spinner
      // until the controller has finished initializing.
      body: Stack(
        children: [
          FutureBuilder<void>(
            future: _initializeControllerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(child: CircularProgressIndicator());
              } else {
                return CameraPreview(_controller);
              }
            },
          ),
          Visibility(
            visible: _controller.value.isRecordingVideo,
            child: Container(
              margin:
                  EdgeInsets.only(top: MediaQuery.of(context).padding.top + 10),
              alignment: Alignment.topCenter,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      child: CircleAvatar(
                        radius: 5.0,
                        backgroundColor: Colors.red,
                      ),
                    ),
                    Text(
                      _getTimer(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          backgroundColor: Theme.of(context).disabledColor),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).padding.bottom + 50),
            child: Visibility(
              visible: !_controller.value.isRecordingVideo ||
                  !_controller.value.isTakingPicture,
              child: FlatButton(
                shape: CircleBorder(),
                color: primaryColor,
                onPressed: () async {
                  await _takeVideo(context);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 20),
                  child: Icon(Icons.videocam,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).padding.bottom + 50),
            child: Visibility(
              visible: _controller.value.isRecordingVideo,
              child: FlatButton(
                shape: CircleBorder(),
                color: Colors.white,
                onPressed: () async {
                  await stopVideoRecording();
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 20),
                  child: Icon(
                    Icons.stop,
                    color: Colors.red,
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: !_controller.value.isRecordingVideo,
            child: Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: BackButton(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  String _getTimer() {
    if (videoStart == null) {
      return "";
    }

    var txt = "${videoStart ~/ 60}:${videoStart % 60}";

    if (maxVideo != null) {
      txt += " / ${maxVideo ~/ 60}:${maxVideo.round() % 60} m";
    }

    return txt;
  }

  Future _takeVideo(BuildContext context) async {
    return onVideoRecordButtonPressed();
  }

  Future _startTimer() async {
    videoStart = 0;

    Timer.periodic(Duration(seconds: 1), (timer) {
      if (_controller.value.isRecordingVideo) {
        setState(() {
          videoStart += 1;
        });
        if (maxVideo != null && videoStart >= maxVideo) {
          stopVideoRecording();
        }
      } else {
        timer.cancel();
      }
    });

    return true;
  }

  void onVideoRecordButtonPressed() {
    startVideoRecording().then((String filePath) {
      if (mounted) setState(() {});
      _startTimer();
    });
  }

  Future<String> startVideoRecording() async {
    videoPath = '${await getStoragePath()}.mp4';

    setState(() {
      videoPath = videoPath;
    });
    try {
      await _controller.startVideoRecording(videoPath);
    } on CameraException catch (e) {
      return null;
    }
    return videoPath;
  }

  Future<void> stopVideoRecording() async {
    if (!_controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await _controller.stopVideoRecording();
    } on CameraException catch (e) {
      return null;
    }

    await _startVideoPlayer();
  }

  Future<void> _startVideoPlayer() async {
    final VideoPlayerController vcontroller =
        VideoPlayerController.file(File(videoPath));
    videoPlayerListener = () {
      if (videoController != null && videoController.value.size != null) {
        // Refreshing the state to update video player with the correct ratio.
        if (mounted) setState(() {});
        videoController.removeListener(videoPlayerListener);
      }
    };
    vcontroller.addListener(videoPlayerListener);
    // await vcontroller.setLooping(true);
    await vcontroller.initialize();
    await videoController?.dispose();
    if (mounted) {
      setState(() {
        imagePath = null;
        videoController = vcontroller;
      });
    }

    // If the picture was taken, display it on a new screen.
    var selectedPath = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DisplayVideoScreen(videoPath: videoPath),
      ),
    );

    if (null != selectedPath) {
      Navigator.of(context).pop(selectedPath);
    }
  }
}


// A widget that displays the picture taken by the user.
class DisplayVideoScreen extends StatelessWidget {
  final String videoPath;

  const DisplayVideoScreen({Key key, this.videoPath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      backgroundColor: Colors.black,
      body: Stack(children: [
        Center(
            child: PlayVideo(
          videoPath: videoPath,
        )),
        Container(
          margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: BackButton(color: Colors.white),
        )
      ]),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).pop(videoPath);
          },
          backgroundColor: primaryColor,
          child: Icon(
            Icons.check,
            color: Colors.white,
          )),
    );
  }
}
