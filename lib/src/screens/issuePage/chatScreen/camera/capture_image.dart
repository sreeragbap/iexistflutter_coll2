import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' show join;
import 'package:provider/provider.dart';
import 'package:synchronized/synchronized.dart' as synchronized;

class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;
  final bool preview;

  const TakePictureScreen({
    Key key,
    this.preview = true,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  AppProvider _appProvider = AppProvider();
  synchronized.Lock _lock = synchronized.Lock();
  bool enableAudio = true;
  String imagePath;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.max,
    );

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _appProvider = Provider.of<AppProvider>(context);

    return Scaffold(
      body: Stack(
        children: [
          FutureBuilder<void>(
            future: _initializeControllerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(child: CircularProgressIndicator());
              } else {
                return CameraPreview(_controller);
              }
            },
          ),
          Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).padding.bottom + 50),
            child: Visibility(
              visible: !_controller.value.isRecordingVideo ||
                  !_controller.value.isTakingPicture,
              child: FlatButton(
                shape: CircleBorder(),
                color: primaryColor,
                onPressed: () async {
                  await _takePicture(context);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 20),
                  child: Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: BackButton(color: Colors.white),
          ),
        ],
      ),
    );
  }

  Future _takePicture(BuildContext context) async {
    await _lock.synchronized(() async {
      setState(() {});
      try {
        // Ensure that the camera is initialized.
        await _initializeControllerFuture;

        // Construct the path where the image should be saved using the
        // pattern package.
        final path = join(
          // Store the picture in the temp directory.
          // Find the temp directory using the `path_provider` plugin.
          (await getTemporaryDirectory()).path,
          '${DateTime.now()}.jpg',
        );

        // Attempt to take a picture and log where it's been saved.
        await _controller.takePicture(path);

        var selectedPath = path;

        if (widget.preview) {
          selectedPath = await cropImage(path);
        }
        if (null != selectedPath) {
          Navigator.of(context).pop(selectedPath);
        }
      } catch (e) {
        print(e);
      }
    });

    setState(() {});
  }

  Future<String> cropImage(pickedFile) async {
    if (pickedFile != null) {
      var imageFile = File(pickedFile);

      try {
        File croppedFile = await ImageCropper.cropImage(
            sourcePath: imageFile.path,
            aspectRatioPresets: [
              CropAspectRatioPreset.square,
              CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.original,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio16x9
            ],
            androidUiSettings: AndroidUiSettings(
                toolbarTitle: 'Crop Image',
                toolbarColor: primaryColor,
                toolbarWidgetColor: Colors.white,
                activeControlsWidgetColor: primaryColor,
                initAspectRatio: CropAspectRatioPreset.original,
                lockAspectRatio: false),
            iosUiSettings: IOSUiSettings(
              minimumAspectRatio: 1.0,
            ));

        return croppedFile.path;
      } catch (e) {
        return null;
      }
    } else {
      return null;
    }
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      backgroundColor: Colors.black,
      body: Stack(children: [
        Center(child: Image.file(File(imagePath))),
        Container(
          margin: new EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: new BackButton(color: Colors.white),
        )
      ]),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).pop(imagePath);
          },
          backgroundColor: primaryColor,
          child: Icon(
            Icons.check,
            color: Colors.white,
          )),
    );
  }
}
