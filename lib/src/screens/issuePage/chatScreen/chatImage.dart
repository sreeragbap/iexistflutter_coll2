import 'dart:io';

import 'package:dio/dio.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/viewImage.dart';
import 'package:iexist/src/control/printString.dart';

import 'chatMedia.dart';

class ChatImage extends ChatMedia {
  const ChatImage({Key key, Map message})
      : super(key: key, message: message, mediaType: "IMAGE");

  @override
  Widget getLocalMediaRenderer(BuildContext context, String localMediaPath, isCurrentUser) {
    return Image.file(
      File(localMediaPath),
      fit: BoxFit.cover,
      height: mediaContainerHeight,
      width: mediaContainerWidth,
    );
  }

  @override
  Widget getRemoteMediaRenderer(BuildContext context, String remoteMediaPath) {
    return CachedNetworkImage(
      imageUrl: remoteMediaPath,
      fit: BoxFit.cover,
      height: mediaContainerHeight,
      width: mediaContainerWidth,
    );
  }

  @override
  VoidCallback getLocalMediaOnTap(BuildContext context, String localMediaPath) {
    return () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ViewImage(
            imagePath: localMediaPath,
          ),
        ),
      );
    };
  }

  @override
  VoidCallback getRemoteMediaOnTap(
      BuildContext context, String remoteMediaPath) {
    return () {
      PrintString(remoteMediaPath);
    };
  }

  @override
  double get mediaContainerHeight => 200;

  @override
  double get mediaContainerWidth => 250;
}
