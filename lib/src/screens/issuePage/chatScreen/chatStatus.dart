import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ChatStatus extends StatelessWidget {
  final Map message;
  final bool alignRight;
  final bool showStatus;
  const ChatStatus(
      {Key key, this.message, this.alignRight = false, this.showStatus = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 6.0),
      child: Container(
        width: 90,
        child: Row(
          mainAxisAlignment:
              alignRight ? MainAxisAlignment.end : MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 2.0),
                child: Text(_getMessageTimestamp(),
                    style: TextStyle(
                      color: Theme.of(context).disabledColor,
                      fontSize: 8,
                    )),
              ),
            ),
            if (showStatus)
              Icon(
                message['sync_status'] == 'complete'
                    ? Icons.done
                    : Icons.schedule,
                size: 10.0,
                color: Theme.of(context).disabledColor,
              ),
          ],
        ),
      ),
    );
  }

  String _getMessageTimestamp() {
    try {
      DateTime date =
          DateTime.fromMillisecondsSinceEpoch(message['time_stamp'] * 1000);

      String formattedDate = DateFormat.MMMd().add_jm().format(date);

      if (date.day == DateTime.now().day &&
          date.month == DateTime.now().month) {
        formattedDate = DateFormat.jm().format(date);
      }

      return formattedDate;
    } catch (e) {
      return message['time_stamp']?.toString() ?? "null";
    }
  }
}
