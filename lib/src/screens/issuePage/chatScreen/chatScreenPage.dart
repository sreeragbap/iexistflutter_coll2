import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:bubble/bubble.dart';
import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/camera/capture_video.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatAudio.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatDocument.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatImage.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatMessageStatus.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatNotification.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatVideo.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/syncGroupInChatScreenIndicator.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/voice_message_player.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/createNewIssueGroup.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:iexist/src/utilities/user_permission_map_keys.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:synchronized/synchronized.dart' as synchronized;
import 'package:pedantic/pedantic.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:toast/toast.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'package:iexist/src/utilities/extensions.dart';
import 'camera/capture_image.dart';
import 'chatStatus.dart';

enum TapState { Up, Down }

class ChatScreenPage extends StatefulWidget {
  final groupDetails;

  const ChatScreenPage({Key key, this.groupDetails}) : super(key: key);

  @override
  _ChatScreenPageState createState() => _ChatScreenPageState();
}

class _ChatScreenPageState extends State<ChatScreenPage>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scfKey = GlobalKey<ScaffoldState>();
  DBHelper _dbHelper = DBHelper.instance;
  AppProvider _appProvider = AppProvider();
  UserProvider _userProvider = UserProvider();
  ChatScreenProvider _chatScreenProvider = ChatScreenProvider();
  String _messageText = '';
  TextEditingController _messageTextCtrl = TextEditingController();
  FlutterAudioRecorder _recorder;
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  synchronized.Lock _voiceRecordingLock = synchronized.Lock();
  static final synchronized.Lock lock = synchronized.Lock();

  AnimationController _animationController;
  Animation _animationCurve;
  Animation<double> _widthAnimation;
  int _recordingDuration = 1;
  Timer _recordingTimer;
  int voiceRecordTapDownTimeStamp = 0;
  TapState _tapState = TapState.Up;
  bool _chatButtonsVisible = true;
  bool readOnly = true;
  double _chatButtonOpacity = 1.0;
  static List _currentChatIds = [];
  double maxVoice;
  bool searching = false;
  int searchIndex = -1;
  String query = '';
  AutoScrollController autoScrollController;

  @override
  Widget build(BuildContext context) {
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _chatScreenProvider = Provider.of<ChatScreenProvider>(context);

    readOnly = _chatScreenProvider.isReadOnly(_appProvider.config);

    maxVoice = _appProvider.config["audio_max_sec"];
    // _chatScreenProvider.addListener(() {
    //   // getGroupMembers();
    //   // getChatHistory();
    // });

    return WillPopScope(
      onWillPop: () async {
        if (searching) {
          resetSearch();
          return false;
        } else if (_chatScreenProvider.selectedMessage != null) {
          _chatScreenProvider.unSelectedMessage();
          return false;
        } else {
          await VoiceMessagePlayer().stop();
          return true;
        }
      },
      child: Scaffold(
        key: _scfKey,
        backgroundColor: Color.fromRGBO(236, 229, 221, 1.0),
        appBar: appBar(),
        body: Column(
          children: <Widget>[
            if (_appProvider.isOnline &&
                widget.groupDetails['groupId'].toString().length < 24)
              SyncGroupInChatScreenIndicator(
                groupDetails: _chatScreenProvider.groupDetails,
              ),
            Expanded(
              child: buildListView(),
            ),

            //============================ bottom bar ===========================

            Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom),
              child: _buildChatBottomBar(),
            )
          ],
        ),
      ),
    );
  }

  ListView buildListView() {
    return ListView.separated(
      controller: autoScrollController,
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 8),
      itemCount: _chatScreenProvider.chatMessage.length,
      reverse: true,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        Map message = _chatScreenProvider.chatMessage[index];
        var key = ObjectKey(message['_id']);
        String chatType = message['type'].toString().toLowerCase();
        return _wrapScrollTag(
            index: index, child: getRow(chatType, key, message, index));
      },
      separatorBuilder: (context, index) {
        return SizedBox(height: 0);
      },
    );
  }

  Widget getRow(String chatType, ObjectKey key, Map message, int index) {
    Widget item;
    if (chatType == 'image') {
      item = ChatImage(
        key: key,
        message: message,
      );
    } else if (chatType == 'video') {
      item = ChatVideo(
        key: key,
        message: message,
      );
    } else if (chatType == 'audio') {
      item = ChatAudio(
        key: key,
        message: message,
      );
    } else if (chatType == 'document') {
      item = ChatDocument(
        key: key,
        message: message,
      );
    } else if (chatType == 'notification') {
      var chatMessage = message['text'];
      item = ChatNotification(
        chatMessage,
        key: key,
      );
    } else {
      bool isCurrentUser = _isCurrentUser(index);

      item = Container(
        key: key,
        color: _chatScreenProvider.isMessageSelected(message)
            ? selectionColor
            : Colors.transparent,
        alignment: isCurrentUser ? Alignment.centerRight : Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.only(
              right: isCurrentUser ? 0 : 100,
              left: isCurrentUser ? 100 : 0,
              bottom: 8),
          child: Bubble(
            //margin: BubbleEdges.only(top: 1),
            nip: isCurrentUser ? BubbleNip.rightTop : BubbleNip.leftTop,
            stick: !isCurrentUser,
            color: Color.fromRGBO(225, 255, 199, 1.0),
            child: Column(
              crossAxisAlignment: isCurrentUser
                  ? CrossAxisAlignment.end
                  : CrossAxisAlignment.start,
              children: <Widget>[
                if (!isCurrentUser)
                  Padding(
                    padding: EdgeInsets.only(bottom: 3),
                    child: Text(
                      '${message['user_fullname']}',
                      style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                Text('${message['text']}'),
                ChatStatus(
                  message: message,
                  alignRight: isCurrentUser,
                  showStatus: isCurrentUser,
                ),
              ],
            ),
          ),
        ),
      );
    }

    item = InkWell(
        onLongPress: () {
          resetSearch();
          _chatScreenProvider.setSelectedMessage(message);
        },
        onTap: () {
          _chatScreenProvider.unSelectedMessage();
        },
        child: item);

    return item;
  }

  Future _scrollToIndex(index) async {
    await autoScrollController.scrollToIndex(index,
        preferPosition: AutoScrollPosition.middle);
    await autoScrollController.highlight(index);
  }

  Widget _wrapScrollTag({int index, Widget child}) => AutoScrollTag(
        key: ValueKey(index),
        controller: autoScrollController,
        index: index,
        child: child,
        highlightColor: Colors.black.withOpacity(0.1),
      );

  appBar() {
    if (_chatScreenProvider.selectedMessage != null) {
      return getAppBarOnSelection();
    }
    if (searching) {
      return getSearchAppBar();
    } else {
      return getAppBar();
    }
  }

  getAppBar() {
    return AppBar(
      automaticallyImplyLeading: true,
      titleSpacing: -10,
      title: Row(
        children: <Widget>[
          CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: AssetImage('assets/images/logo.png'),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Stack(
              alignment: Alignment.centerLeft,
              children: [
                Visibility(
                    visible: _chatScreenProvider.groupDetails.isNotEmpty,
                    child: _buildGroupInfoOnAppBar(context)),
                Visibility(
                  visible: _chatScreenProvider.groupDetails.isEmpty,
                  child: Container(
                    child: SpinKitThreeBounce(
                      color: Colors.grey,
                      size: 20,
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.refresh),
          onPressed: () {
            _userProvider.setSyncToRemote(true);
            _userProvider.setSyncLatestUpdatesClick(true);
          },
        ),
        PopupMenuButton<String>(
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem<String>(
                value: "copy_issue",
                child: Text("Copy Issue"),
              ),
              PopupMenuItem<String>(
                value: "search",
                child: Text("Search"),
              ),
            ];
          },
          onSelected: (result) {
            if (result == 'copy_issue') {
              if (_appProvider.isTheCurrentUserPrivilegedFor(
                  UserPermissionMapKeys.canAddChatGroup, _userProvider.data)) {
                if (_chatScreenProvider.groupDetails['syncStatus'] ==
                    'pending') {
                  _scfKey.currentState.showSnackBar(
                    SnackBar(
                      content: Text("Please sync chat before copying"),
                      backgroundColor: Colors.red,
                    ),
                  );
                } else {
                  _newIssuePro.initEditData(_chatScreenProvider.groupDetails,
                      _appProvider, _userProvider,
                      copying: true);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CreateNewIssueGroup()),
                  );
                }
              } else {
                Toast.show(
                  'You do not have the privilege to copy an issue',
                  context,
                  backgroundColor: Colors.red,
                );
              }
            } else if (result == 'search') {
              setState(() {
                searching = true;
              });
            }
          },
        )
      ],
    );
  }

  Future<bool> _getArchivedChats() async {
    _userProvider.setLoading(true);

    if (lock.locked) {
      return false;
    } else {
      await lock.synchronized(() async {
        try {
          Dio dio = Dio();
          dio.options.headers['content-Type'] = 'application/json';
          dio.options.headers["Authorization"] = _userProvider.token;

          Map body = {
            "message_id": _chatScreenProvider.getLastMessageId(),
          };

          try {
            var resp = await dio.postExtended(getChatMessages, data: body);

            Map respData = resp.data;

            if (respData['success']) {
              List messages = respData['data'];
              await insertGroupMessages(
                  messages, _chatScreenProvider.currentChatGroupId);
              await _chatScreenProvider.refreshChatData();
              return messages.isNotEmpty;
            }
          } catch (e) {
            PrintString(e);
          }
        } catch (e) {} finally {
          _userProvider.setLoading(false);
        }
      });
      return false;
    }
  }

  getAppBarOnSelection() {
    return AppBar(
      automaticallyImplyLeading: true,
      titleSpacing: -10,
      actions: <Widget>[
        _chatScreenProvider.selectedMessage['type'] == 'TEXT'
            ? IconButton(
                icon: Icon(Icons.content_copy),
                onPressed: () {
                  _chatScreenProvider.copySelectedMessage();
                  Toast.show('Message Copied', context);
                },
              )
            : Container(),
        _chatScreenProvider.selectedMessage['user_id'] == _userProvider.userId
            ? IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChatMessageStatus(
                            message: _chatScreenProvider.selectedMessage)),
                  );
                },
              )
            : Container(),
      ],
    );
  }

  getSearchAppBar() {
    return AppBar(
      automaticallyImplyLeading: true,
      titleSpacing: -10,
      title: TextFormField(
        autofocus: true,
        onChanged: (val) {
          setState(() {
            query = val;
          });
        },
        cursorColor: Colors.white,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          hintText: "Search",
          hintStyle: TextStyle(color: Colors.white),
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.arrow_drop_up),
          onPressed: () async {
            await findNext();
          },
        ),
        IconButton(
          icon: Icon(Icons.arrow_drop_down),
          onPressed: () {
            findPrev();
          },
        ),
      ],
    );
  }

  findNext() async {
    int newIndex = _chatScreenProvider.search(query, 'up', index: searchIndex);

    if (newIndex == null) {
      bool moreMessages = await _getArchivedChats();
      if (moreMessages) {
        await findNext();
      }
    }

    await searchQuery(newIndex);
  }

  Future searchQuery(int newIndex) async {
    if (newIndex == null) {
      Toast.show('Not results', context, gravity: 100);
    } else {
      setState(() {
        searchIndex = newIndex;
      });
      await _scrollToIndex(newIndex);
    }
  }

  resetSearch() {
    setState(() {
      query = '';
      searchIndex = -1;
      searching = false;
    });
  }

  findPrev() async {
    int newIndex =
        _chatScreenProvider.search(query, 'down', index: searchIndex);

    await searchQuery(newIndex);
  }

  InkWell _buildGroupInfoOnAppBar(BuildContext context) {
    return InkWell(
      onTap: _onTapGroupInfo,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '${_chatScreenProvider.groupDetails['issue_no'] != null ? _chatScreenProvider.groupDetails['issue_no'] : ''} ${_chatScreenProvider.groupDetails['group_name']}'
                .trim(),
            style: TextStyle(
              fontWeight: FontWeight.w400,
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(height: 3),
          Container(
            alignment: Alignment.bottomLeft,
            height: 15,
            width: MediaQuery.of(context).size.width * .6,
            child: ListView.separated(
              //physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: _chatScreenProvider.groupMembersList.length,
              itemBuilder: (context, index) {
                return Text(
                  '${_chatScreenProvider.groupMembersList[index]['user_fullname']}',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w300,
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Text(
                  ', ',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w300,
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Container _buildChatBottomBar() {
    if (searching) {
      return Container(
        child: Text(''),
      );
    } else if (readOnly) {
      return Container(
        width: double.maxFinite,
        color: chatInfoColor,
        alignment: Alignment.center,
        constraints: BoxConstraints(minHeight: 40),
        margin: EdgeInsets.only(bottom: 4, top: 4, left: 4),
        child: Container(child: Text('View Only')),
      );
    } else {
      return Container(
        constraints: BoxConstraints(minHeight: 60, maxHeight: 200),
        margin: EdgeInsets.only(bottom: 4, top: 4, left: 4),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.keyboard),
                    ),
                    Expanded(
                      child: TextField(
                        enabled: _recorder == null,
                        controller: _messageTextCtrl,
                        onChanged: _onChatMessageChanged,
                        maxLines: null,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Type a message"),
                      ),
                    ),
                    _buildChatButton(Icons.attach_file, _showAttachOption),
                    _buildChatButton(Icons.camera_alt, () {
                      //_getImageFromDevice(ImageSource.camera);
                      // _showPicker(context);
                      _getImageFromCamera();
                      // Navigator.of(context).pop();
                    }),
                  ],
                ),
              ),
            ),
            SizedBox(width: 4),
            Stack(alignment: Alignment.centerRight, children: [
              Container(
                decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(23.0))),
                width: _widthAnimation.value,
                height: 46,
                padding:
                    EdgeInsets.only(left: _recordingDuration > 59 ? 10 : 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Text(
                        getVoiceDuration(),
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(" m", style: TextStyle(fontSize: 12))
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: onTapVoiceMessage,
                onTapDown: onTapDownVoiceMessage,
                onTapCancel: onTapCancelVoiceMessage,
                child: CircleAvatar(
                  radius: 23,
                  backgroundColor: primaryColor,
                  child: Icon(
                    _messageText.isEmpty ? Icons.mic : Icons.send,
                    color: Colors.white,
                    size: 23,
                  ),
                ),
              )
            ]),
            SizedBox(width: 4)
          ],
        ),
      );
    }
  }

  String getVoiceDuration() {
    if (_tapState == TapState.Down && _recordingDuration > 1) {
      var txt = "${_recordingDuration ~/ 60}:${_recordingDuration % 60}";

      if (maxVoice != null) {
        txt += " / ${maxVoice ~/ 60}:${maxVoice.round() % 60}";
      }

      return txt;
    } else {
      return '';
    }
  }

  Visibility _buildChatButton(IconData icon, VoidCallback onPressed) {
    return Visibility(
      visible: _chatButtonsVisible,
      child: AnimatedOpacity(
        duration: Duration(milliseconds: 300),
        opacity: _chatButtonOpacity,
        child: IconButton(
          onPressed: onPressed,
          icon: Icon(icon, color: Theme.of(context).disabledColor),
        ),
      ),
    );
  }

  void onTapVoiceMessage() async {
    _tapState = TapState.Up;
    await _voiceRecordingLock.synchronized(() async {
      if (_messageText.isEmpty) {
        PrintString("onTapVoiceMessage");
        if (_recorder != null) {
          PrintString("_recorder != null");
          await _recorder.stop();
          var recording = await _recorder.current(channel: 0);

          PrintString("Audio Recording Duration :" +
              recording.duration.inMilliseconds.toString());
          if (recording.duration.inMilliseconds > 500) {
            PrintString(recording.path);
            await _sendMessageOffline(type: 'AUDIO', file: recording.path);
          }
          // _recorder.dispose();
          _recorder = null;
        }
      } else {
        await _sendMessageOffline(type: 'TEXT');
      }
    });
  }

  bool _isInTapDown = false;
  void onTapDownVoiceMessage(TapDownDetails details) async {
    _tapState = TapState.Down;
    if (_isInTapDown) return;
    await _voiceRecordingLock.synchronized(() async {
      _isInTapDown = true;
      try {
        voiceRecordTapDownTimeStamp = DateTime.now().millisecondsSinceEpoch;
        if (_messageText.isEmpty) {
          PrintString("onTapDownVoiceMessage");
          bool hasPermission = await FlutterAudioRecorder.hasPermissions;
          if (hasPermission &&
              (DateTime.now().millisecondsSinceEpoch -
                      voiceRecordTapDownTimeStamp <
                  500)) {
            if (_recorder == null) {
              PrintString("_recorder == null");

              String filePath = "${await getStoragePath()}.wav";
              PrintString(filePath);
              _recorder = FlutterAudioRecorder(filePath.replaceAll(' ', ''),
                  audioFormat: AudioFormat.WAV); // .wav .aac .m4a
              await _recorder.initialized;
              await _recorder.start();
              _initVoiceRecordingTimerAnimation();

              PrintString("Recording started");
            }
          }
        }
      } finally {
        _isInTapDown = false;
      }
    });
  }

  void _killVoiceRecordingTimer(bool reinit) {
    if (null != _recordingTimer) {
      _recordingTimer.cancel();
      _recordingTimer = null;
    }

    if (reinit) {
      setState(() {
        _recordingDuration = 1;
      });
      _recordingTimer = Timer.periodic(Duration(seconds: 1), (Timer t) async {
        setState(() {
          _recordingDuration++;
        });

        if (maxVoice != null && _recordingDuration >= maxVoice) {
          onTapVoiceMessage();
        }
      });
    }
  }

  void _initVoiceRecordingTimerAnimation() {
    Future.delayed(Duration(milliseconds: 300), () {
      _killVoiceRecordingTimer(true);

      if (_tapState == TapState.Down) _animationController.forward();
      Timer.periodic(Duration(milliseconds: 50), (Timer t) async {
        if (_tapState == TapState.Up) {
          if (_animationController.isAnimating) {
            _animationController.stop();
          }
          _animationController.reverse();
          t.cancel();

          _killVoiceRecordingTimer(false);
        }
        setState(() {});
      });
    });
  }

  void onTapCancelVoiceMessage() async {
    _tapState = TapState.Up;
    await _voiceRecordingLock.synchronized(() async {
      PrintString("onTapCancelVoiceMessage");
      if (_recorder != null) {
        PrintString("_recorder != null");
        await _recorder.stop();
        _recorder = null;
      }
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _userProvider.setSyncToRemote(false);
      _setCurrentChatGroupId();
    });
    super.initState();

    if (widget.groupDetails['unread_message_count'] > 0) {
      markGroupAsRead(
          _userProvider, _chatScreenProvider, widget.groupDetails['groupId']);
    }

    _animationController = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    _animationCurve =
        CurvedAnimation(parent: _animationController, curve: Curves.easeOut);

    _widthAnimation = Tween(begin: 46.0, end: 170.0).animate(_animationCurve);

    autoScrollController = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: Axis.horizontal)
      ..addListener(onScroll);
  }

  void onScroll() {
    if (autoScrollController.position.extentAfter < 1) {
      _getArchivedChats();
    }
  }

  @override
  void dispose() {
    super.dispose();

    _resetCurrentChatGroupId();

    Timer(Duration(seconds: 1), () {
      _userProvider.setSyncToRemote(true);
    });

    if (_animationController != null) {
      _animationController.dispose();
      _animationController = null;
    }
  }

  void _setCurrentChatGroupId() {
    var currentChatGroupId = widget.groupDetails['groupId'].toString();
    _chatScreenProvider.currentChatGroupId = currentChatGroupId;
    _currentChatIds.add(currentChatGroupId);
  }

  void _resetCurrentChatGroupId() {
    if (_currentChatIds.isNotEmpty) {
      _currentChatIds.removeLast();
    }

    if (_currentChatIds.isEmpty) {
      _chatScreenProvider.currentChatGroupId = null;
    } else {
      _chatScreenProvider.currentChatGroupId = _currentChatIds.last;
    }
  }

  _showAttachOption() {
    FocusScope.of(context).unfocus();
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      barrierColor: Color(0x00ff00),
      context: context,
      builder: (context) {
        return Container(
          margin: EdgeInsets.only(bottom: 60, left: 10, right: 10),
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 25,
                    backgroundColor: Colors.blue.shade600,
                    child: IconButton(
                      onPressed: () async {
                        Navigator.pop(context);
                        unawaited(_getDocumentFromDevice());
                      },
                      icon: Icon(
                        Icons.insert_drive_file,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 3),
                  Text(
                    'Document',
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
              // Column(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: <Widget>[
              //     CircleAvatar(
              //       radius: 25,
              //       backgroundColor: Colors.deepOrangeAccent,
              //       child: Icon(
              //         Icons.music_note,
              //         color: Colors.white,
              //       ),
              //     ),
              //     SizedBox(height: 3),
              //     Text(
              //       'Audio',
              //       style: TextStyle(
              //         fontSize: 12,
              //       ),
              //     ),
              //   ],
              // ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  _getImageFromGallery();
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.indigo,
                      child: Icon(
                        Icons.image,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(height: 3),
                    Text(
                      'Gallery',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  _isCurrentUser(index) {
    return _chatScreenProvider.chatMessage[index]['user_id'] ==
            _userProvider.userId
        ? true
        : false;
  }

  Future<void> _getDocumentFromDevice() async {
    String filePath = await FilePicker.getFilePath();
    if (null != filePath) {
      var maxFileSize = _appProvider.config["max_file_size"];
      var fileSize = (await File(filePath).length()) * 0.000001;

      if (maxFileSize != null && fileSize > maxFileSize) {
        Toast.show(
          'Maximum allowed document is $maxFileSize mb',
          context,
          duration: 5,
          backgroundColor: Colors.red,
        );
      } else {
        await _sendMessageOffline(type: 'DOCUMENT', file: filePath);
      }
    }
  }

  Future _sendImage(String filePath) async {
    PrintString(filePath);
    var p = filePath.split('/');
    String fileName = p[p.length - 1];
    var compressedFile;

    String fileType = fileName.split('.').last == 'mp4' ? 'VIDEO' : 'IMAGE';

    if (fileType == 'IMAGE') {
      final targetPath = "${await getStorageLoc()}/$fileName";
      compressedFile = await FlutterImageCompress.compressAndGetFile(
          filePath, targetPath,
          quality: 80);
    } else {
      compressedFile = File(filePath);
    }

    await _sendMessageOffline(type: fileType, file: compressedFile.path);
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_camera),
                      title: Text('Image'),
                      onTap: () {
                        _getImageFromCamera();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: Icon(Icons.videocam),
                    title: Text('Video'),
                    onTap: () {
                      _getVideoFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> _getImageFromCamera() async {
    // Obtain a list of the available cameras on the device.
    final cameras = await availableCameras();

    // Get a specific camera from the list of available cameras.
    final firstCamera = cameras.first;

    String imagePath = await Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => TakePictureScreen(
              camera: firstCamera,
            )));

    if (null != imagePath) {
      await _sendImage(imagePath);
    }
  }

  Future<void> _getVideoFromCamera() async {
    // Obtain a list of the available cameras on the device.
    final cameras = await availableCameras();

    // Get a specific camera from the list of available cameras.
    final firstCamera = cameras.first;

    String imagePath = await Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => CaptureVideoScreen(
              camera: firstCamera,
            )));

    if (null != imagePath) {
      await _sendImage(imagePath);
    }
  }

  Future<void> _getImageFromGallery() async {
    String imagePath = await FilePicker.getFilePath(type: FileType.image);
    if (null != imagePath) {
      await _sendImage(imagePath);
    }
  }

  // This is to a message to DB and try to  send it online
  Future<void> _sendMessageOffline({String type, file}) async {
    String message = _messageText;
    String syncStatus = 'pending';

    String id = Random().nextInt(100000).toString();
    String groupId = widget.groupDetails['groupId'];
    await _dbHelper.insertChatMessage(
      id: id,
      userId: _userProvider.userId,
      userFullName: _userProvider.userNameFullName,
      userName: _userProvider.userName,
      timeStamp: await getCurrentSec(),
      type: type,
      localPath: file,
      file: null,
      chatGroupId: groupId,
      text: message,
      syncStatus: syncStatus,
    );
    if (!mounted) return;
    setState(() {
      _messageText = '';
    });
    _messageTextCtrl.clear();
    _onChatMessageChanged('');
    unawaited(
        _chatScreenProvider.refreshChatData().then((value) => setState(() {})));

    _userProvider.setSyncToRemote(true);
    _userProvider.setSyncLatestUpdatesClick(true);
  }

  _onTapGroupInfo() {
    if (_chatScreenProvider.groupDetails['syncStatus'] == 'pending') {
      _scfKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Please sync chat before editing"),
          backgroundColor: Colors.red,
        ),
      );
      return false;
    }
    _newIssuePro.initEditData(
        _chatScreenProvider.groupDetails, _appProvider, _userProvider);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CreateNewIssueGroup()),
    );
  }

  void _onChatMessageChanged(String val) {
    if (mounted) {
      setState(() {
        _chatButtonOpacity = (val?.length ?? 0) == 0 ? 1.0 : 0.0;
        if (_chatButtonOpacity == 0.0) {
          Future.delayed(Duration(milliseconds: 1000), () {
            setState(() {
              _chatButtonsVisible = !(_chatButtonOpacity == 0.0);
            });
          });
        } else {
          _chatButtonsVisible = true;
        }

        _messageText = val;
      });
    }
  }
}
