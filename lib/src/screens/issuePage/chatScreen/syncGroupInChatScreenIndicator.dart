import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/syncFunction/syncFunction.dart';
import 'package:toast/toast.dart';

// UI Widget Indicator to manually sync the group
class SyncGroupInChatScreenIndicator extends StatefulWidget {
  final groupDetails;

  const SyncGroupInChatScreenIndicator({Key key, this.groupDetails})
      : super(key: key);
  @override
  _SyncGroupInChatScreenIndicatorState createState() =>
      _SyncGroupInChatScreenIndicatorState();
}

class _SyncGroupInChatScreenIndicatorState
    extends State<SyncGroupInChatScreenIndicator> {
  DBHelper _dbHelper = DBHelper.instance;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: 50,
      color: Colors.red,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              'Group not synced',
              style: TextStyle(color: Colors.white),
            ),
          ),
          FlatButton(
            onPressed: () async {
              await _syncFunction();
            },
            color: Colors.white,
            child: Text('SYNC'),
          )
        ],
      ),
    );
  }

  Future<void> _syncFunction() async {
//    List groupTag = await _dbHelper.getGroupTagByGroupIdData(widget.groupDetails['groupId']);
//    List groupMembers = await _dbHelper.getGroupMembersByGroupIdData(widget.groupDetails['groupId']);
//    List groupMessage = await _dbHelper.getChatMessageByGroupIdData(widget.groupDetails['groupId']);
//    PrintString(widget.groupDetails);
//    PrintString('\ngroup tags $groupTag');
//    PrintString('\ngroup members $groupMembers');
//    PrintString('\ngroup Message $groupMessage');
    await SyncFunction(appProvider: AppProvider(), userProvider: UserProvider())
        .syncGroup(); //todo pass appProvider and UserProvider
  }
}
