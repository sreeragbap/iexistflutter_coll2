import 'package:flutter/material.dart';

class ChatNotification extends StatelessWidget {
  final String text;
  const ChatNotification(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return null != text
        ? Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Center(
                child: Card(
                    color: Colors.blue[50],
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(text),
                    ))),
          )
        : Container();
  }
}
