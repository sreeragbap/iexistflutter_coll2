import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/cupertino.dart';

enum PlayerState { stopped, playing, paused }

class VoiceMessagePlayer extends ChangeNotifier {
  static final VoiceMessagePlayer _singleton = VoiceMessagePlayer._internal();

  factory VoiceMessagePlayer() => _singleton;

  VoiceMessagePlayer._internal() {
    WidgetsFlutterBinding.ensureInitialized();
    _initAudioPlayer();
  }

  String _url;
  String get url => _url;

  Duration _duration;
  Duration get duration => _duration;

  Duration _position;
  Duration get position => _position;

  AudioPlayer _audioPlayer;
  PlayerState _playerState = PlayerState.stopped;
  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  get isPlaying => _playerState == PlayerState.playing;
  get isPaused => _playerState == PlayerState.paused;
  get durationText => _duration?.toString()?.split('.')?.first ?? '';
  get positionText => _position?.toString()?.split('.')?.first ?? '';

  void disposeAudioPlayer() {
    if (null != _audioPlayer) {
      _positionSubscription?.cancel();
      _audioPlayerStateSubscription?.cancel();
      _audioPlayer = null;
    }
  }

  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer();

    _positionSubscription = _audioPlayer.onAudioPositionChanged.listen((p) {
      _position = p;
      notifyListeners();
    });

    _audioPlayerStateSubscription =
        _audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        _duration = _audioPlayer.duration;
        notifyListeners();
      } else if (s == AudioPlayerState.STOPPED) {
        _playerState = PlayerState.stopped;
        _position = Duration();
        _duration = Duration();
        _url = null;
        notifyListeners();
      }
    }, onError: (msg) {
      _playerState = PlayerState.stopped;
      _position = Duration();
      _duration = _audioPlayer.duration;
      _url = null;
      notifyListeners();
    });
  }

  Future<void> play(String path) async {
    if (url == path && isPaused) {
      await _audioPlayer.play(url, isLocal: true);
      await _audioPlayer.seek(_position.inSeconds.toDouble());
    } else {
      if (isPlaying || isPaused) {
        await stop();
      }

      _url = path;
      _audioPlayer.play(url, isLocal: true);
      notifyListeners();
    }

    _playerState = PlayerState.playing;
  }

  Future<void> pause() async {
    await _audioPlayer.pause();
    _playerState = PlayerState.paused;
  }

  Future<void> stop() async {
    await _audioPlayer.stop();
    _playerState = PlayerState.stopped;
    _position = Duration();
    _duration = Duration();
  }

  Future<void> seek(String path, double seekPercentage) async {
    if (path == url) {
      if (seekPercentage < 0 ||
          seekPercentage > 1 ||
          _duration.inMilliseconds == 0) {
        return;
      }

      final position = seekPercentage * _duration.inMilliseconds;
      await _audioPlayer
          .seek(Duration(milliseconds: position.toInt()).inSeconds.toDouble());
      _position = Duration(milliseconds: position.toInt());
    }
  }
}
