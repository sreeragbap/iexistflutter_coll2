import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'chatMedia.dart';

// Chat Image and Text chat type
class ChatDocument extends ChatMedia {
  const ChatDocument({Key key, Map message})
      : super(key: key, message: message, mediaType: "DOCUMENT");

  @override
  Widget getLocalMediaRenderer(
      BuildContext context, String localMediaPath, isCurrentUser) {
    int maxDocumentNameLength = 20;

    var p = localMediaPath.split('/');
    String fullFileName = p[p.length - 1];

    String fileName = fullFileName;

    if (!isCurrentUser) {
      var timeParts = fullFileName.split('_');
      if (null != int.tryParse(timeParts[0])) {
        fileName = timeParts.skip(2).join('_');
      }
    }

    String fileDisplayName = fileName.substring(
        0,
        (fileName.length > maxDocumentNameLength
            ? maxDocumentNameLength
            : fileName.length));
    if (fileDisplayName != fileName) {
      fileDisplayName = fileDisplayName + "...";
    }

    return InkWell(
      child: Container(
          height: mediaContainerHeight,
          width: mediaContainerWidth,
          margin: EdgeInsets.all(2),
          child: Container(
            color: Colors.black.withOpacity(0.05),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Icon(Icons.attach_file),
                ),
                Text(
                  fileDisplayName,
                ),
              ],
            ),
          )),
      onTap: () async {
        await OpenFile.open(localMediaPath);
      },
    );
  }

  @override
  Widget getRemoteMediaRenderer(BuildContext context, String remoteMediaPath) {
    return Container(
      height: mediaContainerHeight,
      width: mediaContainerWidth,
    );
  }

  @override
  VoidCallback getLocalMediaOnTap(BuildContext context, String localMediaPath) {
    return () {};
  }

  @override
  VoidCallback getRemoteMediaOnTap(
      BuildContext context, String remoteMediaPath) {
    return () {};
  }

  @override
  double get mediaContainerHeight => 65;

  @override
  double get mediaContainerWidth => 250;
}
