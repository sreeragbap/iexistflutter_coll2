import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatScreenPage.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:iexist/src/utilities/ui_helper.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:provider/provider.dart';
import 'package:synchronized/synchronized.dart' as synchronized;

// Used to render the group statuses in the top
class IssueTabScreen extends StatefulWidget {
  final List groupList;
  final String status;

  const IssueTabScreen({Key key, this.groupList, this.status})
      : super(key: key);

  @override
  _IssueTabScreenState createState() => _IssueTabScreenState();
}

class _IssueTabScreenState extends State<IssueTabScreen> {
  DBHelper _dbHelper = DBHelper.instance;
  ScrollController scrollController;
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  static final synchronized.Lock lock = synchronized.Lock();

  @override
  void initState() {
    super.initState();

    scrollController = ScrollController();

    scrollController.addListener(() {
      if (scrollController.position.extentAfter < 1) {
        _getArchivedGroups();
      }
    });
  }

  void _getArchivedGroups() async {
    _userProvider.setLoading(true);

    if (lock.locked) {
      return;
    } else {
      await lock.synchronized(() async {
        try {
          Dio dio = Dio();
          dio.options.headers['content-Type'] = 'application/json';
          dio.options.headers["Authorization"] = _userProvider.token;

          Map body = {
            "user_id": _userProvider.userId,
            "group_id": _appProvider.getLastGroupId(),
            "reverse": false,
          };

          try {
            var resp = await dio.postExtended(getChatGroups, data: body);

            Map respData = resp.data;

            if (respData['success']) {
              List groupList = respData['data'];
              for (var group in groupList) {
                await insertGroups(group, group['timestamp'], _appProvider,
                    showNotification: false);
              }
              await _appProvider.updateIssueGroupList();
            }
          } catch (e) {
            PrintString(e);
          }
        } catch (e) {} finally {
          _userProvider.setLoading(false);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);

    return Container(
      child: ListView.separated(
        padding: EdgeInsets.symmetric(vertical: 20),
        itemCount: widget.groupList.length,
        controller: scrollController,
        itemBuilder: (context, index) {
          return Visibility(
            visible: widget.groupList[index]['group_status']
                    .toString()
                    .toLowerCase() ==
                widget.status.toLowerCase(),
            child: ListTile(
              onTap: () => _onTapChatGroupListTile(index),
              leading: CircleAvatar(
                radius: 25,
                backgroundColor: Colors.white,
                backgroundImage: AssetImage('assets/images/logo.png'),
              ),
              title: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      '${widget.groupList[index]['issue_no'] != null ? widget.groupList[index]['issue_no'] : ''} ${widget.groupList[index]['group_name']}'
                          .trim(),
                      style: TextStyle(fontWeight: FontWeight.w600),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  //                  Text(
                  //                    UiHelper()
                  //                        .getDate('${widget.groupList[index]['times']}'),//TODO CHANGE update date
                  //                    textAlign: TextAlign.center,
                  //                    style: TextStyle(fontSize: 12, color: Colors.grey),
                  //                  ),
                ],
              ),
              trailing: Visibility(
                visible: widget.groupList[index]['unread_message_count'] > 0,
                child: CircleAvatar(
                  backgroundColor: buttonColor,
                  radius: 11,
                  child: Center(
                      child: Text(
                    widget.groupList[index]['unread_message_count'].toString(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 11,
                        fontWeight: FontWeight.bold),
                  )),
                ),
              ),
              //              subtitle: Visibility(
              //                visible: widget.groupList[index]['complainant'] != null,
              //                child: FutureBuilder(
              //                  future: _dbHelper.getGroupTagByGroupIdData(
              //                      widget.groupList[index]['groupId']),
              //                  builder: (context, snapshot) {
              //                    if (snapshot.hasData) {
              //                      return Container(
              //                        height: 15,
              //                        child: ListView.builder(
              //                          shrinkWrap: true,
              //                          scrollDirection: Axis.horizontal,
              //                          itemCount: snapshot.data.length,
              //                          itemBuilder: (context, index) {
              //                            return Text('${snapshot.data[index]['tagName']}, ');
              //                          },
              //                        ),
              //                      );
              //                    }
              //                    return Text('');
              //                  },
              //                ),
              //              ),
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Visibility(
            visible: widget.groupList[index]['group_status'] == widget.status,
            child: Divider(
              indent: 60,
            ),
          );
        },
      ),
    );
  }

  void _onTapChatGroupListTile(int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChatScreenPage(
          groupDetails: widget.groupList[index],
        ),
      ),
    );
  }
}
