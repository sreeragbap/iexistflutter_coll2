import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/widgets/tagChipsWrap.dart';
import 'package:provider/provider.dart';

class SearchGroupTags extends StatefulWidget {
  @override
  _SearchGroupTagsState createState() => _SearchGroupTagsState();
}

class _SearchGroupTagsState extends State<SearchGroupTags> {
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  AppProvider _appProvider = AppProvider();
  UserProvider _userProvider = UserProvider();
  final _dbHelper = DBHelper.instance;

  @override
  Widget build(BuildContext context) {
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        titleSpacing: 0,
        title: TextField(
          onChanged: (val) {
            _newIssuePro.getSearchResultTags(val);
          },
          decoration: InputDecoration.collapsed(hintText: 'Search Tags'),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'OK',
              style: TextStyle(color: Colors.green),
            ),
          )
        ],
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
        child: TagChipsWrap(
          tagList: _newIssuePro.searchResultTags,
          tagChipTap: _onTapTagChip,
          isTagChipSelected: _checkIsTagSelected,
          getTagChipLabel: _getTagChipLabel,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _getGroupTagFromLocalDB();
    });
  }

  bool _checkIsTagSelected(index) {
    Map tag = _newIssuePro.searchResultTags[index];
    for (int i = 0; i < _newIssuePro.selectedGroupTags.length; i++) {
      if (_newIssuePro.selectedGroupTags[i]['tagId'].toString() ==
          tag['tagId'].toString()) {
        return true;
      }
    }
    return false;
  }

  void _getGroupTagFromLocalDB() async {
    List groupTag = await _dbHelper
        .getTagDataFromOrganization(_appProvider.selectedOrganization['_id']);
    _newIssuePro.setAllGroupTags(groupTag);
    PrintString(groupTag);
  }

  void _onTapTagChip(int index) {
    _newIssuePro.setSelectedGroupTags(_newIssuePro.searchResultTags[index]);
  }

  String _getTagChipLabel(int index) {
    return _newIssuePro.searchResultTags[index]['tagName'];
  }
}
