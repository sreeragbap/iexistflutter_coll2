import 'package:flutter/material.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/firstPage/savingLoading.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/step1Page.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/step2Page.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class CreateNewIssueGroup extends StatefulWidget {
  @override
  _CreateNewIssueGroupState createState() => _CreateNewIssueGroupState();
}

class _CreateNewIssueGroupState extends State<CreateNewIssueGroup>
    with TickerProviderStateMixin {
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();

  @override
  Widget build(BuildContext context) {
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);

    bool creating = _newIssuePro.groupId == null;

    return WillPopScope(
        onWillPop: () async {
          return !_userProvider.saving;
        },
        child: Stack(children: [
          DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                titleSpacing: 0,
                title: Text(creating ? 'New Ticket' : 'Edit Ticket'),
                bottom: TabBar(
                  controller: _newIssuePro.tabController,
                  labelStyle: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                  indicatorColor: Colors.white,
                  tabs: <Widget>[
                    Tab(
                      text: 'STEP 1',
                    ),
                    Tab(
                      text: 'STEP 2',
                    ),
                  ],
                ),
              ),
              body: TabBarView(
                controller: _newIssuePro.tabController,
                children: <Widget>[
                  Step1Page(tabController: _newIssuePro.tabController),
                  Step2Page(),
                ],
              ),
            ),
          ),
          Visibility(visible: _userProvider.saving, child: SavingLoading()),
        ]));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((time) {
      _userProvider.setSaving(false);
      _newIssuePro.tabController =
          TabController(length: 2, initialIndex: 0, vsync: this);
      _newIssuePro.tabController.addListener(() {});
      _userProvider.setSyncToRemote(false);

      _setMembers();
      _setOfflineId();
    });
  }

  Future<void> _setMembers() async {
    List groupTag = await DBHelper.instance
        .getTagDataFromOrganization(_appProvider.selectedOrganization['_id']);
    _newIssuePro.setAllGroupTags(groupTag);

    List groupMembersList = await DBHelper.instance.getUserDataFromOrganization(
      _appProvider.selectedOrganization['_id'],
    );

    await _newIssuePro.setAllGroupMembers(
        groupMembersList, _appProvider, _userProvider);

    var groupId = _newIssuePro.groupId ?? _newIssuePro.copyOf;

    if (groupId != null) {
      List membersSaved =
          await DBHelper.instance.getGroupMembersByGroupIdData(groupId);

      for (Map member in membersSaved) {
        dynamic memberToAdd = _newIssuePro.getMemberWithId(member["user_id"]);

        if (null != memberToAdd) {
          _newIssuePro.initSelectedMembers(memberToAdd);
        } else if (member['user_id'] != _userProvider.userId) {
          _newIssuePro.setHiddenMembers(member);
        }
      }
    } else {
      for (Map member in _appProvider.getDefaultUsers(_userProvider.data)) {
        dynamic defaultMember = _newIssuePro.getMemberWithId(member["_id"]);
        if (null != defaultMember) {
          _newIssuePro.initSelectedMembers(defaultMember);
        }
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    //_createNewIssueGroupProvider.tabController.dispose();
    _newIssuePro.clearAllData(_appProvider);
  }

  void _setOfflineId() {
    if (_newIssuePro.groupId != null) {
      _newIssuePro.offlineId = null;
    } else {
      _newIssuePro.offlineId = Uuid().v4();
    }
  }
}
