import 'package:flutter/material.dart';

class ListTileSubtitleTagList extends StatelessWidget {
  const ListTileSubtitleTagList({
    Key key,
    @required this.tagList,
  }) : super(key: key);

  final List tagList;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomLeft,
      height: 15,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: tagList.length,
        itemBuilder: (context, tagIndex) {
          return ListTileSubtitleTag(
            text: tagList.isEmpty ? '' : "${tagList[tagIndex]['tagName']}",
          );
        },
      ),
    );
  }
}

class ListTileSubtitleTag extends StatelessWidget {
  const ListTileSubtitleTag({
    Key key,
    @required this.text,
  }) : super(key: key);

  final String text;

//TODO: This complex Container implementation to be replaced with Chip.
//But Chip unable to align the label to exact center as it is placed in ListTile subtitle
//A solution for this need to be figured out

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 4),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 4),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.2),
          borderRadius: BorderRadius.all(Radius.circular(3.0))),
      child: Text(text ?? '', style: TextStyle(fontSize: 13)),
    );
  }
}
