import 'dart:async';

import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/listTileSubtitleTagList.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/widgets/tagChipsWrap.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class SearchGroupMembersPage extends StatefulWidget {
  @override
  _SearchGroupMembersPageState createState() => _SearchGroupMembersPageState();
}

class _SearchGroupMembersPageState extends State<SearchGroupMembersPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();

  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();

  Future<bool> _willPopCallback() async {
    Future.delayed(Duration(seconds: 1), () {
      _newIssuePro.resetSearchResults();
      _newIssuePro.resetAllGroupFilterTags();
    });

    return true;
  }

  @override
  void initState() {
    super.initState();
    _newIssuePro.initTempSelectedMembers();
  }

  @override
  Widget build(BuildContext context) {
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);

    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          titleSpacing: 20,
          title: TextField(
            onChanged: (val) {
              _newIssuePro.getSearchResultGroupMembers(val);
            },
            decoration: InputDecoration.collapsed(hintText: 'Search members'),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.tune),
              onPressed: () {
                _scaffoldKey.currentState.openEndDrawer();
              },
            ),
            IconButton(
              onPressed: () {
                var error = _newIssuePro.saveSelectedMembers(
                    _userProvider, _appProvider);
                if (error == null) {
                  _willPopCallback();
                  Navigator.pop(context);
                } else {
                  Toast.show(
                    error,
                    context,
                    duration: 5,
                    backgroundColor: Colors.red,
                  );
                }
              },
              icon: Icon(Icons.check),
            )
          ],
          iconTheme: IconThemeData(color: Colors.black),
        ),
        endDrawer: Drawer(
            child: ListView(children: <Widget>[
          _createHeader(),
          if (!_newIssuePro.groupMembersLoading)
            Padding(
              padding: EdgeInsets.only(left: 24, top: 16, right: 24),
              child: TagChipsWrap(
                tagList: _newIssuePro.groupFilterTags,
                tagChipTap: _onTapFilterTag,
                isTagChipSelected: _checkIsFilterTagSelected,
                getTagChipLabel: _getFilterTagLabel,
              ),
            )
        ])),
        body: Column(
          children: <Widget>[
            if (_newIssuePro.groupMembersLoading)
              Container(
                margin: EdgeInsets.only(top: 20),
                height: 20,
                width: 20,
                child: CircularProgressIndicator(),
              ),
            CheckboxListTile(
              onChanged: (value) {
                if (value) {
                  _newIssuePro.selectAllGroupMembers(
                      _appProvider, _userProvider);
                } else {
                  _newIssuePro.deSelectAllGroupMembers(
                      _appProvider, _userProvider);
                }
              },
              value: _newIssuePro.selectAllUserCheck,
              controlAffinity: ListTileControlAffinity.leading,
              title: Text("Select All"),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _newIssuePro.filteredSearchResultGroupMembers.length,
                itemBuilder: (context, index) {
                  return ListTile(
                      onTap: () {
                        PrintString(_newIssuePro
                            .filteredSearchResultGroupMembers[index]);
                        _newIssuePro.setSelectedMembers(
                            _newIssuePro
                                .filteredSearchResultGroupMembers[index],
                            _appProvider,
                            _userProvider,
                            context);
                      },
                      leading: _newIssuePro.tmpSelectedGroupMembers.contains(
                              _newIssuePro
                                  .filteredSearchResultGroupMembers[index])
                          ? CircleAvatar(
                              backgroundColor: Colors.green,
                              child: Text(
                                '${_newIssuePro.filteredSearchResultGroupMembers[index]['user_fullname']}'[
                                        0]
                                    .toUpperCase(),
                                style: TextStyle(color: Colors.white),
                              ),
                            )
                          : CircleAvatar(
                              backgroundColor: Colors.red.shade50,
                              child: Text(
                                '${_newIssuePro.filteredSearchResultGroupMembers[index]['user_fullname']}'[
                                        0]
                                    .toUpperCase(),
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                      title: Text(
                        '${_newIssuePro.filteredSearchResultGroupMembers[index]['user_fullname']}',
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                      subtitle: ListTileSubtitleTagList(
                        tagList:
                            _newIssuePro.filteredSearchResultGroupMembers[index]
                                ['members_tag'],
                      ));
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  bool _checkIsFilterTagSelected(index) {
    Map tag = _newIssuePro.groupFilterTags[index];
    for (int i = 0; i < _newIssuePro.selectedGroupFilterTags.length; i++) {
      if (CreateNewIssueGroupProvider.areTagsSame(
          _newIssuePro.selectedGroupFilterTags[i], tag)) {
        return true;
      }
    }
    return false;
  }

  void _onTapFilterTag(int index) {
    _newIssuePro
        .setSelectedGroupFilterTags(_newIssuePro.groupFilterTags[index]);
  }

  String _getFilterTagLabel(int index) {
    return _newIssuePro.groupFilterTags[index]['tagName'];
  }

  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(color: primaryColor),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Filter with Tags",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }
}
