import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/searchGroupMembersPage.dart';
import 'package:iexist/src/screens/issuePage/createNewIssueGroup/searchGroupTags.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/user_permission_map_keys.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class Step1Page extends StatefulWidget {
  final TabController tabController;
  final dynamic groupDetails;

  const Step1Page({Key key, this.tabController, this.groupDetails})
      : super(key: key);

  @override
  _Step1PageState createState() => _Step1PageState();
}

class _Step1PageState extends State<Step1Page> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();

  @override
  Widget build(BuildContext context) {
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);

    bool creating = _newIssuePro.groupId == null;

    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _validate();
        },
        backgroundColor: primaryColor,
        child: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.white,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: null != null ? NetworkImage("") : null,
                    child: null == null
                        ? Icon(
                            Icons.camera_alt,
                            size: 30,
                            color: Colors.white,
                          )
                        : null,
                    radius: 30,
                    backgroundColor: Colors.black12,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: TextFormField(
                      controller: _newIssuePro.nameController,
                      decoration: InputDecoration(labelText: 'Ticket Name'),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Please provide a case subject and optional icon",
                    style: TextStyle(color: Colors.black54),
                    textAlign: TextAlign.left,
                  )),
            ),
            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: DropdownButton(
                isExpanded: true,
                value: _newIssuePro.status ??
                    _appProvider.selectedOrganizationTab[0]['name'],
                onChanged: (val) {
                  setState(() {
                    _newIssuePro.status = val;
                  });
                },
                hint: _newIssuePro.status == null
                    ? Text('Group status')
                    : Text(
                        _newIssuePro.status,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                items: List.generate(
                  _appProvider.selectedOrganizationTab.length,
                  (index) {
                    return DropdownMenuItem(
                      value: _appProvider.selectedOrganizationTab[index]
                          ['name'],
                      child: Text(
                        _appProvider.selectedOrganizationTab[index]['name'],
                      ),
                    );
                  },
                ),
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              color: Colors.black12,
              margin: EdgeInsets.only(top: 20.0),
              padding: EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      FocusScope.of(context).unfocus();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SearchGroupTags(),
                        ),
                      );
                    },
                    child: Container(
                      color: Colors.green,
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Text(
                        creating ? "Add Group Tags" : "Edit Group Tags",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Wrap(
                    spacing: 4,
                    runSpacing: 6,
                    children: List.generate(
                        _newIssuePro.selectedGroupTags.length, (index) {
                      return Chip(
                        backgroundColor: primaryColor,
                        label: Text(
                          _newIssuePro.selectedGroupTags[index]['tagName'],
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.black12,
              margin: EdgeInsets.only(top: 20.0),
              padding: EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Participants: ${_newIssuePro.selectedGroupMembers.length}",
                            style: TextStyle(color: Colors.black87),
                            textAlign: TextAlign.left,
                          )),
                      SizedBox(width: 15.0),
                      InkWell(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          if (_appProvider.isTheCurrentUserPrivilegedFor(
                                  UserPermissionMapKeys.canAddUsersInGroup,
                                  _userProvider.data) ||
                              _appProvider.isTheCurrentUserPrivilegedFor(
                                  UserPermissionMapKeys.canRemoveUsersInGroup,
                                  _userProvider.data)) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SearchGroupMembersPage(),
                              ),
                            );
                          } else {
                            Toast.show(
                              'You do not have the privilege edit the participants',
                              context,
                              backgroundColor: Colors.red,
                            );
                          }
                        },
                        child: Container(
                          color: Colors.green,
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          child: Text(
                            creating ? "Add Participants" : "Edit Participants",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      )
                    ],
                  ),
                  GridView.builder(
                    padding: EdgeInsets.only(top: 26),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      crossAxisSpacing: 4,
                      mainAxisSpacing: 4,
                    ),
                    itemCount: _newIssuePro.selectedGroupMembers.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: <Widget>[
                          CircleAvatar(
                            radius: 25,
                            backgroundColor: primaryColor,
                            child: Text(
                              '${_newIssuePro.selectedGroupMembers[index]['user_fullname']}'[
                                      0]
                                  .toUpperCase(),
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                            child: Text(
                              "${_newIssuePro.selectedGroupMembers[index]['user_fullname']}",
                              style: TextStyle(color: Colors.black54),
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _validate() async {
    FocusScope.of(context).unfocus();
    if (_newIssuePro.nameController.text.isEmpty ||
        _newIssuePro.nameController.text.length <= 3) {
      _errorMessage('Must enter a Group name');
    } else if (_newIssuePro.status == null) {
      _errorMessage('Select Group Status');
      // }else if(_newIssuePro.selectedGroupTags.isEmpty){
      //   _errorMessage('Select Group Tags');
    } else if (_newIssuePro.isEmptyMembers()) {
      _errorMessage('Select Group Members');
    } else {
      widget.tabController.index = 1;
    }
  }

  void _errorMessage(message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  }
}
