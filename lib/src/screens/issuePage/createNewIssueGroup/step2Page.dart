import 'dart:async';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:iexist/src/utilities/ui_helper.dart';
import 'package:iexist/src/utilities/user_permission_map_keys.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatScreenPage.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:pedantic/pedantic.dart';

class Step2Page extends StatefulWidget {
  @override
  _Step2PageState createState() => _Step2PageState();
}

String loggedInUser;
bool updating = false;
bool canEdit = true;

class _Step2PageState extends State<Step2Page> {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  DBHelper _dbHelper = DBHelper.instance;
  ChatScreenProvider _chatScreenProvider = ChatScreenProvider();
  final formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _chatScreenProvider = Provider.of<ChatScreenProvider>(context);
    UiHelper uiHelper = UiHelper();
    canEdit = _appProvider.isTheCurrentUserPrivilegedFor(
        UserPermissionMapKeys.canEditChatGroup, _userProvider.data);

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _verify();
        },
        backgroundColor: primaryColor,
        child: _isLoading
            ? CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              )
            : Icon(
                Icons.check,
                color: Colors.white,
              ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SwitchListTile(
                title: const Text('Very High Priority'),
                value: _newIssuePro.highPriority,
                onChanged: (bool value) {
                  setState(() {
                    _newIssuePro.highPriority = value;
                  });
                },
              ),
              TextFormField(
                readOnly: true,
                initialValue: _userProvider.userNameFullName,
                decoration: InputDecoration(labelText: 'Logged In user'),
              ),
              SizedBox(height: 8),
              // TextFormField(
              //   initialValue: _newIssuePro.complainant,
              //   validator: (value) {
              //     if (value.isEmpty || value.length < 3) {
              //       return "complainant must have minimum 3 characters";
              //     }
              //     return null;
              //   },
              //   onChanged: (val) {
              //     _newIssuePro.complainant = val;
              //   },
              //   maxLines: null,
              //   decoration: InputDecoration(labelText: 'Complainant'),
              // ),
              // SizedBox(height: 8),
              // TextFormField(
              //   initialValue: _newIssuePro.phone,
              //   validator: (value) {
              //     if (value.isEmpty || value.length != 10) {
              //       return "Enter a valid phone number";
              //     }
              //     return null;
              //   },
              //   onChanged: (val) {
              //     _newIssuePro.phone = val;
              //   },
              //   keyboardType: TextInputType.phone,
              //   decoration: InputDecoration(labelText: 'Phone'),
              // ),
              // SizedBox(height: 8),
              // InkWell(
              //   onTap: () {
              //     _getDateTime(i: 1, dateOnly: true);
              //   },
              //   child: Container(
              //     padding: EdgeInsets.only(bottom: 12),
              //     alignment: Alignment.bottomLeft,
              //     height: 45,
              //     decoration: BoxDecoration(
              //         border: Border(bottom: BorderSide(color: Colors.grey))),
              //     child: Text(
              //       _newIssuePro.complaintDate == null
              //           ? 'Complaint Date'
              //           : uiHelper.getDate(
              //               _newIssuePro.complaintDate.toString(),
              //             ),
              //       style: TextStyle(
              //           color: _newIssuePro.complaintDate == null
              //               ? Colors.black54
              //               : Colors.black,
              //           fontSize: 16),
              //     ),
              //   ),
              // ),
              SizedBox(height: 8),
              /** Additonal Fields Introduced START*/
              
              AbsorbPointer(
              absorbing: _newIssuePro.groupId == null ? false : true,
              child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Types Of Services',
                    style: TextStyle(fontSize: 15),),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.otherServiceTypeOfService == "" ? "Select" : _newIssuePro.otherServiceTypeOfService ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Covid Test","Diagnostic Test","Disinfection","Doctor Consultation","Bed allocation","Medical Equipment","Kit distribution",]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Types Of Services',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.otherServiceTypeOfService = value;
                  });
                },
              ),
                    ],
                  ),),
               TextFormField(
                 enabled: (_newIssuePro.otherServiceTypeOfService == "Disinfection" 
                          || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                          || _newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.otherServicePatientID,
                onChanged: (val) {
                  _newIssuePro.otherServicePatientID = val;
                },
                decoration: InputDecoration(
                    labelText: 'Beneficiary ID'),
              ),
              SizedBox(height: 8),
              TextFormField(
                initialValue: _newIssuePro.otherServicePatientName,
                onChanged: (val) {
                  _newIssuePro.otherServicePatientName = val;
                },
                decoration: InputDecoration(
                    labelText: 'Beneficiary Name'),
              ),
              SizedBox(height: 8),
              TextFormField(
                initialValue: _newIssuePro.patientAge,
                onChanged: (val) {
                  _newIssuePro.patientAge = val;
                },
                decoration: InputDecoration(labelText: 'Beneficiary Age'),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 8),
              TextFormField(
                initialValue: _newIssuePro.otherServicePhoneNo,
                onChanged: (val) {
                  _newIssuePro.otherServicePhoneNo = val;
                },
                decoration: InputDecoration(
                    labelText: 'Phone No'),
                    keyboardType: TextInputType.number,
              ),
               SizedBox(height: 8),
              TextFormField(
                enabled: _newIssuePro.otherServiceTypeOfService == "Bed allocation" ? true : false,
                initialValue: _newIssuePro.attenderName,
                onChanged: (val) {
                  _newIssuePro.attenderName = val;
                },
                decoration: InputDecoration(labelText: 'Alternative Contact Person'),
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: _newIssuePro.otherServiceTypeOfService == "Bed allocation" ? true : false,
                initialValue: _newIssuePro.attenderMobileno,
                onChanged: (val) {
                  _newIssuePro.attenderMobileno = val;
                },
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(labelText: 'Alternative Contact Number'),
              ),
              TextFormField(
                initialValue: _newIssuePro.otherServicePatientAddress,
                onChanged: (val) {
                  _newIssuePro.otherServicePatientAddress = val;
                },
                decoration: InputDecoration(
                    labelText: 'Beneficiary  Address (Hs/Street)'),   
              ),
              TextFormField(
                minLines: 3,
                maxLines: 3,
                initialValue: _newIssuePro.address1,
                onChanged: (val) {
                  _newIssuePro.address1 = val;
                },
                decoration: InputDecoration(labelText: 'Area / Locality'),
              ),
               TextFormField(
                initialValue: _newIssuePro.pinCode,
                onChanged: (val) {
                  _newIssuePro.pinCode = val;
                },
                decoration: InputDecoration(labelText: 'Pin Code'),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 20),
              TextFormField(
                initialValue: _newIssuePro.otherServiceLocation,
                onChanged: (val) {
                  _newIssuePro.otherServiceLocation = val;
                },
                decoration: InputDecoration(
                    labelText: 'Location'),
              ),
              Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Service Status',),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.otherServiceStatus == "" ? "Select" : _newIssuePro.otherServiceStatus ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Not Started","Ongoing","Completed"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Service Status',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.otherServiceStatus = value;
                  });
                },
              ),
                    ],
                  ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Covid Test" 
                || _newIssuePro.otherServiceTypeOfService == "Disinfection"
                || _newIssuePro.otherServiceTypeOfService == "Bed allocation"
                || _newIssuePro.otherServiceTypeOfService == "Medical Equipment") ? true : false,
                initialValue: _newIssuePro.otherServiceCoordinatorInCharge,
                onChanged: (val) {
                  _newIssuePro.otherServiceCoordinatorInCharge = val;
                },
                decoration: InputDecoration(
                    labelText: 'Co-ordinator in Charge'),
              ),
              SizedBox(height: 8),
              AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                || _newIssuePro.otherServiceTypeOfService == "Bed allocation") ? false : true,
                child: InkWell(
                onTap: () {
                  _getDateTime(i: 5, dateOnly: false);
                },
                child: Container(
                  padding: EdgeInsets.only(bottom: 12),
                  alignment: Alignment.bottomLeft,
                  height: 45,
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Text(
                    _newIssuePro.preferredVisitDataAndTimeTo == null
                        ? 'Date/Time of Request:'
                        : uiHelper.getDateTime(
                            _newIssuePro.preferredVisitDataAndTimeTo.toString(),
                          ),
                    style: TextStyle(
                      color: _newIssuePro.preferredVisitDataAndTimeTo == null
                          ? Colors.black54
                          : Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),),
              InkWell(
                onTap: () {
                  _getDateTime(i: 3, dateOnly: false, stateKey:  "otherServiceDateTimeOfService");
                },
                child: Container(
                  padding: EdgeInsets.only(bottom: 12),
                  alignment: Alignment.bottomLeft,
                  height: 45,
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Text(
                    _newIssuePro.otherServiceDateTimeOfService == null
                        ? 'Date / Time of Service'
                        : uiHelper.getDateTime(
                            _newIssuePro.otherServiceDateTimeOfService.toString(),
                          ),
                    style: TextStyle(
                      color: _newIssuePro.otherServiceDateTimeOfService == null
                          ? Colors.black54
                          : Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8),
               TextFormField(
                initialValue: _newIssuePro.otherServiceDatesIfAny,
                onChanged: (val) {
                  _newIssuePro.otherServiceDatesIfAny = val;
                },
                decoration: InputDecoration(
                    labelText: 'Other Dates if Any'),
              ),
              TextFormField(
                maxLines: 3,
                minLines: 3,
                initialValue: _newIssuePro.otherServiceRemarksOrComments,
                onChanged: (val) {
                  _newIssuePro.otherServiceRemarksOrComments = val;
                },
                decoration: InputDecoration(labelText: 'Remarks/Comments'),
              ),
              ExpansionTile(
                title: Text("Health Care Services"),
              children: [
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Covid Test") ? true : false,
                initialValue: _newIssuePro.otherServiceVendor,
                onChanged: (val) {
                  _newIssuePro.otherServiceVendor = val;
                },
                decoration: InputDecoration(
                    labelText: 'Vendor'),
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Doctor Consultation") ? true : false,
                initialValue: _newIssuePro.otherServiceConsultingDoctor,
                onChanged: (val) {
                  _newIssuePro.otherServiceConsultingDoctor = val;
                },
                decoration: InputDecoration(
                    labelText: 'Consulting Doctor (In the case of Doctor Consultation)'),
              ),
              SizedBox(height: 8),
              TextFormField(
                initialValue: _newIssuePro.otherServiceEquipmentID,
                onChanged: (val) {
                  _newIssuePro.otherServiceEquipmentID = val;
                },
                decoration: InputDecoration(
                  enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                  || _newIssuePro.otherServiceTypeOfService == "Kit distribution") ? true : false,      
                    labelText: 'Equipment ID  (in the case of Equipments)'),
              ),
                AbsorbPointer(
                  absorbing: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                  || _newIssuePro.otherServiceTypeOfService == "Kit distribution") ? false : true,
                  child: Wrap(
                 alignment: WrapAlignment.start,
                    children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child:Text("Equipment Type(In the case of Equipments)"),),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.otherServiceEquipmentType == "" ? "Select" : _newIssuePro.otherServiceEquipmentType ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Oxygen Concentrator", "Oxygen Cylinder", "Other Equipments"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.otherServiceEquipmentType = value;
                  });
                },
              ),
                    ],
                  ),),
              SizedBox(height: 8),
               TextFormField(
                 enabled: _newIssuePro.otherServiceTypeOfService == "Diagnostic Test" ? true : false,
                initialValue: _newIssuePro.otherServiceNamesOfTest,
                onChanged: (val) {
                  _newIssuePro.otherServiceNamesOfTest = val;
                },
                decoration: InputDecoration(
                    labelText: 'Names Of Test (In the case of Diagnostic)'),
              ),
              SizedBox(height: 20),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Diagnostic Test" ||
                _newIssuePro.otherServiceTypeOfService == "Covid Test" ) ? true : false,
                initialValue: _newIssuePro.otherServiceNoOfTestConducted,
                onChanged: (val) {
                  _newIssuePro.otherServiceNoOfTestConducted = val;
                },
                decoration: InputDecoration(
                    labelText: 'Number of test conducted (In case of Covid Test)'),
                    keyboardType: TextInputType.number,
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: _newIssuePro.otherServiceTypeOfService == "Disinfection" ? true : false,
                initialValue: _newIssuePro.otherServiceAreaSquareFeet,
                onChanged: (val) {
                  _newIssuePro.otherServiceAreaSquareFeet = val;
                },
                decoration: InputDecoration(
                    labelText: 'Area - Square feet (In the case of sanitization)'),
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution") ? true : false,  
                initialValue: _newIssuePro.otherServiceNoOfEquipmentsIssued,
                onChanged: (val) {
                  _newIssuePro.otherServiceNoOfEquipmentsIssued = val;
                },
                decoration: InputDecoration(
                    labelText: 'Number of Equipments Issued'),
                    keyboardType: TextInputType.number,
              ),
              SizedBox(height: 20),
              AbsorbPointer(
              absorbing: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation") ? false : true,  
              child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text("Paid/Free"),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.otherServicePaidOrFree == "" ? "Select" : _newIssuePro.otherServicePaidOrFree,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Paid", "Free",]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  "Paid/Free",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.otherServicePaidOrFree = value;
                  });
                },
              ),
                    ],
                  ),),
               TextFormField(
                 enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceRate,
                onChanged: (val) {
                  _newIssuePro.otherServiceRate = val;
                },
                decoration: InputDecoration(
                    labelText: 'Rate'),
                    keyboardType: TextInputType.number,
              ),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceTotalCost,
                onChanged: (val) {
                  _newIssuePro.otherServiceTotalCost = val;
                },
                decoration: InputDecoration(
                    labelText: 'Total Cost'),
                    keyboardType: TextInputType.number,
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceCollectedBy,
                onChanged: (val) {
                  _newIssuePro.otherServiceCollectedBy = val;
                },
                decoration: InputDecoration(labelText: 'Collected By'),
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceAmountCollected,
                onChanged: (val) {
                  _newIssuePro.otherServiceAmountCollected = val;
                },
                decoration: InputDecoration(labelText: 'Amount collected'),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 8),
              TextFormField(enabled: (  _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test") ? true : false,
                initialValue: _newIssuePro.otherServiceCombinedPaymentDetails,
                onChanged: (val) {
                  _newIssuePro.otherServiceCombinedPaymentDetails = val;
                },
                decoration: InputDecoration(labelText: 'Combined Payment Details'),
              ),
              SizedBox(height: 8),
              AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? false : true,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Collection Mode',),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.otherServiceCollectionMode == "" ? "Select" : _newIssuePro.otherServiceCollectionMode ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Cash","Google Pay","Other Online payment"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Collection Mode',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.otherServiceCollectionMode = value;
                  });
                },
              ),
                    ],
                  ),),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceTransactionID,
                onChanged: (val) {
                  _newIssuePro.otherServiceTransactionID = val;
                },
                decoration: InputDecoration(labelText: 'Transaction ID'),
              ),
              SizedBox(height: 8),
              AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? false : true,
                child:InkWell(
                onTap: () {
                  _getDateTime(i: 4, dateOnly: true);
                },
                child: Container(
                  padding: EdgeInsets.only(bottom: 12),
                  alignment: Alignment.bottomLeft,
                  height: 45,
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Text(
                    _newIssuePro.otherServicePaymentDate == null
                        ? 'Payment Date'
                        : uiHelper.getDate(
                            _newIssuePro.otherServicePaymentDate
                                .toString(),
                          ),
                    style: TextStyle(
                        color:
                            _newIssuePro.otherServicePaymentDate == null
                                ? Colors.black54
                                : Colors.black,
                        fontSize: 16),
                  ),
                ),
              ),),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServicePaymentStatus,
                onChanged: (val) {
                  _newIssuePro.otherServicePaymentStatus = val;
                },
                decoration: InputDecoration(labelText: 'Payment Status'),
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceSurplus,
                onChanged: (val) {
                  _newIssuePro.otherServiceSurplus = val;
                },
                decoration: InputDecoration(labelText: 'Surplus'),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Medical Equipment"
                      || _newIssuePro.otherServiceTypeOfService == "Kit distribution" 
                      || _newIssuePro.otherServiceTypeOfService == "Doctor Consultation" 
                      || _newIssuePro.otherServiceTypeOfService == "Covid Test" 
                      || _newIssuePro.otherServiceTypeOfService == "Diagnostic Test"
                      || _newIssuePro.otherServiceTypeOfService == "Disinfection") ? true : false,
                initialValue: _newIssuePro.otherServiceShortage,
                onChanged: (val) {
                  _newIssuePro.otherServiceShortage = val;
                },
                decoration: InputDecoration(labelText: 'Shortage'),
                keyboardType: TextInputType.number,
              ),
              ],),
              SizedBox( height: 20,),
              ExpansionTile( 
                title: Text("Bed Allocation"),
                children:[
                AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? false : true,
                child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Patient Location',),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.bedAllocationPatientLocation == "" ? "Select" : _newIssuePro.bedAllocationPatientLocation ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Home","Hospital"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Patient Location',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.bedAllocationPatientLocation = value;
                  });
                },
              ),
                    ],
                  ),),
              // SizedBox(height: 8),
              // TextFormField(
              //   enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
              //   initialValue: _newIssuePro.relationToPatient,
              //   onChanged: (val) {
              //     _newIssuePro.relationToPatient = val;
              //   },
              //   decoration:
              //       InputDecoration(labelText: 'Relation to the Patient'),
              // ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.bloodGroup,
                onChanged: (val) {
                  _newIssuePro.bloodGroup = val;
                },
                decoration: InputDecoration(labelText: 'Blood Group'),
              ),
              SizedBox(height: 8),
              AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? false : true,
                child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Covid Result',),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.covidResult == "" ? "Select" : _newIssuePro.covidResult ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Positive","Negative"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Covid Result',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.covidResult = value;
                  });
                },
              ),
                    ],
                  ),),
              SizedBox(height: 8),
                TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.symptoms,
                onChanged: (val) {
                  _newIssuePro.symptoms = val;
                },
                decoration: InputDecoration(labelText: 'Symptoms'),
              ),
              SizedBox(height: 8),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.sinceHowManyDays,
                onChanged: (val) {
                  _newIssuePro.sinceHowManyDays = val;
                },
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Since how many days'),
              ),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.spo2Level,
                onChanged: (val) {
                  _newIssuePro.spo2Level = val;
                },
                decoration: InputDecoration(labelText: 'SPO2 Level'),
              ),
              SizedBox(height: 8),
              AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? false : true,
                child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Is patient on Oxygen Cylinder?'),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.isOnOxygenCylinder == "" ? "Select" : _newIssuePro.isOnOxygenCylinder ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","Yes","No"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Oxygen Cylinder',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.isOnOxygenCylinder = value;
                  });
                },
              ),
                    ],
                  ),),
                  SizedBox(height: 10,),
               AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? false : true,
                child:InkWell(
                onTap: () {
                  _getDateTime(i: 2, dateOnly: false);
                },
                child: Container(
                  padding: EdgeInsets.only(bottom: 12),
                  alignment: Alignment.bottomLeft,
                  height: 45,
                  decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                  child: Text(
                    _newIssuePro.preferredVisitDataAndTimeFrom == null
                        ? 'Searching Hospital Bed Since?'
                        : uiHelper.getDateTime(
                            _newIssuePro.preferredVisitDataAndTimeFrom
                                .toString(),
                          ),
                    style: TextStyle(
                        color:
                            _newIssuePro.preferredVisitDataAndTimeFrom == null
                                ? Colors.black54
                                : Colors.black,
                        fontSize: 16),
                  ),
                ),
              ),),
              SizedBox(height: 8),

              AbsorbPointer(
                absorbing: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? false : true,
                child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Text('Type of Bed Required',),
                    DropdownButton<String>(
                focusColor:Colors.white,
                value: _newIssuePro.typeOfBedRequied == "" ? "Select" : _newIssuePro.typeOfBedRequied ,
                elevation: 5,
                style: TextStyle(color: Colors.white),
                iconEnabledColor:Colors.black,
                items: <String>["Select","General","HDU","ICU"]
                .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,style:TextStyle(color:Colors.black),),
                  );
                }).toList(),
                hint:Text(
                  'Bed Required ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                onChanged: (String value) {
                  setState(() {
                    _newIssuePro.typeOfBedRequied = value;
                  });
                },
              ),
                    ],
                  ),),
              SizedBox(height: 8),
               TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                minLines: 3,
                maxLines: 3,
                initialValue: _newIssuePro.address2,
                onChanged: (val) {
                  _newIssuePro.address2 = val;
                },
                decoration:
                    InputDecoration(labelText: 'List of Hospitals Tried'),
              ),
              SizedBox(height: 8),
              TextFormField(
                minLines: 3,
                maxLines: 3,
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.hospitalPreference,
                onChanged: (val) {
                  _newIssuePro.hospitalPreference = val;
                },
                decoration: InputDecoration(labelText: 'Hospital Preference'),
              ),
              SizedBox(height: 20),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.srfId,
                onChanged: (val) {
                  _newIssuePro.srfId = val;
                },
                decoration: InputDecoration(labelText: 'SRF ID'),
              ),
              TextFormField(
                enabled: (_newIssuePro.otherServiceTypeOfService == "Bed allocation") ? true : false,
                initialValue: _newIssuePro.buNumber,
                onChanged: (val) {
                  _newIssuePro.buNumber = val;
                },
                decoration: InputDecoration(labelText: 'BU Number'),
              ),
              SizedBox(height: 8),
              /** Additonal Fields Introduced START*/
              // SizedBox(height: 8),
              // TextFormField(
              //   initialValue: _newIssuePro.bedAllocationRemarks,
              //   onChanged: (val) {
              //     _newIssuePro.bedAllocationRemarks = val;
              //   },
              //   decoration: InputDecoration(labelText: 'Remarks'),
              // ),
              // SizedBox(height: 18),
                ]),
              ],
          ),
        ),
      ),
    );
  }

  Future _verify() async {
    if (_isLoading) {
      return false;
    }

    updating = _newIssuePro.groupId != null;

    if (updating && !canEdit) {
      Toast.show(
        'You do not have the privilege to edit this group',
        context,
        backgroundColor: Colors.red,
      );

      return;
    } else if (updating &&
        _chatScreenProvider.isReadOnly(_appProvider.config)) {
      Toast.show(
        'You cannot edit this group',
        context,
        backgroundColor: Colors.red,
      );

      return;
    }

    FocusScope.of(context).unfocus();

    if (_newIssuePro.nameController.text.isEmpty ||
            _newIssuePro.nameController.text.length <= 3 ||
            _newIssuePro.isEmptyMembers() ||
            _newIssuePro.status ==
                null /*||
        _newIssuePro.selectedGroupTags.isEmpty*/
        ) {
      _newIssuePro.tabController.index = 0;
      Toast.show(
        'Please complete step 1',
        context,
        backgroundColor: Colors.red,
      );
    } else {
//      if (formKey.currentState.validate()) {
//        formKey.currentState.save();
//        _createGroupToLocalDB();
//      }
      setState(() {
        _isLoading = true;
      });
      _userProvider.setSaving(true);
      if (_newIssuePro.groupId == null) {
        await _createGroupToLocalDB();
      } else {
        _updateGroupFromLocalDB();
      }
    }
  }

  void _getDateTime({int i, bool dateOnly, String stateKey}) async {
    var selectedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2050),
    );
    if (selectedDate == null) {
      return;
    }
    if (dateOnly) { 
      if(i == 4){
            setState(() {
              _newIssuePro.otherServicePaymentDate = selectedDate;
                        });
        }
      else if(i == 1){
        setState(() {
        _newIssuePro.complaintDate = selectedDate;
      });
      PrintString(_newIssuePro.complaintDate);
      }
    } else {
      _getTime(i, selectedDate);
    }
  }

  void _getTime(int i, DateTime selectedDate) async {
    var time =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (time != null) {
      DateTime dateTime = DateTime(selectedDate.year, selectedDate.month,
          selectedDate.day, time.hour, time.minute);
      String errorMessage;

      if (i == 2) {
          setState(() {
            _newIssuePro.preferredVisitDataAndTimeFrom = dateTime;
          });
        } else if(i == 3){
        setState(() {
            _newIssuePro.otherServiceDateTimeOfService = dateTime;
          });
        }else if(i == 5) {
          setState(() {
            _newIssuePro.preferredVisitDataAndTimeTo = dateTime;
          });
        }

      if (null != errorMessage) {
        Toast.show(
          errorMessage,
          context,
          backgroundColor: Colors.red,
        );
      }
    }
  }

  void _updateGroupFromLocalDB() async {
    List selectedGroupTag = _newIssuePro.selectedGroupTags;

    await _dbHelper.deleteGroupMembers(groupId: _newIssuePro.groupId);

    for (Map member in _newIssuePro.allMembers()) {
      await _dbHelper.insertGroupMembers(
        timeStamp: await getCurrentSec(local: true),
        userName: member['user_name'],
        userFullName: member['user_fullname'],
        syncStatus: 'pending',
        userId: member['user_id'],
        groupId: _newIssuePro.groupId,
        id: Random().nextInt(100000).toString(),
        userType: member['user_type'],
      );
    }

    await _dbHelper.clearGroupTags(_newIssuePro.groupId);

    for (int i = 0; i < selectedGroupTag.length; i++) {
      await _dbHelper.insertGroupTag(
        groupId: _newIssuePro.groupId,
        tagCategory: selectedGroupTag[i]['tagCategory'],
        tagName: selectedGroupTag[i]['tagName'],
        tagId: selectedGroupTag[i]['tagId'],
      );
    }

    await _dbHelper.updateGroupData(
      currentGroupId: _newIssuePro.groupId,
      newGroupId: _newIssuePro.groupId,
      name: _newIssuePro.nameController.text,
      issueNo: _newIssuePro.issueNo,
      complainant: _newIssuePro.complainant,
      address1: _newIssuePro.address1,
      address2: _newIssuePro.address2,
      //Introducing New Fields START
      patientName: _newIssuePro.patientName,
      patientAge: _newIssuePro.patientAge,
      symptoms: _newIssuePro.symptoms,
      sinceHowManyDays: _newIssuePro.sinceHowManyDays,
      spo2Level: _newIssuePro.spo2Level,
      isOnOxygenCylinder: _newIssuePro.isOnOxygenCylinder,
      covidResult: _newIssuePro.covidResult,
      hospitalPreference: _newIssuePro.hospitalPreference,
      attenderName: _newIssuePro.attenderName,
      attenderMobileno: _newIssuePro.attenderMobileno,
      relationToPatient: _newIssuePro.relationToPatient,
      srfId: _newIssuePro.srfId,
      buNumber: _newIssuePro.buNumber,
      bloodGroup: _newIssuePro.bloodGroup,
      typeOfBedRequied: _newIssuePro.typeOfBedRequied,
      otherServicePatientID: _newIssuePro.otherServicePatientID,
      otherServicePatientName: _newIssuePro.otherServicePatientName,
      otherServiceLocation: _newIssuePro.otherServiceLocation,
      otherServicePhoneNo: _newIssuePro.otherServicePhoneNo,
      otherServicePatientAddress: _newIssuePro.otherServicePatientAddress,
      otherServiceStatus: _newIssuePro.otherServiceStatus,
      otherServiceCoordinatorInCharge: _newIssuePro.otherServiceCoordinatorInCharge,
      otherServiceTypeOfService: _newIssuePro.otherServiceTypeOfService,
      otherServiceDateTimeOfService: _newIssuePro.otherServiceDateTimeOfService != null
              ? _newIssuePro
                      .otherServiceDateTimeOfService.millisecondsSinceEpoch ~/
                  1000
              : null,
      otherServiceDatesIfAny: _newIssuePro.otherServiceDatesIfAny,
      otherServiceVendor: _newIssuePro.otherServiceVendor,
      otherServiceConsultingDoctor:_newIssuePro.otherServiceConsultingDoctor,
      otherServiceEquipmentID: _newIssuePro.otherServiceEquipmentID,
      otherServiceEquipmentType: _newIssuePro.otherServiceEquipmentType,
      otherServiceNamesOfTest: _newIssuePro.otherServiceNamesOfTest,
      otherServiceNoOfTestConducted: _newIssuePro.otherServiceNoOfTestConducted,
      otherServiceAreaSquareFeet: _newIssuePro.otherServiceAreaSquareFeet,
      otherServiceNoOfEquipmentsIssued: _newIssuePro.otherServiceNoOfEquipmentsIssued,
      otherServicePaidOrFree: _newIssuePro.otherServicePaidOrFree,
      otherServiceRate: _newIssuePro.otherServiceRate,
      otherServiceTotalCost: _newIssuePro.otherServiceTotalCost,
      otherServiceCollectedBy:_newIssuePro.otherServiceCollectedBy,
      otherServiceAmountCollected: _newIssuePro.otherServiceAmountCollected,
      otherServiceCombinedPaymentDetails: _newIssuePro.otherServiceCombinedPaymentDetails,
      otherServiceCollectionMode: _newIssuePro.otherServiceCollectionMode,
      otherServiceTransactionID: _newIssuePro.otherServiceTransactionID,
      otherServicePaymentDate: _newIssuePro.otherServicePaymentDate != null
              ? _newIssuePro
                      .otherServicePaymentDate.millisecondsSinceEpoch ~/
                  1000
              : null,
      otherServicePaymentStatus: _newIssuePro.otherServicePaymentStatus,
      otherServiceSurplus: _newIssuePro.otherServiceSurplus,
      otherServiceShortage: _newIssuePro.otherServiceShortage,
      otherServiceRemarksOrComments: _newIssuePro.otherServiceRemarksOrComments,
      bedAllocationPatientLocation: _newIssuePro.bedAllocationPatientLocation,
      bedAllocationRemarks: _newIssuePro.bedAllocationRemarks,

      //Introducing New Fields START
      preferredVisitDateAndTimeFrom:
          _newIssuePro.preferredVisitDataAndTimeFrom != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeFrom.millisecondsSinceEpoch ~/
                  1000
              : null,
      timestamp: await getCurrentSec(local: true),
      orgId: _appProvider.selectedOrganization['_id'],
      complaintDate: _newIssuePro.complaintDate != null
          ? _newIssuePro.complaintDate.millisecondsSinceEpoch ~/ 1000
          : null,
      groupStatus: _newIssuePro.status,
      highPriority: _newIssuePro.highPriority ? 1 : 0,
      phone: _newIssuePro.phone,
      pinCode: _newIssuePro.pinCode,
      preferredVisitDateAndTimeFromTo:
          _newIssuePro.preferredVisitDataAndTimeTo != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeTo.millisecondsSinceEpoch ~/
                  1000
              : null,
      syncStatus: 'pending',
    );
    if (_appProvider.isOnline) {
      await createGroupToRemoteDB(_newIssuePro.groupId);
    } else {
      await _updateChatScreen(_newIssuePro.groupId);
    }
    _newIssuePro.clearAllData(_appProvider);
  }

  Future _createGroupToLocalDB() async {
    String tempGroupId = "temp" + Random().nextInt(100000).toString();

    List selectedGroupMembers = _newIssuePro.selectedGroupMembers;
    List selectedGroupTag = _newIssuePro.selectedGroupTags;
    for (int i = 0; i < selectedGroupMembers.length; i++) {
      await _dbHelper.insertGroupMembers(
        timeStamp: await getCurrentSec(local: true),
        userName: selectedGroupMembers[i]['user_name'],
        userFullName: selectedGroupMembers[i]['user_fullname'],
        syncStatus: 'pending',
        userId: selectedGroupMembers[i]['user_id'],
        groupId: tempGroupId,
        id: "temp" + Random().nextInt(100000).toString(),
        userType: selectedGroupMembers[i]['user_type'],
      );
    }

    for (int i = 0; i < selectedGroupTag.length; i++) {
      await _dbHelper.insertGroupTag(
        groupId: tempGroupId,
        tagCategory: selectedGroupTag[i]['tagCategory'],
        tagName: selectedGroupTag[i]['tagName'],
        tagId: selectedGroupTag[i]['tagId'],
      );
    }

    await _dbHelper.insertGroup(
      groupId: tempGroupId,
      name: _newIssuePro.nameController.text,
      issueNo: null,
      complainant: _newIssuePro.complainant,
      //Addtion Fields START
      patientName: _newIssuePro.patientName,
      patientAge: _newIssuePro.patientAge,
      symptoms: _newIssuePro.symptoms,
      sinceHowManyDays: _newIssuePro.sinceHowManyDays,
      spo2Level: _newIssuePro.spo2Level,
      isOnOxygenCylinder: _newIssuePro.isOnOxygenCylinder,
      covidResult: _newIssuePro.covidResult,
      hospitalPreference: _newIssuePro.hospitalPreference,
      attenderName: _newIssuePro.attenderName,
      attenderMobileno: _newIssuePro.attenderMobileno,
      relationToPatient: _newIssuePro.relationToPatient,
      srfId: _newIssuePro.srfId,
      buNumber: _newIssuePro.buNumber,
      bloodGroup: _newIssuePro.bloodGroup,
      typeOfBedRequied: _newIssuePro.typeOfBedRequied,
      otherServicePatientID: _newIssuePro.otherServicePatientID,
      otherServicePatientName: _newIssuePro.otherServicePatientName,
      otherServiceLocation: _newIssuePro.otherServiceLocation,
      otherServicePhoneNo: _newIssuePro.otherServicePhoneNo,
      otherServicePatientAddress: _newIssuePro.otherServicePatientAddress,
      otherServiceStatus: _newIssuePro.otherServiceStatus,
      otherServiceCoordinatorInCharge: _newIssuePro.otherServiceCoordinatorInCharge,
      otherServiceTypeOfService: _newIssuePro.otherServiceTypeOfService,
      otherServiceDateTimeOfService: _newIssuePro.otherServiceDateTimeOfService != null
              ? _newIssuePro
                      .otherServiceDateTimeOfService.millisecondsSinceEpoch ~/
                  1000
              : null,
      otherServiceDatesIfAny: _newIssuePro.otherServiceDatesIfAny,
      otherServiceVendor: _newIssuePro.otherServiceVendor,
      otherServiceConsultingDoctor:_newIssuePro.otherServiceConsultingDoctor,
      otherServiceEquipmentID: _newIssuePro.otherServiceEquipmentID,
      otherServiceEquipmentType: _newIssuePro.otherServiceEquipmentType,
      otherServiceNamesOfTest: _newIssuePro.otherServiceNamesOfTest,
      otherServiceNoOfTestConducted: _newIssuePro.otherServiceNoOfTestConducted,
      otherServiceAreaSquareFeet: _newIssuePro.otherServiceAreaSquareFeet,
      otherServiceNoOfEquipmentsIssued: _newIssuePro.otherServiceNoOfEquipmentsIssued,
      otherServicePaidOrFree: _newIssuePro.otherServicePaidOrFree,
      otherServiceRate: _newIssuePro.otherServiceRate,
      otherServiceTotalCost: _newIssuePro.otherServiceTotalCost,
      otherServiceCollectedBy:_newIssuePro.otherServiceCollectedBy,
      otherServiceAmountCollected: _newIssuePro.otherServiceAmountCollected,
      otherServiceCombinedPaymentDetails: _newIssuePro.otherServiceCombinedPaymentDetails,
      otherServiceCollectionMode: _newIssuePro.otherServiceCollectionMode,
      otherServiceTransactionID: _newIssuePro.otherServiceTransactionID,
      otherServicePaymentDate: _newIssuePro.otherServicePaymentDate != null
              ? _newIssuePro
                      .otherServicePaymentDate.millisecondsSinceEpoch ~/
                  1000
              : null,
      otherServicePaymentStatus: _newIssuePro.otherServicePaymentStatus,
      otherServiceSurplus: _newIssuePro.otherServiceSurplus,
      otherServiceShortage: _newIssuePro.otherServiceShortage,
      otherServiceRemarksOrComments: _newIssuePro.otherServiceRemarksOrComments,
      bedAllocationPatientLocation: _newIssuePro.bedAllocationPatientLocation,
      bedAllocationRemarks: _newIssuePro.bedAllocationRemarks,
      //Addtion Fields END
      address1: _newIssuePro.address1,
      address2: _newIssuePro.address2,
      preferredVisitDateAndTimeFrom:
          _newIssuePro.preferredVisitDataAndTimeFrom != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeFrom.millisecondsSinceEpoch ~/
                  1000
              : null,
      timestamp: await getCurrentSec(local: true),
      orgId: _appProvider.selectedOrganization['_id'],
      complaintDate: _newIssuePro.complaintDate != null
          ? _newIssuePro.complaintDate.millisecondsSinceEpoch ~/ 1000
          : null,
      groupStatus: _newIssuePro.status,
      highPriority: _newIssuePro.highPriority ? 1 : 0,
      phone: _newIssuePro.phone,
      pinCode: _newIssuePro.pinCode,
      preferredVisitDateAndTimeFromTo:
          _newIssuePro.preferredVisitDataAndTimeTo != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeTo.millisecondsSinceEpoch ~/
                  1000
              : null,
      syncStatus: _appProvider.isOnline ? 'processing' : 'pending',
    );
    if (_appProvider.isOnline) {
      await createGroupToRemoteDB(tempGroupId);
    } else {
      await _updateChatListScreenAndNavigateToNewChatScreen(
          newChatGroupId: tempGroupId);
    }
  }

  createGroupToRemoteDB(String tempGroupId) async {
    if (!mounted) return;

    Dio dio = Dio();
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["Authorization"] = _userProvider.token;
    List membersSaved =
        await _dbHelper.getGroupMembersByGroupIdData(tempGroupId);
    List groupTagsSaved = await _dbHelper.getGroupTagByGroupIdData(tempGroupId);
    List members = [];
    List groupTags = [];
    for (int i = 0; i < groupTagsSaved.length; i++) {
      groupTags.add({
        "_id": groupTagsSaved[i]['tag_id'],
        "tag_name": groupTagsSaved[i]['tagName'],
        "tag_category": groupTagsSaved[i]['tag_category']
      });
    }
    for (int i = 0; i < membersSaved.length; i++) {
      members.add(
        {
          "_id": membersSaved[i]['_id'],
          "user_id": membersSaved[i]['user_id'],
          "user_name": membersSaved[i]['user_name'],
          "user_fullname": membersSaved[i]['user_fullname'],
          "user_type": "MEMBER",
          "group_id": tempGroupId,
        },
      );
    }

    Map body = {
      "app_group_id": tempGroupId,
      "name": _newIssuePro.nameController.text,
      "icon": "",
      "details": "",
      "organization_id": _appProvider.selectedOrganization['_id'],
      'organization_name': _appProvider.getOrgName(_userProvider.data),
      "user_id": _userProvider.userId,
      "user_name": _userProvider.userName,
      "user_fullname": _userProvider.userNameFullName,
      "group_status": _newIssuePro.status,
      "high_priority": _newIssuePro.highPriority,
      "phone_no": _newIssuePro.phone,
      "complainant": _newIssuePro.complainant,
      "complaint_date": _newIssuePro.complaintDate != null
          ? _newIssuePro.complaintDate.millisecondsSinceEpoch ~/ 1000
          : null,
      //Introducing New Fields START
      "patient_name": _newIssuePro.patientName,
      "patient_age": _newIssuePro.patientAge,
      "symptoms": _newIssuePro.symptoms,
      "since_how_many_days": _newIssuePro.sinceHowManyDays,
      "spo2_level": _newIssuePro.spo2Level,
      "is_on_oxygen_cylinder": _newIssuePro.isOnOxygenCylinder,
      "covid_result": _newIssuePro.covidResult,
      "hospital_preference": _newIssuePro.hospitalPreference,
      "attender_name": _newIssuePro.attenderName,
      "attender_mobileno": _newIssuePro.attenderMobileno,
      "relation_to_patient": _newIssuePro.relationToPatient,
      "srf_id": _newIssuePro.srfId,
      "bu_number": _newIssuePro.buNumber,
      "blood_group": _newIssuePro.bloodGroup,
      "type_of_bed_required": _newIssuePro.typeOfBedRequied,
      "other_service_patient_id": _newIssuePro.otherServicePatientID,
      "other_service_patient_name": _newIssuePro.otherServicePatientName,
      "other_service_location": _newIssuePro.otherServiceLocation,
      "other_service_phoneno": _newIssuePro.otherServicePhoneNo,
      "other_service_patient_address": _newIssuePro.otherServicePatientAddress,
      "other_service_status": _newIssuePro.otherServiceStatus,
      "other_service_coordinator_in_charge": _newIssuePro.otherServiceCoordinatorInCharge,
      "other_service_type_of_service": _newIssuePro.otherServiceTypeOfService,
      "other_service_date_time_of_service": _newIssuePro.otherServiceDateTimeOfService != null
              ? _newIssuePro
                      .otherServiceDateTimeOfService.millisecondsSinceEpoch ~/
                  1000
              : null,
      "other_service_dates_if_any": _newIssuePro.otherServiceDatesIfAny,
      "other_service_vendor": _newIssuePro.otherServiceVendor,
      "other_service_consulting_doctor": _newIssuePro.otherServiceConsultingDoctor,
      "other_service_equipment_id": _newIssuePro.otherServiceEquipmentID,
      "other_service_equipment_type": _newIssuePro.otherServiceEquipmentType,
      "other_service_names_of_test": _newIssuePro.otherServiceNamesOfTest,
      "other_service_no_of_test_conducted": _newIssuePro.otherServiceNoOfTestConducted,
      "other_service_area_square_feet": _newIssuePro.otherServiceAreaSquareFeet,
      "other_service_no_of_equipment_issued": _newIssuePro.otherServiceNoOfEquipmentsIssued,
      "other_service_paid_or_free": _newIssuePro.otherServicePaidOrFree,
      "other_service_rate": _newIssuePro.otherServiceRate,
      "other_service_total_cost": _newIssuePro.otherServiceTotalCost,
      "other_service_collected_by": _newIssuePro.otherServiceCollectedBy,
      "other_service_amount_collected": _newIssuePro.otherServiceAmountCollected,
      "other_service_combined_payment_details": _newIssuePro.otherServiceCombinedPaymentDetails,
      "other_service_collection_mode" : _newIssuePro.otherServiceCollectionMode,
      "other_service_transaction": _newIssuePro.otherServiceTransactionID,
      "other_service_payment_date": _newIssuePro.otherServicePaymentDate != null
              ? _newIssuePro
                      .otherServicePaymentDate.millisecondsSinceEpoch ~/
                  1000
              : null,
      "other_service_payment_status": _newIssuePro.otherServicePaymentStatus,
      "other_service_surplus": _newIssuePro.otherServiceSurplus,
      "other_service_shortage" : _newIssuePro.otherServiceShortage,
      "other_service_remarks_or_comments": _newIssuePro.otherServiceRemarksOrComments,
      "bed_allocation_patient_location" : _newIssuePro.bedAllocationPatientLocation,
      "bed_allocation_remarks": _newIssuePro.bedAllocationRemarks,
      //Introducing New Fields END
      "preferred_visit_date_and_time_from":
          _newIssuePro.preferredVisitDataAndTimeFrom != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeFrom.millisecondsSinceEpoch ~/
                  1000
              : null,
      "preferred_visit_date_and_time_to":
          _newIssuePro.preferredVisitDataAndTimeTo != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeTo.millisecondsSinceEpoch ~/
                  1000
              : null,
      "address_1": _newIssuePro.address1,
      "address_2": _newIssuePro.address2,
      "pincode": _newIssuePro.pinCode,
      "members": members,
      "group_tags": groupTags
    };

    if (!updating) {
      body["offline_id"] = _newIssuePro.offlineId;
    }

    String api = updating
        ? updateChatGroupApi.replaceAll(':id', tempGroupId)
        : createChatGroupApi;

    try {
      var resp = await dio.postExtended(api, data: body);
      Map respData = resp.data;
      PrintString('resp data ================ $respData');
      if ((respData.containsKey('success') && respData['success']) ||
          (respData.containsKey('code') && respData['code'] == 'ID_EXISTS')) {
        String newGroupId = respData['id'];

        await _updateGroupToLocalDB(
            newGroupId: newGroupId,
            issueNumber: respData['issue_no'],
            currentGroupId: tempGroupId,
            timeStamp: respData['timestamp'],
            members: respData['data']['members'],
            tag: respData['data']['group_tags']);

        _userProvider.setSaving(false);
        setState(() {
          _isLoading = false;
        });

        updating
            ? await _updateChatScreen(newGroupId)
            : await _updateChatListScreenAndNavigateToNewChatScreen(
                newChatGroupId: newGroupId);
      } else {
        _userProvider.setSaving(false);
        setState(() {
          _isLoading = false;
        });
        if (updating) {
          await _updateChatScreen(tempGroupId);
        } else {
          await _dbHelper
              .updateGroupVal(tempGroupId, {'syncStatus': 'pending'});

          await _updateChatListScreenAndNavigateToNewChatScreen(
              newChatGroupId: tempGroupId);
        }

        PrintString('Something went wrong');
      }
      if (!mounted) return;
    } catch (e) {
      if (!mounted) return;

      if (updating) {
        _userProvider.setSaving(false);
        setState(() {
          _isLoading = false;
        });
        await _updateChatScreen(tempGroupId);
      } else {
        await _dbHelper.updateGroupVal(tempGroupId, {'syncStatus': 'pending'});

        await _updateChatListScreenAndNavigateToNewChatScreen(
            newChatGroupId: tempGroupId);
      }

      PrintString('Something went wrong');
      PrintString(e);
    } finally {
      _userProvider.setSaving(false);
      if (!mounted) return;

      setState(() {
        _isLoading = false;
      });
      _userProvider.setSyncLatestUpdatesClick(true);
    }
  }

  _updateGroupToLocalDB(
      {String newGroupId,
      issueNumber,
      currentGroupId,
      timeStamp,
      members,
      tag}) async {
    List selectedGroupMembers = members;
    List selectedGroupTag = tag;
    PrintString('=================================');
    //-----------------  group members  ----------------------------

    await _dbHelper.deleteGroupMembers(groupId: currentGroupId);

    for (int i = 0; i < selectedGroupMembers.length; i++) {
      await _dbHelper.insertGroupMembers(
        timeStamp: timeStamp,
        userName: selectedGroupMembers[i]['user_name'],
        userFullName: selectedGroupMembers[i]['user_fullname'],
        syncStatus: 'complete',
        userId: selectedGroupMembers[i]['user_id'],
        groupId: newGroupId,
        id: selectedGroupMembers[i]['_id'],
        userType: selectedGroupMembers[i]['user_type'],
      );
    }

    //-----------------  group tag  ----------------------------
    await _dbHelper.clearGroupTags(currentGroupId);

    for (int i = 0; i < selectedGroupTag.length; i++) {
      await _dbHelper.insertGroupTag(
        groupId: newGroupId,
        tagCategory: selectedGroupTag[i]['tag_category'],
        tagName: selectedGroupTag[i]['tag_name'],
        tagId: selectedGroupTag[i]['_id'],
      );
    }

    //----------------- update group ----------------------------
    final a = await _dbHelper.updateGroupData(
      currentGroupId: currentGroupId,
      newGroupId: newGroupId,
      name: _newIssuePro.nameController.text,
      issueNo: issueNumber,
      complainant: _newIssuePro.complainant,
      //Addtion Fields START
      patientName: _newIssuePro.patientName,
      patientAge: _newIssuePro.patientAge,
      symptoms: _newIssuePro.symptoms,
      sinceHowManyDays: _newIssuePro.sinceHowManyDays,
      spo2Level: _newIssuePro.spo2Level,
      isOnOxygenCylinder: _newIssuePro.isOnOxygenCylinder,
      covidResult: _newIssuePro.covidResult,
      hospitalPreference: _newIssuePro.hospitalPreference,
      attenderName: _newIssuePro.attenderName,
      attenderMobileno: _newIssuePro.attenderMobileno,
      relationToPatient: _newIssuePro.relationToPatient,
      srfId: _newIssuePro.srfId,
      buNumber: _newIssuePro.buNumber,
      bloodGroup: _newIssuePro.bloodGroup,
      typeOfBedRequied: _newIssuePro.typeOfBedRequied,
      otherServicePatientID: _newIssuePro.otherServicePatientID,
      otherServicePatientName: _newIssuePro.otherServicePatientName,
      otherServiceLocation: _newIssuePro.otherServiceLocation,
      otherServicePhoneNo: _newIssuePro.otherServicePhoneNo,
      otherServicePatientAddress: _newIssuePro.otherServicePatientAddress,
      otherServiceStatus: _newIssuePro.otherServiceStatus,
      otherServiceCoordinatorInCharge: _newIssuePro.otherServiceCoordinatorInCharge,
      otherServiceTypeOfService: _newIssuePro.otherServiceTypeOfService,
      otherServiceDateTimeOfService: _newIssuePro.otherServiceDateTimeOfService != null
              ? _newIssuePro
                      .otherServiceDateTimeOfService.millisecondsSinceEpoch ~/
                  1000
              : null,
      otherServiceDatesIfAny: _newIssuePro.otherServiceDatesIfAny,
      otherServiceVendor: _newIssuePro.otherServiceVendor,
      otherServiceConsultingDoctor:_newIssuePro.otherServiceConsultingDoctor,
      otherServiceEquipmentID: _newIssuePro.otherServiceEquipmentID,
      otherServiceEquipmentType: _newIssuePro.otherServiceEquipmentType,
      otherServiceNamesOfTest: _newIssuePro.otherServiceNamesOfTest,
      otherServiceNoOfTestConducted: _newIssuePro.otherServiceNoOfTestConducted,
      otherServiceAreaSquareFeet: _newIssuePro.otherServiceAreaSquareFeet,
      otherServiceNoOfEquipmentsIssued: _newIssuePro.otherServiceNoOfEquipmentsIssued,
      otherServicePaidOrFree: _newIssuePro.otherServicePaidOrFree,
      otherServiceRate: _newIssuePro.otherServiceRate,
      otherServiceTotalCost: _newIssuePro.otherServiceTotalCost,
      otherServiceCollectedBy:_newIssuePro.otherServiceCollectedBy,
      otherServiceAmountCollected: _newIssuePro.otherServiceAmountCollected,
      otherServiceCombinedPaymentDetails: _newIssuePro.otherServiceCombinedPaymentDetails,
      otherServiceCollectionMode: _newIssuePro.otherServiceCollectionMode,
      otherServiceTransactionID: _newIssuePro.otherServiceTransactionID,
      otherServicePaymentDate: _newIssuePro.otherServicePaymentDate != null
              ? _newIssuePro
                      .otherServicePaymentDate.millisecondsSinceEpoch ~/
                  1000
              : null,
      otherServicePaymentStatus: _newIssuePro.otherServicePaymentStatus,
      otherServiceSurplus: _newIssuePro.otherServiceSurplus,
      otherServiceShortage: _newIssuePro.otherServiceShortage,
      otherServiceRemarksOrComments: _newIssuePro.otherServiceRemarksOrComments,
      bedAllocationPatientLocation: _newIssuePro.bedAllocationPatientLocation,
      bedAllocationRemarks: _newIssuePro.bedAllocationRemarks,
      //Addtion Fields END
      address1: _newIssuePro.address1,
      address2: _newIssuePro.address2,
      preferredVisitDateAndTimeFrom:
          _newIssuePro.preferredVisitDataAndTimeFrom != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeFrom.millisecondsSinceEpoch ~/
                  1000
              : null,
      timestamp: timeStamp,
      orgId: _appProvider.selectedOrganization['_id'],
      complaintDate: _newIssuePro.complaintDate != null
          ? _newIssuePro.complaintDate.millisecondsSinceEpoch ~/ 1000
          : null,
      groupStatus: _newIssuePro.status,
      highPriority: _newIssuePro.highPriority ? 1 : 0,
      phone: _newIssuePro.phone,
      pinCode: _newIssuePro.pinCode,
      preferredVisitDateAndTimeFromTo:
          _newIssuePro.preferredVisitDataAndTimeTo != null
              ? _newIssuePro
                      .preferredVisitDataAndTimeTo.millisecondsSinceEpoch ~/
                  1000
              : null,
      syncStatus: 'complete',
    );
    PrintString('print id of updated group $a');
    // _updateChatListScreen();
  }

  // Refreshes the UI to show all groups once again
  _updateChatListScreenAndNavigateToNewChatScreen({newChatGroupId}) async {
    // List groupData = await _dbHelper
    //     .getGroupDataFromOrganization(_appProvider.selectedOrganization['_id']);
    // _appProvider.setIssueGroupList(groupData);
    await _appProvider.updateIssueGroupList();
    // PrintString("============== local ======== $groupData");
    Timer(Duration(seconds: 5), () {
      //
      _userProvider.setSyncToRemote(true);
    });

    await _exit(groupId: newChatGroupId);
  }

  // Refreshes the UI to show all groups once again
  _updateChatScreen(groupId) async {
    // List groupData = await _dbHelper
    //     .getGroupDataFromOrganization(_appProvider.selectedOrganization['_id']);
    // _appProvider.setIssueGroupList(groupData);

    await _appProvider.updateIssueGroupList();

    // _appProvider.setIssueGroupList(groupData);

    Timer(Duration(seconds: 5), () {
      _userProvider.setSyncToRemote(true);
    });

    await _exit(groupId: groupId);
  }

  Future<void> _exit({groupId}) async {
    Navigator.pop(context);
    bool creating = _newIssuePro.groupId == null;
    if (creating && null != groupId) {
      if (_newIssuePro.copyOf != null) {
        Navigator.pop(context);
      }
      List data = await _dbHelper.getGroupData(groupId);
      if (data.isNotEmpty) {
        unawaited(Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChatScreenPage(groupDetails: data[0]))));
      }
    } else {
      await _chatScreenProvider.refreshChatData();
    }
  }
}
