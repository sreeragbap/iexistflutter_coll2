import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:provider/provider.dart';

import 'chatScreen/chatScreenPage.dart';
import 'issueTabScreen.dart';

class SearchIssuePage extends StatefulWidget {
  @override
  _SearchIssuePageState createState() => _SearchIssuePageState();
}

class _SearchIssuePageState extends State<SearchIssuePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  AppProvider _appProvider = AppProvider();
  UserProvider _userProvider = UserProvider();
  final _dbHelper = DBHelper.instance;
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        titleSpacing: 0,
        title: TextField(
          onChanged: (val) {
            _appProvider.getFilteredGroups(val, _dbHelper);
          },
          decoration: InputDecoration.collapsed(hintText: 'Search issues'),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        child: ListView.separated(
        padding: EdgeInsets.symmetric(vertical: 20),
        itemCount: _appProvider.filteredGroupList.length,
        itemBuilder: (context, index) {
          return Visibility(
            visible: true,
            child: ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ChatScreenPage(
                      groupDetails: _appProvider.filteredGroupList[index],
                    ),
                  ),
                );
              },
              leading: CircleAvatar(
                radius: 25,
                backgroundColor: Colors.white,
                backgroundImage: AssetImage('assets/images/logo.png'),
              ),
              title: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      '${_appProvider.filteredGroupList[index]['issue_no'] != null ? _appProvider.filteredGroupList[index]['issue_no'] : ''} ${_appProvider.filteredGroupList[index]['group_name']}'
                          .trim(),
                      style: TextStyle(fontWeight: FontWeight.w600),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
//                  Text(
//                    UiHelper()
//                        .getDate('${_appProvider.filteredGroupList[index]['times']}'),//TODO CHANGE update date
//                    textAlign: TextAlign.center,
//                    style: TextStyle(fontSize: 12, color: Colors.grey),
//                  ),
                ],
              ),
//              subtitle: Visibility(
//                visible: _appProvider.filteredGroupList[index]['complainant'] != null,
//                child: FutureBuilder(
//                  future: _dbHelper.getGroupTagByGroupIdData(
//                      _appProvider.filteredGroupList[index]['groupId']),
//                  builder: (context, snapshot) {
//                    if (snapshot.hasData) {
//                      return Container(
//                        height: 15,
//                        child: ListView.builder(
//                          shrinkWrap: true,
//                          scrollDirection: Axis.horizontal,
//                          itemCount: snapshot.data.length,
//                          itemBuilder: (context, index) {
//                            return Text('${snapshot.data[index]['tagName']}, ');
//                          },
//                        ),
//                      );
//                    }
//                    return Text('');
//                  },
//                ),
//              ),
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Visibility(
            visible: true,
            child: Divider(
              indent: 60,
            ),
          );
        },
      ),
      )
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

        _getGroupChatsFromLocalDb();
    });
  }

  void _getGroupChatsFromLocalDb() async{
    List groups =await _dbHelper.getGroupDataFromOrganization(_appProvider.selectedOrganization['_id']);
    _appProvider.setFilteredGroupList(groups);
  }

}
