import 'dart:async';

import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/fcm/fcm_handler.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/firstPage/firstPage.dart';
import 'package:iexist/src/screens/login/loginLanding.dart';
import 'package:iexist/src/utilities/notification_actions.dart';
import 'package:iexist/src/utilities/sharedPreferenceKeys.dart';
import 'package:iexist/src/widgets/notificationIndicator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../main.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with WidgetsBindingObserver {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  ChatScreenProvider _chatScreenProvider = ChatScreenProvider();
  NotificationIndicator _notificationIndicator = NotificationIndicator();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    _appProvider.setAppVisibility(state);
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    _chatScreenProvider = Provider.of<ChatScreenProvider>(context);

    FCMHandler.onMessage = (message) async {
      _updateData(message);
    };

    FCMHandler.onLaunch = (message) async {
      _updateData(message);
    };

    FCMHandler.onResume = (message) async {
      _updateData(message);
    };

    FCMHandler.onBackground = (message) async {
      _updateData(message);
    };

    FCMHandler.init(_appProvider);

    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void _updateData(message, {background = false}) {
    PrintString("_updateData - $message");
    _notificationIndicator.show();

    if (background) {
      _appProvider.setAppVisibility(AppLifecycleState.detached);
    } else {
      _appProvider.setAppVisibility(AppLifecycleState.resumed);
    }

    dynamic action;
    if (null != message && null != message['data']) {
      action = message['data']['action'];
    }

    if (action != null && NotificationActions.org_actions.contains(action)) {
      _userProvider.setFirstSyncStatus(false);
    } else {
      _userProvider.setSyncLatestUpdatesClick(true);
    }

    if (action == NotificationActions.group_member_rm) {
      var groupId = message['data']['id'];
      if (null != groupId) {
        _chatScreenProvider.removeUserFromGroup(groupId);
      }
    }
  }

  @override
  void initState() {
    _appProvider.setOrgs();
    WidgetsBinding.instance.addObserver(this);
    Timer(Duration(seconds: 2), () {
      _getUserData();
    });
    super.initState();

    _appProvider.setAppVisibility(AppLifecycleState.resumed);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      navigatorKey.currentState.overlay
          .insert(OverlayEntry(builder: (_) => _notificationIndicator));
    });
  }

  // Used to save sharedpreferences to provider/state
  void _getUserData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await _userProvider.init();

    if (_userProvider.token != null) {
      var selectedOrganization =
          sharedPreferences.getString(selectedOrganizationSaveData);

      await _appProvider.setSelectedOrganization(selectedOrganization);

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => FirstPage(),
        ),
      );
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => LoginLanding(),
        ),
      );
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
