
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/extensions.dart';


class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String email;
  bool loading;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Reset Password'),
      ),
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              children: <Widget>[
                Text(
                  'Provide your registered email address and we will send you a password reset link.',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500,
                      fontSize: 15),
                ),
                SizedBox(
                  width: double.maxFinite,
                  height: 70,
                ),
                Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(hintText: 'Email'),
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regex = RegExp(pattern);
                          if (value.isEmpty) {
                            return 'Email is required';
                          }
                          else if (!regex.hasMatch(value)) {
                            return 'Invalid Email';
                          }
                          _formKey.currentState.save();
                          return null;
                        },
                        onSaved: (value) {
                          this.email = value;
                        },
                      ),
                      SizedBox(
                        height: 20,
                        width: double.maxFinite,
                      ),
                      SizedBox(
                        height: 50,
                        width: double.maxFinite,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FloatingActionButton(
                            onPressed: () {
                              _validate();
                            },
                            child: this.loading != true
                                ? Icon(
                                    Icons.check,
                                    color: mainTextColor,
                                    size: 30,
                                  )
                                : CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  ),
                          )
                        ],
                      )
                    ])),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _validate() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _reset();
    }
  }

  _reset() async {
    if (!mounted) return;
    
    setState(() {
      loading = true;
    });

    var body = {
      "email": email,
    };

    try {
    Dio dio = Dio();
    dio.options.headers['content-Type'] = 'application/x-www-form-urlencoded';
      var resp = await dio.postExtended(resetPasswordApi, data: body);
      Map respData = resp.data;

      if (!mounted) return;
      setState(() {
        loading = false;
      });

      if (respData['code'] == "SUC") {
        Navigator.of(context).pop(true);
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(respData['message']),
            backgroundColor: Colors.red,
          ),
        );
      }
    } catch (e) {
      if (!mounted) return;
      setState(() {
        loading = false;
      });
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Something went wrong...'),
          backgroundColor: Colors.red,
        ),
      );
      PrintString(e);
    }
  }
}
