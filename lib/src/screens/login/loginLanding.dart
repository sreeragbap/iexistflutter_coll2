import 'package:flutter/material.dart';
import 'package:iexist/src/screens/login/loginWithEmail.dart';
import 'package:iexist/src/screens/login/loginWithPhoneNumber.dart';
import 'package:iexist/src/screens/login/resetPassword.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:toast/toast.dart';

// Login Landing page
class LoginLanding extends StatefulWidget {
  @override
  _LoginLandingState createState() => _LoginLandingState();
}

class _LoginLandingState extends State<LoginLanding> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: Container(
          alignment: Alignment.topLeft,
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 36.0),
                    child: Image.asset(
                      'assets/images/logo.png',
                      width: 200,
                      height: 200,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 30,
                      horizontal: 5,
                    ),
                    child: Text(
                      'By continuing, you agree to the terms & conditions',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: SizedBox(
                      width: 300,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginWithPhoneNumber(),
                            ),
                          );
                        },
                        color: Colors.red[500],
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 15, horizontal: 30),
                          child: Text(
                            'LOGIN WITH PHONE NUMBER',
                            style: TextStyle(
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w700,
                              color: mainTextColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 300,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LoginWithEmail(),
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 15,
                          horizontal: 30,
                        ),
                        child: Text(
                          'LOGIN WITH USERNAME',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w700,
                            color: mainTextColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 50,
                    ),
                    child: InkWell(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text(
                          'Reset Password',
                          style: TextStyle(color: primaryColor),
                        ),
                      ),
                      onTap: () async {
                        var success = await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ResetPassword(),
                          ),
                        );
                        if (success) {
                          Toast.show(
                            'Check your email for a link to reset your password.',
                            context,
                            backgroundColor: Colors.green,
                            duration: 10
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
