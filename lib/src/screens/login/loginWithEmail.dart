import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/firstPage/firstPage.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:provider/provider.dart';
import 'package:iexist/src/utilities/extensions.dart';

// Used to login with username

class LoginWithEmail extends StatefulWidget {
  @override
  _LoginWithEmailState createState() => _LoginWithEmailState();
}

class _LoginWithEmailState extends State<LoginWithEmail> {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String email;
  String pwd;
  bool loading;

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Login with Username'),
      ),
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              children: <Widget>[
                Text(
                  'Provide your registered Username and password to login',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500,
                      fontSize: 15),
                ),
                SizedBox(
                  width: double.maxFinite,
                  height: 70,
                ),
                Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(hintText: 'Username'),
                        // The validator receives the text that the user has entered.
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Username is required';
                          }
                          _formKey.currentState.save();
                          return null;
                        },
                        onSaved: (value) {
                          this.email = value;
                        },
                      ),
                      SizedBox(
                        height: 20,
                        width: double.maxFinite,
                      ),
                      TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(hintText: 'Password'),
                        // The validator receives the text that the user has entered.
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password is required';
                          }
                          _formKey.currentState.save();
                          return null;
                        },
                        onSaved: (value) {
                          this.pwd = value;
                        },
                      ),
                      SizedBox(
                        height: 50,
                        width: double.maxFinite,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FloatingActionButton(
                            onPressed: () {
                              _validate();
                            },
                            child: this.loading != true
                                ? Icon(
                                    Icons.arrow_forward,
                                    color: mainTextColor,
                                    size: 30,
                                  )
                                : CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  ),
                          )
                        ],
                      )
                    ])),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Used for form validation
  void _validate() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _login();
    }
  }
  // Used to send the login response

  _login() async {
    if (!mounted) return;
    setState(() {
      loading = true;
    });
    var body = {
      "user_cred": email,
      "password": pwd,
      "fcm_device_id": _appProvider.fcmDeviceId
      // "method": pwd
    };

    PrintString("Login API Body------------");
    PrintString(body);

    try {
      var resp = await Dio().postExtended(loginApi, data: body);
      Map respData = resp.data;
      PrintString(respData);
      if (!mounted) return;
      setState(() {
        loading = false;
      });

      if (respData['id'] != null) {
        await saveUserData(_userProvider, _appProvider, respData);

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => FirstPage()),
            (route) => false);
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(respData['message']),
            backgroundColor: Colors.red,
          ),
        );
      }
    } catch (e) {
      if (!mounted) return;
      setState(() {
        loading = false;
      });
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Something went wrong...'),
          backgroundColor: Colors.red,
        ),
      );
      PrintString(e);
    }
  }
}
