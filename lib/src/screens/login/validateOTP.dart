import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/firstPage/firstPage.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:provider/provider.dart';
import 'package:iexist/src/utilities/extensions.dart';

class ValidateOTP extends StatefulWidget {
  final phone;
  final isUpdating;

  const ValidateOTP({this.phone, this.isUpdating = false});

  @override
  _ValidateOTPState createState() => _ValidateOTPState();
}

class _ValidateOTPState extends State<ValidateOTP> {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  CreateNewIssueGroupProvider _newIssuePro = CreateNewIssueGroupProvider();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String otp;
  bool loading;

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    _newIssuePro = Provider.of<CreateNewIssueGroupProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Login with Phone number'),
      ),
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              children: <Widget>[
                Text(
                  'Please enter OTP',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500,
                      fontSize: 15),
                ),
                SizedBox(
                  width: double.maxFinite,
                  height: 70,
                ),
                Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(hintText: 'OTP'),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'OTP is required';
                          }
                          _formKey.currentState.save();
                          return null;
                        },
                        onSaved: (value) {
                          this.otp = value;
                        },
                      ),
                      SizedBox(
                        height: 20,
                        width: double.maxFinite,
                      ),
                      SizedBox(
                        height: 50,
                        width: double.maxFinite,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FloatingActionButton(
                            onPressed: () {
                              _validate();
                            },
                            child: this.loading != true
                                ? Icon(
                                    Icons.arrow_forward,
                                    color: mainTextColor,
                                    size: 30,
                                  )
                                : CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  ),
                          )
                        ],
                      )
                    ])),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Used for form validation
  void _validate() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _login();
    }
  }
  // Used to send the login response

  Future<void> _login() async {
    if (!mounted) return;
    setState(() {
      loading = true;
    });

    var body = {
      "user_cred": widget.phone,
      "otp": otp,
      "fcm_device_id": _appProvider.fcmDeviceId,
      "method": 'otp'
    };

    try {
      var resp = await Dio().postExtended(loginApi, data: body);
      Map respData = resp.data;
      PrintString(respData);
      if (!mounted) return;
      setState(() {
        loading = false;
      });

      if (respData['id'] != null) {
        await saveUserData(_userProvider, _appProvider, respData);

        if (widget.isUpdating) {
          Navigator.of(context).pop(true);
        } else {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => FirstPage()),
              (route) => false);
        }
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(respData['message']),
            backgroundColor: Colors.red,
          ),
        );
      }
    } catch (e) {
      if (!mounted) return;
      setState(() {
        loading = false;
      });
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Something went wrong...'),
          backgroundColor: Colors.red,
        ),
      );
      PrintString(e);
    }
  }
}
