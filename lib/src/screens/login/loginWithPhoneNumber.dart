
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/screens/login/validateOTP.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:sms_autofill/sms_autofill.dart';


class LoginWithPhoneNumber extends StatefulWidget {
  @override
  _LoginWithPhoneNumberState createState() => _LoginWithPhoneNumberState();
}

class _LoginWithPhoneNumberState extends State<LoginWithPhoneNumber> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String phone;
  bool loading;

  final SmsAutoFill _autoFill = SmsAutoFill();
  var txt_controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      String value = await _autoFill.hint;
      print(value);
      setState(() {
        txt_controller.text = value.substring(3);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Login with Phone number'),
      ),
      body: Builder(
        builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              children: <Widget>[
                Text(
                  'Provide your registered phone number to login',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500,
                      fontSize: 15),
                ),
                SizedBox(
                  width: double.maxFinite,
                  height: 70,
                ),
                Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      TextFormField(
                        controller: txt_controller,
                        decoration: InputDecoration(hintText: 'Phone number'),
                        keyboardType: TextInputType.phone,
                        validator: (value) {
                          Pattern pattern = r'^\d{10}$';
                          RegExp regex = RegExp(pattern);
                          if (value.isEmpty) {
                            return 'Phone number is required';
                          } else if (!regex.hasMatch(value)) {
                            return 'Invalid Phone number';
                          }
                          _formKey.currentState.save();
                          return null;
                        },
                        onSaved: (value) {
                          this.phone = value;
                        },
                      ),
                      SizedBox(
                        height: 20,
                        width: double.maxFinite,
                      ),
                      SizedBox(
                        height: 50,
                        width: double.maxFinite,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FloatingActionButton(
                            onPressed: () {
                              _validate();
                            },
                            child: this.loading != true
                                ? Icon(
                                    Icons.arrow_forward,
                                    color: mainTextColor,
                                    size: 30,
                                  )
                                : CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  ),
                          )
                        ],
                      )
                    ])),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Used for form validation
  void _validate() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _login();
    }
  }
  // Used to send the login response

  _login() async {
    if (!mounted) return;
    setState(() {
      loading = true;
    });
    var body = {
      "user_cred": phone,
    };

    try {
      var resp = await Dio().postExtended(generateOTP, data: body);
      Map respData = resp.data;
      PrintString(respData);
      if (!mounted) return;
      setState(() {
        loading = false;
      });

      if (respData['success']) {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ValidateOTP(
                phone: phone,
              ),
            ));
      } else {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(respData['message']),
            backgroundColor: Colors.red,
          ),
        );
      }
    } catch (e) {
      if (!mounted) return;
      setState(() {
        loading = false;
      });
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text('Something went wrong...'),
          backgroundColor: Colors.red,
        ),
      );
      PrintString(e);
    }
  }
}
