/// Donut chart with labels example. This is a simple pie chart with a hole in
/// the middle.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:iexist/src/utilities/colors.dart';

class IssueStatusChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  IssueStatusChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory IssueStatusChart.withData(List<IssueStatus> data) {
    return IssueStatusChart(
      _createSampleData(data),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: GlobalKey<ScaffoldState>(),
      child: charts.PieChart(seriesList,
          animate: animate,
          // Configure the width of the pie slices to 60px. The remaining space in
          // the chart will be left as a hole in the center.
          //
          // [ArcLabelDecorator] will automatically position the label inside the
          // arc if the label will fit. If the label will not fit, it will draw
          // outside of the arc with a leader line. Labels can always display
          // inside or outside using [LabelPosition].
          //
          // Text style for inside / outside can be controlled independently by
          // setting [insideLabelStyleSpec] and [outsideLabelStyleSpec].
          //
          // Example configuring different styles for inside/outside:
          //       new charts.ArcLabelDecorator(
          //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
          //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
          defaultRenderer: charts.ArcRendererConfig(
              arcWidth: 90,
              arcRendererDecorators: [charts.ArcLabelDecorator()])),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<IssueStatus, int>> _createSampleData(
      List<IssueStatus> data) {
    int total = getTotal(data);
    data = data ??
        [
          IssueStatus("", 10),
        ];

    return [
      charts.Series<IssueStatus, int>(
        id: "status",
        domainFn: (IssueStatus status, index) => index,
        measureFn: (IssueStatus status, index) => status.count,
        data: data,
        colorFn: (IssueStatus status, index) {
          //Select colors from palette

          return charts.ColorUtil.fromDartColor(
              getIssueStatusColor(data, index));
        },
        // Set a label accessor to control the text of the arc label.
        labelAccessorFn: (IssueStatus row, _) =>
            '${((row.count / total) * 100).round()}%',
      )
    ];
  }

  static Color getIssueStatusColor(List<IssueStatus> data, int index) {
    //Select colors from palette
    int colorPaletteSections =
        colorPalette.length ~/ (data.length % colorPalette.length);
    int colorPaletteIndex = index * colorPaletteSections;

    if (0 > colorPaletteIndex) colorPaletteIndex = 0;
    if (colorPalette.length <= colorPaletteIndex) {
      colorPaletteIndex = colorPalette.length - 1;
    }

    return colorPalette[colorPaletteIndex];
  }

  static int getTotal(List<IssueStatus> data) {
    if (data == null) return 1;
    return data.fold(
        0, (previousValue, element) => (previousValue + element.count));
  }
}

/// Sample linear data type.
class IssueStatus {
  final String category;
  final int count;
  IssueStatus(
    this.category,
    this.count,
  );
}
