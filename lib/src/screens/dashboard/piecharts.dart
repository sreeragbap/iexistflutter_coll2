// import 'package:flutter/material.dart';
// import 'package:pie_chart/pie_chart.dart';

// class DashboardReportScreen extends StatefulWidget {
//   @override
//   _DashboardReportScreenState createState() => _DashboardReportScreenState();
// }

// class _DashboardReportScreenState extends State<DashboardReportScreen> {
//   Map<String, double> dataMap;
//   List<Color> colorList = [
//     Colors.red,
//     Colors.green,
//     Colors.blue,
//     Colors.yellow,
//   ];
//   @override
//   void initState() {
//     super.initState();
//     dataMap = new Map();
//     dataMap.putIfAbsent("Completed", () => 5);
//     dataMap.putIfAbsent("Open", () => 3);
//     dataMap.putIfAbsent("Reject", () => 2);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Report"),
//       ),
//       body: SingleChildScrollView(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 10.0),
//           child: Column(
//             mainAxisSize: MainAxisSize.max,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Card(
//                 child: Column(
//                   children: <Widget>[
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Text(
//                       "Public Work Department",
//                       style:
//                       TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     PieChart(
//                       dataMap: dataMap,
//                       animationDuration: Duration(milliseconds: 800),
//                       chartLegendSpacing: 32.0,
//                       chartRadius: MediaQuery.of(context).size.width / 1.1,
//                       showChartValuesInPercentage: true,
//                       showChartValues: true,
//                       showChartValuesOutside: false,
//                       chartValueBackgroundColor: Colors.grey[200],
//                       colorList: colorList,
//                       showLegends: true,
//                       legendPosition: LegendPosition.right,
//                       decimalPlaces: 1,
//                       showChartValueLabel: true,
//                       initialAngle: 0,
//                       chartValueStyle: defaultChartValueStyle.copyWith(
//                         color: Colors.blueGrey[900].withOpacity(0.9),
//                       ),
//                       chartType: ChartType.disc,
//                     )
//                   ],
//                 ),
//               ),
//               Card(
//                 child: Column(
//                   children: <Widget>[
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Text(
//                       "Labour Work Department",
//                       style:
//                       TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     PieChart(
//                       dataMap: dataMap,
//                       animationDuration: Duration(milliseconds: 800),
//                       chartLegendSpacing: 32.0,
//                       chartRadius: MediaQuery.of(context).size.width / 1.1,
//                       showChartValuesInPercentage: true,
//                       showChartValues: true,
//                       showChartValuesOutside: false,
//                       chartValueBackgroundColor: Colors.grey[200],
//                       colorList: colorList,
//                       showLegends: true,
//                       legendPosition: LegendPosition.right,
//                       decimalPlaces: 1,
//                       showChartValueLabel: true,
//                       initialAngle: 0,
//                       chartValueStyle: defaultChartValueStyle.copyWith(
//                         color: Colors.blueGrey[900].withOpacity(0.9),
//                       ),
//                       chartType: ChartType.disc,
//                     )
//                   ],
//                 ),
//               ),
//               Card(
//                 child: Column(
//                   children: <Widget>[
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Text(
//                       "Other Work Department",
//                       style:
//                       TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     PieChart(
//                       dataMap: dataMap,
//                       animationDuration: Duration(milliseconds: 800),
//                       chartLegendSpacing: 32.0,
//                       chartRadius: MediaQuery.of(context).size.width / 1.1,
//                       showChartValuesInPercentage: true,
//                       showChartValues: true,
//                       showChartValuesOutside: false,
//                       chartValueBackgroundColor: Colors.grey[200],
//                       colorList: colorList,
//                       showLegends: true,
//                       legendPosition: LegendPosition.right,
//                       decimalPlaces: 1,
//                       showChartValueLabel: true,
//                       initialAngle: 0,
//                       chartValueStyle: defaultChartValueStyle.copyWith(
//                         color: Colors.blueGrey[900].withOpacity(0.9),
//                       ),
//                       chartType: ChartType.disc,
//                     )
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//       bottomNavigationBar: Padding(
//         padding: const EdgeInsets.all(12.0),
//         child: Row(
//           mainAxisSize: MainAxisSize.max,
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           children: <Widget>[
//             Text(
//               "50 Open",
//               style: TextStyle(
//                   fontSize: 18,
//                   fontWeight: FontWeight.w600,
//                   color: Colors.green),
//             ),
//             Text(
//               "32 Closed",
//               style: TextStyle(
//                   fontSize: 18, fontWeight: FontWeight.w600, color: Colors.red),
//             ),
//             Text(
//               "5 Rejected",
//               style: TextStyle(
//                   fontSize: 18,
//                   fontWeight: FontWeight.w600,
//                   color: Colors.blue),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
