import 'dart:async';

import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'indicator.dart';
import 'issueStatusChart.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _fromDate = TextEditingController();
  final TextEditingController _toDate = TextEditingController();
  List<IssueStatus> data;
  String error;
  Map _selectedDepartment;

  final DEFAULT_FROM_DATE = DateTime.now().add(Duration(days: -10));
  final DEFAULT_TO_DATE = DateTime.now();
  static const String DATE_FORMAT = "dd-MM-yyyy";
  List<Map<dynamic, dynamic>> _allGroupTags = List<Map<dynamic, dynamic>>();

  AnimationController _legendAnimationController;
  bool _isLegendAnimationRunning = false;
  static const double _legendWidthMax = 120;
  double _legendWidth = _legendWidthMax;
  static const Duration _legendAnimationDuration = Duration(milliseconds: 450);
  bool _selected = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Timer.periodic(Duration(seconds: 1), (timer) async {
        if (null != _appProvider.selectedOrganization) {
          timer.cancel();
          await _fetchIssueStatus();

          Timer.periodic(Duration(seconds: 1), (timer) async {
            _allGroupTags = await DBHelper.instance.getTagDataFromOrganization(
                _appProvider.selectedOrganization['_id']);

            if (_allGroupTags.isNotEmpty) {
              timer.cancel();
              if (mounted) {
                setState(() {});
              }
            }
          });
        }
      });
    });

    _legendAnimationController =
        AnimationController(vsync: this, duration: _legendAnimationDuration);
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.get_app),
            onPressed: () {
              _userProvider.setFirstSyncStatus(false);
            },
          ),
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              _userProvider.setSyncLatestUpdatesClick(true);
            },
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10.0,
          ),
          Expanded(
            child: _buildissueStatusChartSection(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: _buildForm(context),
          ),
        ],
      ),
    );
  }

  Form _buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _buildFromDateField(context),
          _buildToDateField(context),
          _buildDepartmentSelectorField(),
          SizedBox(
            height: 10.0,
          ),
          _buildFormButtons(),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Row _buildFormButtons() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        OutlineButton(
          onPressed: () {
            _fetchIssueStatus();
          },
          // child: Padding(
          //   padding: const EdgeInsets.symmetric(
          //       vertical: 15, horizontal: 30),
          child: Text(
            'UPDATE',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w700,
              color: primaryColor,
            ),
          ),
          // ),
        ),
        SizedBox(width: 10),
        OutlineButton(
          onPressed: () {
            data = null;
            error = "No data  to display";
            _fromDate.text = "";
            _toDate.text = "";
            _selectedDepartment = null;
            setState(() {});
          },
          // child: Padding(
          //   padding: const EdgeInsets.symmetric(
          //       vertical: 15, horizontal: 30),
          child: Text(
            'CLEAR',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w700,
              color: primaryColor,
            ),
          ),
          // ),
        )
      ],
    );
  }

  Stack _buildDepartmentSelectorField() {
    return Stack(alignment: Alignment.center, children: [
      DropdownSearch<Map>(
        clearButton: Icon(
          Icons.clear,
          size: 16,
          color: Colors.grey,
        ),
        dropDownButton: Icon(
          Icons.arrow_drop_down,
          size: 24,
          color: Colors.grey,
        ),
        showClearButton: true,
        mode: Mode.DIALOG,
        showSearchBox: true,
        items: _allGroupTags,
        filterFn: (item, filter) {
          String tagName = item['tagName'].toLowerCase();
          return tagName.startsWith(filter.toLowerCase()) ||
              tagName.contains(" " + filter.toLowerCase());
        },
        autoFocusSearchBox: true,
        selectedItem: _selectedDepartment,
        itemAsString: (Map u) => u['tagName'],
        onChanged: (Map data) {
          _selectedDepartment = data;
          _fetchIssueStatus();
        },
        dropdownSearchDecoration: InputDecoration(
            labelText: "Dept/Office",
            contentPadding: EdgeInsets.only(top: 8.0)),
      ),
      Visibility(
        child: Container(
          height: 72,
          color: Colors.transparent,
          child: Center(
            child: SpinKitThreeBounce(
              color: Colors.grey,
              size: 20,
            ),
          ),
        ),
        visible: _allGroupTags.isEmpty,
      )
    ]);
  }

  TextFormField _buildToDateField(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'To'),
      controller: _toDate,
      readOnly: true,
      onTap: () {
        print(_formKey.currentState);
        DatePicker.showDatePicker(context,
            showTitleActions: true, maxTime: DateTime.now(), onChanged: (date) {
          print('change $date');
        }, onConfirm: (date) {
          print('confirm $date');
          this.setState(() {
            _toDate.text = DateFormat(DATE_FORMAT).format(date);
          });
        }, currentTime: DateTime.now(), locale: LocaleType.en);
      },
    );
  }

  TextFormField _buildFromDateField(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(labelText: 'From'),
      controller: _fromDate,
      readOnly: true,
      onTap: () {
        DatePicker.showDatePicker(context,
            showTitleActions: true, maxTime: DateTime.now(), onChanged: (date) {
          print('change $date');
        }, onConfirm: (date) {
          print('confirm $date');
          this.setState(() {
            _fromDate.text = DateFormat(DATE_FORMAT).format(date);
          });
        }, currentTime: DateTime.now(), locale: LocaleType.en);
      },
    );
  }

  Stack _buildissueStatusChartSection() {
    return Stack(
      children: [
        Row(children: [
          Expanded(
            child: IssueStatusChart.withData(data),
          ),
          if (data != null)
            AnimatedContainer(
              margin: EdgeInsets.only(top: 54, bottom: 16),
              width: _selected ? _legendWidthMax : 0.0,
              alignment: Alignment.center,
              duration: _legendAnimationDuration,
              curve: Curves.fastOutSlowIn,
              child: _buildLegend(),
            )
        ]),
        if (data != null)
          Positioned(
            top: 0,
            right: 0,
            child: _buildShowHideLegenedButton(),
          ),
        if (data == null)
          Container(
            color: backgroundColor.withOpacity(0.97),
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Center(
              child: error == null
                  ? CircularProgressIndicator()
                  : Text(
                      error,
                      style: TextStyle(color: errorTextColor),
                    ),
            ),
          ),
      ],
    );
  }

  Widget _buildLegend() {
    List<Widget> indicatorControls = [];
    for (int i = 0; i < data.length; i++) {
      indicatorControls.addAll([
        Indicator(
          color: IssueStatusChart.getIssueStatusColor(data, i),
          text: "${data[i].category} (${data[i].count})",
          isSquare: true,
        ),
        SizedBox(
          height: (i < data.length - 1) ? 4 : 18,
        ),
      ]);
    }
    return SingleChildScrollView(
        child: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: indicatorControls,
    ));
  }

  IconButton _buildShowHideLegenedButton() {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_close, progress: _legendAnimationController),
      onPressed: () {
        setState(() {
          _isLegendAnimationRunning = !_isLegendAnimationRunning;
          _legendWidth = (_legendWidth == 0) ? _legendWidthMax : 0;
          _isLegendAnimationRunning
              ? _legendAnimationController.forward()
              : _legendAnimationController.reverse();

          _selected = !_selected;
        });
      },
    );
  }

  Future<void> _fetchIssueStatus() async {
    if (_fromDate.text == null || _fromDate.text.isEmpty) {
      _fromDate.text = DateFormat(DATE_FORMAT).format(DEFAULT_FROM_DATE);
    }

    if (_toDate.text == null || _toDate.text.isEmpty) {
      _toDate.text = DateFormat(DATE_FORMAT).format(DEFAULT_TO_DATE);
    }

    data = null;
    error = null;
    if (mounted) {
      setState(() {});
    }

    String apiUrl = groupStatisticsApi
        .replaceAll(":user_id", _userProvider.userId)
        .replaceAll(":from_date", _fromDate.text)
        .replaceAll(":to_date", _toDate.text)
        .replaceAll(
            ":organization_id", _appProvider.selectedOrganization['_id']);

    if (null != _selectedDepartment) {
      apiUrl = apiUrl + '&department_id=' + _selectedDepartment['tagId'];
    }

    try {
      var resp = await Dio(
              BaseOptions(headers: {"Authorization": _userProvider.token}))
          .getExtended(apiUrl);
      PrintString(resp.data);

      if (resp.data != null &&
          resp.data.containsKey("Success") &&
          resp.data["Success"].toString().toLowerCase() == "false") {
        error = resp.data.containsKey("Message")
            ? resp.data["Message"]
            : "Error occurred while fetching data";
        data = null;
      } else if (resp.data != null) {
        data = List<IssueStatus>();
        resp.data["data"].forEach((category, count) {
          if (category.toString().toLowerCase() != "total") {
            data.add(IssueStatus(category.toString().capitalize(), count));
          }
        });

        if (data.isEmpty) {
          error = "No data  to display";
          data = null;
        }
      } else {
        data = null;
        error = "Error occurred while fetching data";
      }
    } on DioError {
      data = null;
      error = "Unable to fetch report in offline";
    } catch (e) {
      error = e?.toString();
      data = null;
    } finally {
      if (mounted) {
        setState(() {});
      }
    }
  }
}
