import 'package:flutter/material.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/syncFunction/latestDataSyncFunction.dart';
// Called when Sync is done

class GetLatestUpdateLoading extends StatefulWidget {
  final AppProvider appProvider;
  final UserProvider userProvider;
  final ChatScreenProvider chatProvider;

  const GetLatestUpdateLoading(
      {Key key, this.appProvider, this.userProvider, this.chatProvider})
      : super(key: key);
  @override
  _GetLatestUpdateLoadingState createState() => _GetLatestUpdateLoadingState();
}

class _GetLatestUpdateLoadingState extends State<GetLatestUpdateLoading> {
  @override
  Widget build(BuildContext context) {
    return Container(
        // alignment: Alignment.center,
        // color: Colors.black45,
        // child: Card(
        //   child: Padding(
        //     padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 40),
        //     child: Column(
        //       mainAxisSize: MainAxisSize.min,
        //       children: <Widget>[
        //         CircularProgressIndicator(),
        //         SizedBox(height: 16),
        //         Text('Please wait updating new data'),
        //       ],
        //     ),
        //   ),
        // ),
        );
  }

  @override
  void initState() {
    super.initState();
    LatestDataSyncFunction(
        widget.appProvider, widget.userProvider, widget.chatProvider);
  }
}
