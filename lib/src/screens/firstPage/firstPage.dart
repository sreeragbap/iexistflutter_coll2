import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/dashboard/dashboard.dart';
import 'package:iexist/src/screens/firstPage/firstSyncLoading.dart';
import 'package:iexist/src/screens/firstPage/getLatestUpdateLoading.dart';
import 'package:iexist/src/screens/issuePage/issuePage.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:iexist/src/screens/profile/profile_screen_ui.dart';
import 'package:iexist/src/syncFunction/syncFunction.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/synchronized.dart' as synchronized;

// Used to display bottom navigation

class FirstPage extends StatefulWidget {
  final int selectedIndex;

  FirstPage({this.selectedIndex});

  @override
  _FirstPageState createState() =>
      _FirstPageState(selectedIndex: selectedIndex);
}

class _FirstPageState extends State<FirstPage> {
  UserProvider _userProvider = UserProvider();
  AppProvider _appProvider = AppProvider();
  DBHelper _dbHelper = DBHelper.instance;
  ChatScreenProvider _chatProvider = ChatScreenProvider();
  _FirstPageState({int selectedIndex}) {
    _selectedIndex = selectedIndex ?? 0;
  }

  int _selectedIndex;
  List screens = [
    Dashboard(),
    IssuePage(),
    ProfileScreen(),
  ];

  static final synchronized.Lock _lock = synchronized.Lock();

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _appProvider = Provider.of<AppProvider>(context);
    _chatProvider = Provider.of<ChatScreenProvider>(context);
    return Stack(
      children: <Widget>[
        Scaffold(
          bottomNavigationBar: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (!_appProvider.isOnline)
                Container(
                  height: 30,
                  color: Colors.red,
                  child: Center(
                    child: Text(
                      'Offline',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              BottomNavigationBar(
                onTap: (index) {
                  if (!mounted) return;
                  setState(() {
                    _selectedIndex = index;
                  });
                },
                elevation: 50,
                type: BottomNavigationBarType.fixed,
                unselectedIconTheme: IconThemeData(
                  color: navigationTextColor,
                  size: 30,
                ),
                selectedIconTheme: IconThemeData(
                  color: buttonColor,
                  size: 30,
                ),
                unselectedLabelStyle: TextStyle(
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                  color: navigationTextColor,
                ),
                selectedLabelStyle: TextStyle(
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w700,
                  fontSize: 12,
                  color: buttonColor,
                ),
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    icon: Icon(Icons.dashboard),
                    title: Text('Dashboard'),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.chat),
                    title: Text('Issues'),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.person),
                    title: Text('Profile'),
                  ),
//            BottomNavigationBarItem(
//              icon: Icon(Icons.rss_feed),
//              title: Text('News'),
//            ),
                ],
                currentIndex: _selectedIndex,
              ),
            ],
          ),
          body: screens[_selectedIndex],
        ),
        if (_userProvider.firstSync != true)
          FirstSyncLoading(
            userProvider: _userProvider,
            appProvider: _appProvider,
          )
        else if (_userProvider.syncLatestUpdatesClick)
          GetLatestUpdateLoading(
            appProvider: _appProvider,
            userProvider: _userProvider,
            chatProvider: _chatProvider,
          ),
      ],
    );
  }

  @override
  void initState() {
    Timer.periodic(Duration(seconds: 1), (timer) {
      _checkConnectivity();
    });
    super.initState();
    // Only called when UI is rendered
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      _userProvider.setSyncToRemote(true);
      await _appProvider.setOrgs();
      await _getProfileData();
      await purge();

      //Pull the latest data from the server as notifications (data messages)
      //received while the application was not running would have lost
      Timer.periodic(Duration(seconds: 1), (timer) {
        if (_userProvider.syncToRemote == false) {
          timer.cancel();
          _userProvider.setSyncLatestUpdatesClick(true);
        }
      });
    });
  }

  Future<void> purge() async {
    var config = _appProvider.config;
    if (null == config['purging_frequency']) return;

    var purgeFrequency = config['purging_frequency'].round();

    var nowDate = DateTime.now();
    var nowSec = nowDate.millisecondsSinceEpoch;
    var purgeDate = nowDate
            .subtract(Duration(days: purgeFrequency))
            .millisecondsSinceEpoch ~/
        1000;

    var key = "last_purge_" + _appProvider.selectedOrganization['_id'];
    String lastRun = nowSec.toString();

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (sharedPreferences.containsKey(key)) {
      lastRun = sharedPreferences.getString(key);
    }

    await sharedPreferences.setString(key, nowSec.toString());

    final lastRunDate =
        DateTime.fromMillisecondsSinceEpoch(int.tryParse(lastRun));
    final difference = nowDate.difference(lastRunDate).inDays;

    if (difference >= 1) {
      await _dbHelper.clearChatMessagesTableWithTime(purgeDate);
      await _dbHelper.clearGroupTableWithTime(purgeDate);
      await _dbHelper.clearGroupMembersTableWithTime(purgeDate);
      await _dbHelper.clearLogMessages(purgeDate);
      await _dbHelper.clearMessagesStatus(purgeDate);
    }
  }

  // To check internet connectivity and set connectivity status to app provider
  Future<void> _checkConnectivity() async {
    if (!_lock.locked) {
      await _lock.synchronized(() async {
        try {
          await InternetAddress.lookup('google.com');
          _appProvider.setConnectionStatus(true);
          await _syncPendingData();
        } on SocketException catch (e) {
          _appProvider.setConnectionStatus(false);
        }
      });
    }
  }

  /* `syncToRemote` is set
  - When user opens app (true)
  - When user goes inside chat (false)
  - When user exits chat (false)
  */

  Future<void> _syncPendingData() async {
    //PrintString('data');
    if (_userProvider.syncToRemote) {
      // PrintString('pushing data');
      await SyncFunction(
              userProvider: _userProvider,
              appProvider: _appProvider,
              chatScreenProvider: _chatProvider)
          .syncGroup();
    }
  }

  /*
    Used to get profile data
  */
  Future<void> _getProfileData() async {
    Dio dio = Dio();
    PrintString(_userProvider.token);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["Authorization"] = _userProvider.token;
    try {
      var resp =
          await dio.getExtended('$profileDataApi/${_userProvider.userId}');
      Map respData = resp.data;
      if (respData['user'] != null) {
        await _setProfileData(respData['user']);
      }
    } catch (e) {
      PrintString(e);
    }
  }

  /*
      Profile Data is stored in SharedPreferences and stored in Provider/state
  */
  Future<void> _setProfileData(Map data) async {
    data['token'] = _userProvider.token;

    await _userProvider.init(data: data);
  }
}
