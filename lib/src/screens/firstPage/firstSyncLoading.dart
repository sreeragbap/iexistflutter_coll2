import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:synchronized/synchronized.dart' as synchronized;

// Called when Complete Data Sync is done
class FirstSyncLoading extends StatefulWidget {
  final AppProvider appProvider;
  final UserProvider userProvider;
  static final synchronized.Lock lock = synchronized.Lock();

  const FirstSyncLoading({Key key, this.appProvider, this.userProvider})
      : super(key: key);

  @override
  _FirstSyncLoadingState createState() => _FirstSyncLoadingState();
}

class _FirstSyncLoadingState extends State<FirstSyncLoading> {
  final _dbHelper = DBHelper.instance;
  bool error = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: Colors.black45,
      child: !error
          ? Card(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 40),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 16),
                    Text('Please wait loading data')
                  ],
                ),
              ),
            )
          : Card(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 40),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.red,
                      onPressed: () {
                        _getSyncData();
                      },
                      child: Text(
                        'Retry',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(height: 16),
                    Text('Something went wrong'),
//                    RaisedButton(
//                      onPressed: () async {
//                        List user = await _dbHelper.getAllGroupTag();
//                        PrintString(user);
//                      },
//                    )
                  ],
                ),
              ),
            ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getSyncData();
  }

  Future<void> _getSyncData() async {
    if (FirstSyncLoading.lock.locked) return;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await FirstSyncLoading.lock.synchronized(() async {
      setState(() {
        error = false;
      });
      Dio dio = Dio();
      dio.options.headers['content-Type'] = 'application/json';
      dio.options.headers["Authorization"] = widget.userProvider.token;
      try {
        var resp = await dio.getExtended(initialSyncApi,
            queryParameters: {'user_id': widget.userProvider.userId});
        Map respData = resp.data;
        PrintString(respData);
        if (respData['data'] != null) {
          List data = respData['data'];
          await _saveData(data, sharedPreferences, respData);
        } else {
          if (mounted) {
            setState(() {
              error = true;
            });
          }
        }

        ChatScreenProvider().reset();
        PrintString('Fetch complete');
        PrintString('====================');
      } catch (e) {
        if (mounted) {
          setState(() {
            error = true;
          });
        }

        PrintString(e);
      }
    });

    widget.userProvider.setFirstSyncStatus(true);
  }

  Future<void> _saveData(
      List data, SharedPreferences sharedPreferences, respData) async {
    await _dbHelper.clearUserTable();
    await _dbHelper.clearUserTagTable();
    await _dbHelper.clearTagTable();
    await _dbHelper.clearGroupTable();
    await _dbHelper.clearGroupMembersTable();
    await _dbHelper.clearChatMessagesTable();
    await _dbHelper.clearGroupTagTable();
    await _dbHelper.clearMessagesStatus(null);

    List orgData = [];

    for (int i = 0; i < data.length; i++) {
      String orgId = data[i]['organization_id'];
      int timeStamp = data[i]['timestamp'];
      List tag = data[i]['tags'];
      List users = data[i]['users'];
      List groups = data[i]['groups'];

      orgData.add({
        '_id': orgId,
        'config': getConfig(orgId, respData['config']['organization_config']),
        'group_statuses': data[i]['group_statuses'],
      });

      await _insertUserData(
        orgId,
        timeStamp,
        users,
      );
      await _insertTagData(orgId, tag);
      for (var group in groups) {
        await insertGroups(group, timeStamp, widget.appProvider, orgId: orgId, showNotification: false);
      }
    }

    await widget.appProvider.setOrgs(data: orgData);
  }

  getConfig(orgId, List data) {
    if (data == null) return {};

    return data.firstWhere((element) => element['organization_id'] == orgId,
        orElse: () => {});
  }

  Future<void> _insertUserData(orgId, timeStamp, List users) async {
    for (int i = 0; i < users.length; i++) {
      await _insertUserTagData(
        orgId,
        users[i]['_id'],
        users[i]['organization_details'],
      );

      await _dbHelper.insertUser(
          userName: users[i]['username'],
          orgId: orgId,
          timestamp: timeStamp,
          userFullName: users[i]['name'],
          userId: users[i]['_id']);
    }
    PrintString('User Inserted');
    PrintString('====================');
  }

  Future<void> _insertUserTagData(
      orgId, userId, Map organizationDetails) async {
    List allTags = organizationDetails['role_tags'] +
        organizationDetails['location_tags'] +
        organizationDetails['user_group_tags'] +
        organizationDetails['dept_tags'] +
        organizationDetails['designation_tags'] +
        organizationDetails['pincode_tags'] +
        organizationDetails['other_tags'] +
        organizationDetails['group_access_tags'];

    for (Map tag in allTags) {
      await _dbHelper.insertUserTag(
        tagName: tag['tag_name'],
        orgId: orgId,
        userId: userId,
        tagId: tag['_id'],
      );
    }
  }

  Future<void> _insertTagData(orgId, List tag) async {
    for (int i = 0; i < tag.length; i++) {
      await _dbHelper.insertTag(
          orgId: orgId,
          tagCategory: tag[i]['tag_category'],
          tagId: tag[i]['_id'],
          tagName: tag[i]['tag_name']);
    }
    PrintString('tag Inserted');
    PrintString('====================');
  }
}
