class ColorConstants {
  //static const int PRIMARY_COLOR = 0xFF036056;
  static const int PRIMARY_COLOR = 0xFF589CD9;
  static const int BUTTON_COLOR = 0xFF63D46A;
  //static const int BUTTON_COLOR = 0xFF62A0D9;
  static const int BACKGROUND_COLOR = 0xFFFFFFFF;
  static const int MAIN_TEXT_COLOR = 0xFFFFFFFF;
  static const int SUB_TEXT_COLOR = 0xFFE6E6E6;
  static const int NAVIGATION_TEXT_COLOR = 0xFF747474;
}
