import 'package:dio/dio.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/newMessageNotificationManager.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:sqflite/sqflite.dart';
import 'package:synchronized/synchronized.dart' as synchronized;
import 'package:pedantic/pedantic.dart';

class LatestDataSyncFunction {
  final AppProvider _appProvider;
  final UserProvider _userProvider;
  final ChatScreenProvider _chatProvider;
  static final synchronized.Lock lock = synchronized.Lock();
  DBHelper _dbHelper = DBHelper.instance;

  static const String userTable = 'usersTable';
  static const String tagTable = 'tagTable';
  static const String groupTable = 'groupTable';
  static const String groupMembersTable = 'groupMembersTable';
  static const String chatMessageTable = 'chatMessageTable';
  static const String groupTagTable = 'groupTagTable';
  static const String userTagTable = 'userTagTable';

  LatestDataSyncFunction(
      this._appProvider, this._userProvider, this._chatProvider) {
    unawaited(_getLatestData());
  }

  // Sending the sync deltas request and getting response
  Future<void> _getLatestData() async {
    if (lock.locked) {
      Future.delayed(Duration(seconds: 5), () {
        unawaited(_getLatestData());
      });
    } else {
      await lock.synchronized(() async {
        PrintString("-----------_getLatestData - entry locked------------");
        try {
          var defaultTimestamp = DateTime.now()
                  .subtract(Duration(days: 100))
                  .millisecondsSinceEpoch ~/
              1000;

          var organizationId = _appProvider.selectedOrganization['_id'];
          List userList = await _dbHelper.getUserListMax(organizationId);

          List groupList = await _dbHelper.getGroupListMax(organizationId);

          List messageList = await _dbHelper.getMessageListMax(
              organizationId, _userProvider.userId);

          Dio dio = Dio();
          dio.options.headers['content-Type'] = 'application/json';
          dio.options.headers["Authorization"] = _userProvider.token;

          Map body = {
            "user_id": _userProvider.userId,
            "organization_id": organizationId,
            "timestamp_json": {
              "users":
                  getTimeStamp(userList, 'MAX(timeStamp)', defaultTimestamp),
              "groups":
                  getTimeStamp(groupList, 'MAX(timestamp)', defaultTimestamp),
              "messages": getTimeStamp(messageList,
                  'MAX(chatMessageTable.time_stamp)', defaultTimestamp)
            }
          };

          PrintString("-----------_getLatestData - body------------");
          PrintString(body);

          try {
            var resp = await dio.postExtended(getLatestUpdateApi, data: body);

            Map respData = resp.data;
            PrintString("-----------_getLatestData - respData------------");
            PrintString(respData);
            if (respData['success']) {
              await _saveData(respData['data']);
            }
          } catch (e) {
            PrintString(e);
          }
        } catch (e) {
          PrintString(e);
        } finally {}
      });

      _userProvider.setSyncLatestUpdatesClick(false);
      PrintString(
          "-----------_getLatestData - setSyncLatestUpdatesClick(false)------------");
      PrintString("-----------_getLatestData - exit unlocked------------");
    }
  }

  dynamic getTimeStamp(List list, String key, dynamic defaultVal) {
    var timestamp = list.isEmpty ? defaultVal : ((list[0][key]) ?? defaultVal);
    return timestamp - 1;
  }

  /// Used to save the sync deltas response
  /// Possible actions are `add`, `edit` and `delete`
  Future<void> _saveData(List respData) async {
    PrintString("-----------_saveData - entry------------");
    List groupList = respData[0]['groups']; //todo update user list
    List userList = respData[0]['users'];
    int timeStamp = respData[0]['timestamp'];

    if (groupList.isEmpty) {}
    PrintString(userList);

    for (int i = 0; i < groupList.length; i++) {
      String action = groupList[i]['action'].toString().toLowerCase();

      if (action == 'add') {
        await insertGroups(groupList[i], timeStamp, _appProvider);
      } else if (action == 'update') {
        await _updateGroups(groupList[i], timeStamp);
      } else if (action == 'delete') {
        // await _updateGroups(groupList[i], timeStamp);
      }
    }

    for (var user in userList) {
      String action = user['action'].toString().toLowerCase();
      String orgId = user['organization_details']['_id'];
      if (action == 'add') {
        await _insertUser(user, timeStamp, orgId);
      } else if (action == 'update') {
        await _updateUser(user, timeStamp, orgId);
      } else if (action == 'delete') {
        await _deleteUser(user, timeStamp, orgId);
      }
    }

    await _updateChatListScreen();
    PrintString("-----------_saveData - exit------------");
  }

  Future<void> _updateNewGroupMessages(List messages, groupId) async {
    PrintString(
        '======================== messages for $groupId =======================');
    // PrintString(messages);
    List chatList = await _dbHelper.getChatMessageByGroupIdData('$groupId');
    List group = await _dbHelper.getGroupData(groupId);

    if (group.isEmpty) return;

    int newMessageCount = group[0]['unread_message_count'];
    List newMessages = [];
    bool messageExist = false;

    for (dynamic message in messages) {
      for (dynamic localMessage in chatList) {
        if (message['_id'] == "") {
          if ((localMessage["offline_id"] == message["offline_id"]) &&
              (message['user_id'] != _userProvider.userId)) {
            messageExist = true;
            break;
          }
        } else if (message["offline_id"] == "") {
          if ((localMessage["_id"] == message["_id"]) &&
              (message['user_id'] != _userProvider.userId)) {
            messageExist = true;
            break;
          }
        } else {
          if ((localMessage["_id"] == message["_id"]) ||
              (localMessage["offline_id"] == message["offline_id"]) &&
                  (message['user_id'] != _userProvider.userId)) {
            messageExist = true;
            break;
          }
        }
      }
      if (messageExist == false) {
        newMessages.add(message);
      }
    }
    //  newMessages = messages
    //     .where((message) =>
    //         !chatList.any((localMessage) =>
    //             (localMessage["_id"] == message["_id"]) ||
    //             (localMessage["offline_id"] == message["offline_id"])) &&
    //         (message['user_id'] != _userProvider.userId ||
    //             message['type'] == 'NOTIFICATION'))
    //     .toList();

    // List newMessages= messages.where((message) =>!chatList.contains(message["_id"] && message["offline_id"])).toList();

    var existing = messages.toSet().difference(newMessages.toSet()).toList();

    await insertGroupMessages(newMessages, groupId);
    await insertGroupMessages(existing, groupId, update: true);

    newMessageCount += newMessages.length;

    PrintString(
        '======================== messages for $groupId complete=======================');

    //For the group that is opened in UI, no need to update the new message count
    //as user would be reading the text already
    //Also no notification overlay to be shown if the group is opened in UI
    if (newMessageCount > 0) {
      if (groupId == _chatProvider.currentChatGroupId &&
          _appProvider.isAppinForeground()) {
        markGroupAsRead(_userProvider, _chatProvider, groupId);
      } else {
        await _dbHelper.updateReadCountGroupTable(
            groupId: groupId, count: newMessageCount);

        if (newMessages.isNotEmpty) {
          var lastMessage = newMessages.last;
          var messageText =
              ((lastMessage['text']?.toString()?.length ?? 0) == 0)
                  ? _getFileName(lastMessage['file'])
                  : lastMessage['text'];
          NewMessageNotificationManager.showNewMessageNotification(
              messageText,
              '${group[0]['issue_no'] != null ? group[0]['issue_no'] : ''} ${group[0]['group_name']}'
                  .trim(),
              group[0]['groupId'],
              lastMessage['chat_type']);
        }
      }
    }

    await _chatProvider.refreshChatData();
  }

  Future<void> _updateGroups(groupData, timeStamp) async {
    String groupId = groupData['_id'];

    List messages = groupData['messages'];
    List groupTag = groupData['group_tags'];
    List groupMembers = groupData['members'];

    await _dbHelper.updateGroupData(
      syncStatus: 'complete',
      currentGroupId: groupId,
      newGroupId: groupId,
      orgId: _appProvider.selectedOrganization['_id'],
      name: groupData['group_name'],
      issueNo: groupData['issue_no'],
      phone: groupData['phone_no'],
      pinCode: groupData['pincode'],
      groupStatus: groupData['group_status'],
      preferredVisitDateAndTimeFrom:
          groupData['preferred_visit_date_and_time_from'],
      preferredVisitDateAndTimeFromTo:
          groupData['preferred_visit_date_and_time_to'],
      highPriority: groupData['high_priority'] == true ? 1 : 0,
      complaintDate: groupData['complaint_date'],
      //Introducing New Fields START
      patientName: groupData['patient_name'],
      patientAge: groupData['patient_age'],
      symptoms: groupData['symptoms'],
      sinceHowManyDays: groupData['since_how_many_days'],
      spo2Level: groupData['spo2_level'],
      isOnOxygenCylinder: groupData['is_on_oxygen_cylinder'],
      covidResult: groupData['covid_result'],
      hospitalPreference: groupData['hospital_preference'],
      attenderName: groupData['attender_name'],
      attenderMobileno: groupData['attender_mobileno'],
      relationToPatient: groupData['relation_to_patient'],
      srfId: groupData['srf_id'],
      buNumber: groupData['bu_number'],
      bloodGroup: groupData['blood_group'],
      typeOfBedRequied: groupData['type_of_bed_required'],
      otherServicePatientID: groupData['other_service_patient_id'],
      otherServicePatientName: groupData['other_service_patient_name'],
      otherServiceLocation: groupData['other_service_location'],
      otherServicePhoneNo: groupData['other_service_phoneno'],
      otherServicePatientAddress: groupData['other_service_patient_address'],
      otherServiceStatus : groupData['other_service_status'],
      otherServiceCoordinatorInCharge: groupData['other_service_coordinator_in_charge'],
      otherServiceTypeOfService: groupData['other_service_type_of_service'],
      otherServiceDateTimeOfService : groupData['other_service_date_time_of_service'],
      otherServiceDatesIfAny : groupData['other_service_dates_if_any'],
      otherServiceVendor: groupData['other_service_vendor'],
      otherServiceConsultingDoctor: groupData['other_service_consulting_doctor'],
      otherServiceEquipmentID: groupData['other_service_equipment_id'],
      otherServiceEquipmentType: groupData['other_service_equipment_type'],
      otherServiceNamesOfTest: groupData['other_service_names_of_test'],
      otherServiceNoOfTestConducted : groupData['other_service_no_of_test_conducted'],
      otherServiceAreaSquareFeet : groupData['other_service_area_square_feet'],
      otherServiceNoOfEquipmentsIssued : groupData['other_service_no_of_equipment_issued'],
      otherServicePaidOrFree : groupData['other_service_paid_or_free'],
      otherServiceRate : groupData['other_service_rate'],
      otherServiceTotalCost : groupData['other_service_total_cost'],
      otherServiceCollectedBy : groupData['other_service_collected_by'],
      otherServiceAmountCollected : groupData['other_service_amount_collected'],
      otherServiceCombinedPaymentDetails : groupData['other_service_combined_payment_details'],
      otherServiceCollectionMode : groupData['other_service_collection_mode'],
      otherServiceTransactionID : groupData['other_service_transaction'],
      otherServicePaymentDate : groupData['other_service_payment_date'],
      otherServicePaymentStatus : groupData['other_service_payment_status'],
      otherServiceSurplus : groupData['other_service_surplus'],
      otherServiceShortage : groupData['other_service_shortage'],
      otherServiceRemarksOrComments : groupData['other_service_remarks_or_comments'],
      bedAllocationPatientLocation : groupData['bed_allocation_patient_location'],
      bedAllocationRemarks : groupData['bed_allocation_remarks'],
      //Introducing New Fields END
      address1: groupData['address_1'],
      address2: groupData['address_2'],
      timestamp: timeStamp,
      complainant: groupData['complainant'],
      offlineId: groupData['offline_id'],
    );

    await syncGroupMembers(groupMembers, groupId, timeStamp);

    await insertGroupTag(groupTag, groupId);

    await _updateNewGroupMessages(messages, groupId);
  }

  Future<void> _updateChatListScreen() async {
    // List groupData = await _dbHelper
    //     .getGroupDataFromOrganization(_appProvider.selectedOrganization['_id']);
    // _appProvider.setIssueGroupList(groupData);
    await _appProvider.updateIssueGroupList();
    await _chatProvider.refreshChatData();
  }

  //======================== user list data operations =======================

  Future<void> _insertUser(user, timeStamp, orgId) async {
    await _insertUserTagData(
      orgId,
      user['_id'],
      user['organization_details'],
    );

    await _dbHelper.deleteUser(user['_id'], orgId);

    await _dbHelper.insertUser(
        userName: user['username'],
        orgId: orgId,
        timestamp: timeStamp,
        userFullName: user['name'],
        userId: user['_id']);
  }

  Future<void> _insertUserTagData(
      orgId, userId, Map organizationDetails) async {
    await _dbHelper.clearUserTags(userId, orgId);

    List allTags = organizationDetails['role_tags'] +
        organizationDetails['location_tags'] +
        organizationDetails['user_group_tags'] +
        organizationDetails['dept_tags'] +
        organizationDetails['designation_tags'] +
        organizationDetails['pincode_tags'] +
        organizationDetails['other_tags'] +
        organizationDetails['group_access_tags'];

    for (Map tag in allTags) {
      await _dbHelper.insertUserTag(
        tagName: tag['tag_name'],
        orgId: orgId,
        userId: userId,
        tagId: tag['_id'],
      );
    }
    PrintString('user tag Inserted');
  }

  Future<void> _updateUser(user, timeStamp, orgId) async {
    await _insertUserTagData(
      orgId,
      user['_id'],
      user['organization_details'],
    );

    await _dbHelper.deleteUser(user['_id'], orgId);

    await _dbHelper.insertUser(
        userName: user['username'],
        orgId: orgId,
        timestamp: timeStamp,
        userFullName: user['name'],
        userId: user['_id']);
  }

  Future<void> _deleteUser(user, timeStamp, orgId) async {
    await _dbHelper.clearUserTags(user['_id'], orgId);
    await _dbHelper.deleteUser(user['_id'], orgId);
  }

  String _getFileName(String fullPath) {
    if ((fullPath?.length ?? 0) == 0) return '';
    var p = fullPath.split('/');
    return p[p.length - 1];
  }
}
