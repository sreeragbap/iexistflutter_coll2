import 'dart:async';
import 'dart:math';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:synchronized/synchronized.dart' as synchronized;
import 'package:video_compress/video_compress.dart';

class SyncFunction {
  final UserProvider userProvider;
  final AppProvider appProvider;
  final ChatScreenProvider chatScreenProvider;
  static final synchronized.Lock lock = synchronized.Lock();

  SyncFunction({this.userProvider, this.appProvider, this.chatScreenProvider});

  DBHelper _dbHelper = DBHelper.instance;
  static const String userTable = 'usersTable';
  static const String tagTable = 'tagTable';
  static const String groupTable = 'groupTable';
  static const String groupMembersTable = 'groupMembersTable';
  static const String chatMessageTable = 'chatMessageTable';
  static const String groupTagTable = 'groupTagTable';
  static const String userTagTable = 'userTagTable';

  Future<void> syncGroup() async {
    if (!lock.locked) {
      bool setSyncToRemote = true;
      await lock.synchronized(() async {
        List pendingGroups = await getPendingGroup();
        List pendingTextMessages = await getPendingTextMessage();
        List pendingNonTextMessages = await getPendingNonTextMessages();
        if (pendingGroups.isNotEmpty) {
          setSyncToRemote = false;
          for (int i = 0; i < pendingGroups.length; i++) {
            await _pushGroupToRemoteDB(pendingGroups[i]);
            PrintString(
                '====================== pushing group =======================');
          }
          Timer(Duration(seconds: 5), () {
            userProvider.setSyncToRemote(true);
          });
          PrintString('ok');
        } else if (pendingTextMessages.isNotEmpty) {
          setSyncToRemote = false;
          await _pushChatMessageToRemoteDB(pendingTextMessages);
          PrintString(
              '====================== pushing Text Message =======================');
          await _updateChatListScreen();

          Timer(Duration(seconds: 5), () {
            userProvider.setSyncToRemote(true);
          });
        } else if (pendingNonTextMessages.isNotEmpty) {
          userProvider.setSyncToRemote(false);
          await _pushMediaChatMessageToRemoteDB(pendingNonTextMessages);
          Timer(Duration(seconds: 5), () {
            userProvider.setSyncToRemote(true);
          });
        } else {
          setSyncToRemote = false;
        }
      });
      userProvider.setSyncToRemote(setSyncToRemote);
      userProvider.setSyncLatestUpdatesClick(true);
    }
  }

  Future getPendingGroup() async {
    Database db = await _dbHelper.db;
    return db.rawQuery(
        "SELECT * FROM $groupTable WHERE syncStatus = 'pending' ORDER BY timestamp");
  }

  Future getPendingTextMessage() async {
    Database db = await _dbHelper.db;
    return db.rawQuery(
        "SELECT * FROM $chatMessageTable WHERE sync_status = 'pending' AND type = 'TEXT' ORDER BY time_stamp");
  }

  Future getPendingNonTextMessages() async {
    Database db = await _dbHelper.db;
    return db.rawQuery(
        "SELECT * FROM $chatMessageTable WHERE sync_status = 'pending' AND type != 'TEXT' ORDER BY time_stamp");
  }

  Future<void> _pushGroupToRemoteDB(groupDetails) async {
    Dio dio = Dio();
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["Authorization"] = userProvider.token;
    String tempGroupId = groupDetails['groupId'];
    bool creating = tempGroupId.contains("temp");

    List membersSaved =
        await _dbHelper.getGroupMembersByGroupIdData(tempGroupId);
    List groupTagsSaved = await _dbHelper.getGroupTagByGroupIdData(tempGroupId);
    List members = [];
    List groupTags = [];
    for (int i = 0; i < groupTagsSaved.length; i++) {
      groupTags.add({
        "_id": groupTagsSaved[i]['tag_id'],
        "tag_name": groupTagsSaved[i]['tagName'],
        "tag_category": groupTagsSaved[i]['tag_category']
      });
    }
    for (int i = 0; i < membersSaved.length; i++) {
      members.add(
        {
          "_id": membersSaved[i]['_id'],
          "user_id": membersSaved[i]['user_id'],
          "user_name": membersSaved[i]['user_name'],
          "user_fullname": membersSaved[i]['user_fullname'],
          "user_type": "MEMBER",
          "group_id": tempGroupId,
        },
      );
    }

    Map body = {
      "app_group_id": tempGroupId,
      "name": groupDetails['group_name'],
      "icon": "",
      "details": "",
      "organization_id": groupDetails['orgId'],
      "organization_name": appProvider.getOrgName(userProvider.data),
      "user_id": userProvider.userId,
      "user_name": userProvider.userName,
      "user_fullname": userProvider.userNameFullName,
      "group_status": groupDetails['group_status'],
      "high_priority": groupDetails['high_priority'] == 0 ? false : true,
      "phone_no": groupDetails['phone_no'],
      "complainant": groupDetails['complainant'],
      "complaint_date": groupDetails['complaint_date'],
      //Introducing New Fields START
      "patient_name": groupDetails['patient_name'],
      "patient_age": groupDetails['patient_age'],
      "symptoms": groupDetails['symptoms'],
      "since_how_many_days": groupDetails['since_how_many_days'],
      "spo2_level": groupDetails['spo2_level'],
      "is_on_oxygen_cylinder": groupDetails['is_on_oxygen_cylinder'],
      "covid_result": groupDetails['covid_result'],
      "hospital_preference": groupDetails['hospital_preference'],
      "attender_name": groupDetails['attender_name'],
      "attender_mobileno": groupDetails['attender_mobileno'],
      "relation_to_patient": groupDetails['relation_to_patient'],
      "srf_id": groupDetails['srf_id'],
      "bu_number": groupDetails['bu_number'],
      "blood_group": groupDetails['blood_group'],
      "type_of_bed_required": groupDetails['type_of_bed_required'],
      "other_service_patient_id": groupDetails['other_service_patient_id'],
      "other_service_patient_name": groupDetails['other_service_patient_name'],
      "other_service_location": groupDetails['other_service_location'],
      "other_service_phoneno": groupDetails['other_service_phoneno'],
      "other_service_patient_address": groupDetails['other_service_patient_address'],
      "other_service_status": groupDetails['other_service_status'],
      "other_service_coordinator_in_charge":groupDetails['other_service_coordinator_in_charge'],
      "other_service_type_of_service": groupDetails['other_service_type_of_service'],
      "other_service_date_time_of_service": groupDetails['other_service_date_time_of_service'],
      "other_service_dates_if_any": groupDetails['other_service_dates_if_any'],
      "other_service_vendor": groupDetails['other_service_vendor'],
      "other_service_consulting_doctor": groupDetails['other_service_consulting_doctor'],
      "other_service_equipment_id": groupDetails['other_service_equipment_id'],
      "other_service_equipment_type": groupDetails['other_service_equipment_type'],
      "other_service_names_of_test": groupDetails['other_service_names_of_test'],
      "other_service_no_of_test_conducted": groupDetails['other_service_no_of_test_conducted'],
      "other_service_area_square_feet": groupDetails['other_service_area_square_feet'],
      "other_service_no_of_equipment_issued": groupDetails['other_service_no_of_equipment_issued'],
      "other_service_paid_or_free": groupDetails['other_service_paid_or_free'],
      "other_service_rate": groupDetails['other_service_rate'],
      "other_service_total_cost": groupDetails['other_service_total_cost'],
      "other_service_collected_by": groupDetails['other_service_collected_by'],
      "other_service_amount_collected": groupDetails['other_service_amount_collected'],
      "other_service_combined_payment_details": groupDetails['other_service_combined_payment_details'],
      "other_service_collection_mode" : groupDetails['other_service_collection_mode'],
      "other_service_transaction": groupDetails['other_service_transaction'],
      "other_service_payment_date": groupDetails['other_service_payment_date'],
      "other_service_payment_status": groupDetails['other_service_payment_status'],
      "other_service_surplus": groupDetails['other_service_surplus'],
      "other_service_shortage" : groupDetails['other_service_shortage'],
      "other_service_remarks_or_comments": groupDetails['other_service_remarks_or_comments'],
      "bed_allocation_patient_location" : groupDetails['bed_allocation_patient_location'],
      "bed_allocation_remarks": groupDetails['bed_allocation_remarks'],
      //Introducing New Fields END
      "preferred_visit_date_and_time_from":
          groupDetails['preferred_visit_date_and_time_from'],
      "preferred_visit_date_and_time_to":
          groupDetails['preferred_visit_date_and_time_to'],
      "address_1": groupDetails['address_1'],
      "address_2": groupDetails['address_2'],
      "pincode": groupDetails['pincode'],
      "members": members,
      "group_tags": groupTags,
      "offline_id": groupDetails['offline_id'],
    };

    PrintString(body);

    String api = creating
        ? createChatGroupApi
        : updateChatGroupApi.replaceAll(':id', tempGroupId);

    try {
      var resp = await dio.postExtended(api, data: body);
      Map respData = resp.data;
      PrintString(respData);
      if ((respData.containsKey('success') && respData['success']) ||
          (respData.containsKey('code') && respData['code'] == 'ID_EXISTS')) {
        await _updateGroupToLocalDB(respData, body);
      } else {
        await _updateChatListScreen();
        PrintString('Something went wrong auto update');
      }
    } catch (e) {
      await _updateChatListScreen();
      PrintString('Something went wrong');
      PrintString(e);
    }
  }

  Future<void> _updateGroupToLocalDB(respData, body) async {
    List selectedGroupMembers = body['members'];
    List selectedGroupTag = body['group_tags'];
    String oldGroupId = body['app_group_id'];
    String newGroupId = respData['id'];
    int timeStamp = respData['timestamp'];

    List chatMessage = await _dbHelper.getChatMessageByGroupIdData(oldGroupId);

    await _dbHelper.deleteGroupMembers(groupId: oldGroupId);
    //-----------------  group members  ----------------------------
    for (int i = 0; i < selectedGroupMembers.length; i++) {
      await _dbHelper.insertGroupMembers(
        timeStamp: timeStamp,
        userName: selectedGroupMembers[i]['user_name'],
        userFullName: selectedGroupMembers[i]['user_fullname'],
        syncStatus: 'complete',
        userId: selectedGroupMembers[i]['user_id'],
        groupId: newGroupId,
        id: Random().nextInt(100000).toString(),
        userType: selectedGroupMembers[i]['user_type'],
      );
    }

    //-----------------  group tag  ----------------------------
    await _dbHelper.clearGroupTags(oldGroupId);

    for (int i = 0; i < selectedGroupTag.length; i++) {
      await _dbHelper.insertGroupTag(
        groupId: newGroupId,
        tagCategory: selectedGroupTag[i]['tag_category'],
        tagName: selectedGroupTag[i]['tag_name'],
        tagId: selectedGroupTag[i]['_id'],
      );
    }

    //----------------- update group ----------------------------
    await _dbHelper.updateGroupData(
        currentGroupId: oldGroupId,
        newGroupId: newGroupId,
        name: body['name'],
        issueNo: respData['issue_no'],
        complainant: body['complainant'],
        address1: body['address_1'],
        preferredVisitDateAndTimeFrom:
            body['preferred_visit_date_and_time_from'],
        timestamp: timeStamp,
        orgId: body['organization_id'],
        address2: body['address_2'],
        complaintDate: body['complaint_date'],
        //Introducing New Fields START
        patientName: body['patient_name'],
        patientAge: body['patient_age'],
        symptoms: body['symptoms'],
        sinceHowManyDays: body['since_how_many_days'],
        spo2Level: body['spo2_level'],
        isOnOxygenCylinder: body['is_on_oxygen_cylinder'],
        covidResult: body['covid_result'],
        hospitalPreference: body['hospital_preference'],
        attenderName: body['attender_name'],
        attenderMobileno: body['attender_mobileno'],
        relationToPatient: body['relation_to_patient'],
        srfId: body['srf_id'],
        buNumber: body['bu_number'],
        bloodGroup: body['blood_group'],
        typeOfBedRequied: body['type_of_bed_required'],
        otherServicePatientID: body['other_service_patient_id'],
        otherServicePatientName: body['other_service_patient_name'],
        otherServiceLocation: body['other_service_location'],
        otherServicePhoneNo: body['other_service_phoneno'],
        otherServicePatientAddress: body['other_service_patient_address'],
        otherServiceStatus : body['other_service_status'],
        otherServiceCoordinatorInCharge: body['other_service_coordinator_in_charge'],
        otherServiceTypeOfService: body['other_service_type_of_service'],
        otherServiceDateTimeOfService : body['other_service_date_time_of_service'],
        otherServiceDatesIfAny : body['other_service_dates_if_any'],
        otherServiceVendor: body['other_service_vendor'],
        otherServiceConsultingDoctor: body['other_service_consulting_doctor'],
        otherServiceEquipmentID: body['other_service_equipment_id'],
        otherServiceEquipmentType: body['other_service_equipment_type'],
        otherServiceNamesOfTest: body['other_service_names_of_test'],
        otherServiceNoOfTestConducted : body['other_service_no_of_test_conducted'],
        otherServiceAreaSquareFeet : body['other_service_area_square_feet'],
        otherServiceNoOfEquipmentsIssued : body['other_service_no_of_equipment_issued'],
        otherServicePaidOrFree : body['other_service_paid_or_free'],
        otherServiceRate : body['other_service_rate'],
        otherServiceTotalCost : body['other_service_total_cost'],
        otherServiceCollectedBy : body['other_service_collected_by'],
        otherServiceAmountCollected : body['other_service_amount_collected'],
        otherServiceCombinedPaymentDetails : body['other_service_combined_payment_details'],
        otherServiceCollectionMode : body['other_service_collection_mode'],
        otherServiceTransactionID : body['other_service_transaction'],
        otherServicePaymentDate : body['other_service_payment_date'],
        otherServicePaymentStatus : body['other_service_payment_status'],
        otherServiceSurplus : body['other_service_surplus'],
        otherServiceShortage : body['other_service_shortage'],
        otherServiceRemarksOrComments : body['other_service_remarks_or_comments'],
        bedAllocationPatientLocation : body['bed_allocation_patient_location'],
        bedAllocationRemarks : body['bed_allocation_remarks'],
        //Introducing New Fields END
        groupStatus: body['group_status'],
        highPriority: body['high_priority'] ? 1 : 0,
        phone: body['phone_no'],
        pinCode: body['pincode'],
        preferredVisitDateAndTimeFromTo:
            body['preferred_visit_date_and_time_to'],
        syncStatus: 'complete',
        offlineId: body['offline_id']);

    //================== change old group id in chat table ======================

    for (var element in chatMessage) {
      await _dbHelper.updateChatMessage(
        oldId: element['_id'],
        newMessageId: element['_id'],
        type: element['type'],
        localPath: element['local_path'],
        file: element['file'],
        syncStatus: element['sync_status'],
        text: element['text'],
        chatGroupId: newGroupId,
        userFullName: element['user_fullname'],
        userName: element['username'],
        userId: element['user_id'],
        offlineId: element['offline_id'],
      );
    }

    await _updateChatListScreen();
  }

  Future<void> _updateChatListScreen() async {
    // List groupData = await _dbHelper
    //     .getGroupDataFromOrganization(appProvider.selectedOrganization['_id']);
    // appProvider.setIssueGroupList(groupData);
    await appProvider.updateIssueGroupList();
    await chatScreenProvider.refreshChatData();
    // PrintString("======================== group data local $groupData");
  }

  ///============================== chat message operation  section =================

  Future<void> _pushChatMessageToRemoteDB(List pendingMessageList) async {
    List data = [];
    pendingMessageList.forEach((element) {
      data.add(
        {
          "app_id": element['_id'],
          "group_id": element['chat_group_id'],
          "user_id": userProvider.userId,
          "username": userProvider.userName,
          "user_fullname": userProvider.userNameFullName,
          "chat_type": "TEXT",
          "organization_id": appProvider.selectedOrganization['_id'],
          'organization_name': appProvider.getOrgName(userProvider.data),
          "text": element['text'],
          "offline_id": element['offline_id'],
        },
      );
    });
    Map body = {'data': data};
    try {
      Dio dio = Dio();
      dio.options.headers['content-Type'] = 'application/json';
      dio.options.headers["Authorization"] = userProvider.token;
      var resp = await dio.postExtended(sendTextMessageApi, data: body);
      PrintString(resp.data);
      Map respData = resp.data;
      if ((respData.containsKey('success') && respData['success']) ||
          (respData.containsKey('code') && respData['code'] == 'ID_EXISTS')) {
        await _updateMessage(respData);
      } else {
        PrintString('something went wrong');
      }
    } catch (e) {
      PrintString(e);
    }
  }

  Future<void> _pushMediaChatMessageToRemoteDB(List pendingMessageList) async {
    for (var message in pendingMessageList) {
      try {
        Dio dio = Dio();
        dio.options.headers['content-Type'] = 'application/json';
        dio.options.headers["Authorization"] = userProvider.token;

        String data =
            '{ "group_id": "${message['chat_group_id']}", "user_id": "${userProvider.userId}", "username": "${userProvider.userName}", "user_fullname": "${userProvider.userNameFullName}", "chat_type": "${message['type']}", "organization_id": "${appProvider.selectedOrganization['_id']}", "organization_name": "${appProvider.getOrgName(userProvider.data)}", "offline_id": "${message['offline_id']}"}';

        File img = File(message['local_path']);

        if (message['type'] == "VIDEO") {
          img = await compressVideo(message, img);
        }

        var p = img.path.split('/');
        String fileName = p[p.length - 1];

        FormData formData = FormData.fromMap({
          'data': data,
          'file': await MultipartFile.fromFile(img.path,
              filename: '${DateTime.now().millisecondsSinceEpoch}_$fileName')
        });

        var resp = await dio.postExtended(
          "$sendFileApi/${userProvider.userId}",
          data: formData,
        );

        Map respData = resp.data;

        String oldId = message['_id'];

        if (respData.containsKey('success') && respData['success']) {
          await _updateMediaMessage(respData['data'], oldId, message);
        } else if (respData.containsKey('code') &&
            respData['code'] == 'ID_EXISTS') {
          var resp = respData['data'][0]; //@TODO correct it from server
          await _updateMediaMessage(resp, oldId, message);
        } else {
          PrintString('something went wrong');
        }
      } catch (e) {
        PrintString(e);
      }
    }
  }

  Future<File> compressVideo(message, File img) async {
    try {
      final _flutterVideoCompress = VideoCompress;

      final info = await _flutterVideoCompress.compressVideo(
        message['local_path'],
        quality: VideoQuality.MediumQuality,
        deleteOrigin: false,
      );

      img = info.file;
      return img;
    } catch (e) {
      PrintString(e);
      return img;
    }
  }

  Future<void> _updateMediaMessage(respData, String oldId, Map message) async {
    DBHelper dbHelper = DBHelper.instance;
    await dbHelper.updateChatMessage(
      oldId: oldId,
      newMessageId: respData['message_id'],
      file: respData['file'],
      type: message['type'],
      userId: userProvider.userId,
      userName: userProvider.userName,
      userFullName: userProvider.userNameFullName,
      chatGroupId: respData['group_id'],
      text: null,
      syncStatus: 'complete',
      localPath: message['local_path'],
      offlineId: message['offline_id'],
    );

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString(
        respData['message_id'], message['local_path']);

    await _updateChatHistory(message['chat_group_id']);
  }

  // Used to get all chats in the DB
  Future<void> _updateChatHistory(groupId) async {
    DBHelper _dbHelper = DBHelper.instance;
    List chatList = await _dbHelper.getChatMessageByGroupIdData('$groupId');
    PrintString(chatList);
    chatScreenProvider.setChatMessageList(chatList);
  }

  Future<void> _updateMessage(Map respData) async {
    List messageDataList = respData['data'];
    PrintString('=========== $messageDataList');
    // int timeStamp = respData['timestamp'];

    for (var messageData in messageDataList) {
      await _dbHelper.updateChatMessage(
        oldId: messageData['app_id'],
        newMessageId: messageData['message_id'],
        userId: userProvider.userId,
        userName: userProvider.userName,
        userFullName: userProvider.userNameFullName,
        chatGroupId: messageData['group_id'],
        text: messageData['text'],
        syncStatus: 'complete',
        file: null,
        localPath: null,
        type: messageData['chat_type'],
        offlineId: messageData['offline_id'],
      );
    }
  }
}
