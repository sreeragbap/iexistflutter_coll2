import 'dart:async';
import 'dart:io' as io;
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/shared.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart' as synchronized;
import 'package:uuid/uuid.dart';

class DBHelper {
  static Database _db;
  static const String dbName = 'master.db';

  static const String userTable = 'usersTable';
  static const String tagTable = 'tagTable';
  static const String groupTable = 'groupTable';
  static const String groupMembersTable = 'groupMembersTable';
  static const String chatMessageTable = 'chatMessageTable';
  static const String groupTagTable = 'groupTagTable';
  static const String userTagTable = 'userTagTable';
  static const String logTable = 'logTable';
  static const String messageStatusTable = 'messageStatusTable';

  static final synchronized.Lock _lock = synchronized.Lock();
  static final Uuid uuid = Uuid();

  DBHelper._privateContractor();

  // `instance` is the singleton instance of DB
  static final DBHelper instance = DBHelper._privateContractor();

  // `_db` is the database instance used everywhere in the app
  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDB();
    return _db;
  }

  // To initialize the Database
  Future initDB() async {
    String path = join(await getStorageLoc(), dbName);
    var db = await openDatabase(path,
        version: 5, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return db;
  }

  // To create all tables
  FutureOr<void> _onCreate(Database database, int version) async {
    Batch batch = database.batch();

    batch.execute('''
            CREATE TABLE $userTable (
            userId VARCHAR(24),
            userName VARCHAR(50),
            userFullName VARCHAR(100),
            orgId VARCHAR(24),
            timeStamp int(11)
            );
          ''');

    batch.execute('''
            CREATE TABLE $tagTable (
            tagId VARCHAR(24),
            tagCategory VARCHAR(50),
            tagName VARCHAR(100),
            orgId VARCHAR(24)
            );
          ''');

    batch.execute('''
            CREATE TABLE $groupTable (
            groupId VARCHAR(24),
            group_name VARCHAR(200),
            group_status VARCHAR(50),
            orgId VARCHAR(24),
            syncStatus varchar(10),
            timestamp int(11),
            high_priority int(4),
            complainant varchar(100),
            phone_no varchar(20),
            complaint_date int(11),
            patient_name varchar(100),
            patient_age varchar(3),
            symptoms varchar(100),
            since_how_many_days varchar(20),
            spo2_level varchar(100),
            is_on_oxygen_cylinder varchar(20),
            covid_result varchar(100),
            hospital_preference varchar(200),
            attender_name varchar(100),
            attender_mobileno varchar(20),
            relation_to_patient varchar(100),
            srf_id varchar(100),
            bu_number varchar(100),
            blood_group varchar(20),
            type_of_bed_required varchar(20),
            other_service_patient_id varchar(20),
            other_service_patient_name varchar(20),
            other_service_location varchar(20),
            other_service_phoneno varchar(20),
            other_service_patient_address varchar(20),
            other_service_status varchar(20),
            other_service_coordinator_in_charge varchar(20),
            other_service_type_of_service varchar(20),
            other_service_date_time_of_service int(11),
            other_service_dates_if_any varchar(20),
            other_service_vendor varchar(20),
            other_service_consulting_doctor varchar(20),
            other_service_equipment_id varchar(20),
            other_service_equipment_type varchar(20),
            other_service_names_of_test varchar(20),
            other_service_no_of_test_conducted varchar(20),
            other_service_area_square_feet varchar(20),
            other_service_no_of_equipment_issued varchar(20),
            other_service_paid_or_free varchar(20),
            other_service_rate varchar(20),
            other_service_total_cost varchar(20),
            other_service_collected_by varchar(20),
            other_service_amount_collected varchar(20),
            other_service_combined_payment_details varchar(20),
            other_service_collection_mode varchar(20),
            other_service_transaction varchar(20),
            other_service_payment_date int(11),
            other_service_payment_status varchar(20),
            other_service_surplus varchar(20),
            other_service_shortage varchar(20),
            other_service_remarks_or_comments varchar(20),
            bed_allocation_patient_location varchar(20),
            bed_allocation_remarks varchar(20),
            preferred_visit_date_and_time_from int(11),
            preferred_visit_date_and_time_to int(11),
            address_1 varchar(100),
            address_2 varchar(100),
            pincode varchar(20),
            issue_no varchar(50),
            unread_message_count int(4) NOT NULL DEFAULT 0,
            offline_id varchar(50)
            );
          ''');

    batch.execute('''
            CREATE TABLE $groupMembersTable (
            _id VARCHAR(24),
            user_id VARCHAR(24),
            user_name VARCHAR(50),
            user_fullname VARCHAR(50),
            user_type VARCHAR(20),
            group_id VARCHAR(24),
            sync_status varchar(10),
            time_stamp int(11)
            );
          ''');

    batch.execute('''
            CREATE TABLE $chatMessageTable (
            _id VARCHAR(24),
            user_id VARCHAR(24),
            username VARCHAR(50),
            type VARCHAR(10),
            user_fullname VARCHAR(50),
            chat_group_id VARCHAR(24),
            text TEXT,
            file TEXT,
            local_path TEXT,
            sync_status varchar(10),
            time_stamp int(11),
            offline_id varchar(50)
            );
          ''');
    batch.execute('''
            CREATE TABLE $groupTagTable (
            group_id VARCHAR(24),
            tag_id VARCHAR(24),
            tagName VARCHAR(100),
            tag_category VARCHAR(50)
            );
          ''');

    batch.execute('''
            CREATE TABLE $userTagTable (
            user_id VARCHAR(24),
            tag_id VARCHAR(24),
            tagName VARCHAR(100),
            orgId VARCHAR(24)
            );
          ''');

    _createLogTable(batch);
    _createMessageStatusTable(batch);

    List<dynamic> res = await batch.commit();
    PrintString('print db $res');
  }

  void _createMessageStatusTable(Batch batch) {
    batch.execute('''
            CREATE TABLE $messageStatusTable (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            time_stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            user_full_name VARCHAR(200),
            _id VARCHAR(24),
            message_id VARCHAR(24),
            user_id VARCHAR(24),
            group_id VARCHAR(24),
            sent_time VARCHAR(100),
            read_time VARCHAR(100)
            );
          ''');
  }

  void _createLogTable(Batch batch) {
    batch.execute('''
            CREATE TABLE $logTable (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            time_stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            message TEXT,
            level VARCHAR(12),
            user_id VARCHAR(24),
            org_id VARCHAR(24)
            );
          ''');
  }

  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    Batch batch = db.batch();
    if (oldVersion <= 1 && newVersion > 1) {
      batch.execute('''
            ALTER TABLE $groupTable ADD COLUMN unread_message_count int(4) NOT NULL DEFAULT 0;
          ''');
    }

    if (oldVersion <= 2 && newVersion > 2) {
      _createLogTable(batch);
    }

    if (oldVersion <= 3 && newVersion > 3) {
      batch.execute('''
            ALTER TABLE $groupTable ADD COLUMN offline_id varchar(50);
          ''');

      batch.execute('''
            ALTER TABLE $chatMessageTable ADD COLUMN offline_id varchar(50);
          ''');
    }

    if (oldVersion <= 4 && newVersion > 4) {
      _createMessageStatusTable(batch);
    }

    List<dynamic> res = await batch.commit();
    PrintString('print db $res');
  }

  //=================== user table operations ==========================
  Future insertUser({userId, userName, userFullName, orgId, timestamp}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "userId": userId,
      "userName": userName,
      "userFullName": userFullName,
      "orgId": orgId,
      "timeStamp": timestamp,
    };
    return db.insert(userTable, userData);
  }

  Future getUserFromId(String id, String orgId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT * FROM $userTable WHER userId = '$id' AND orgId = '$orgId'");
  }

  Future getAllUsersData(orgId) async {
    Database db = await instance.db;
    return db.query(userTable);
  }

  Future getUserDataFromOrganization(String id) async {
    PrintString(id);
    Database db = await instance.db;
    return db.rawQuery("SELECT * FROM $userTable WHERE orgId = '$id'");
  }

  Future clearUserTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $userTable");
  }

  Future deleteUser(userId, orgId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "DELETE FROM $userTable WHERE userId = '$userId' AND orgId = '$orgId'");
  }

  //=================== tag table operations ==========================
  Future insertTag({tagId, tagCategory, tagName, orgId}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "tagId": tagId,
      "tagCategory": tagCategory,
      "tagName": tagName,
      "orgId": orgId,
    };
    return db.insert(tagTable, userData);
  }

  Future<List<Map<String, dynamic>>> getTagDataFromOrganization(
      String id) async {
    PrintString(id);
    Database db = await instance.db;
    return db.rawQuery("SELECT * FROM $tagTable WHERE orgId = '$id'");
  }

  Future clearTagTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $tagTable");
  }

  //=================== group table operations ==========================
  Future insertGroup({
    groupId,
    name,
    issueNo,
    groupStatus,
    orgId,
    syncStatus,
    timestamp,
    int highPriority,
    complainant,
    //Introducing New Fields START
    patientName,
    patientAge,
    symptoms,
    sinceHowManyDays,
    spo2Level,
    isOnOxygenCylinder,
    covidResult,
    hospitalPreference,
    attenderName,
    attenderMobileno,
    relationToPatient,
    srfId,
    buNumber,
    bloodGroup,
    typeOfBedRequied,
    otherServicePatientID,
    otherServicePatientName,
    otherServiceLocation,
    otherServicePhoneNo,
    otherServicePatientAddress,
    otherServiceStatus,
    otherServiceCoordinatorInCharge,
    otherServiceTypeOfService,
    otherServiceDateTimeOfService,
    otherServiceDatesIfAny,
    otherServiceVendor,
    otherServiceConsultingDoctor,
    otherServiceEquipmentID,
    otherServiceEquipmentType,
    otherServiceNamesOfTest,
    otherServiceNoOfTestConducted,
    otherServiceAreaSquareFeet,
    otherServiceNoOfEquipmentsIssued,
    otherServicePaidOrFree,
    otherServiceRate,
    otherServiceTotalCost,
    otherServiceCollectedBy,
    otherServiceAmountCollected,
    otherServiceCombinedPaymentDetails,
    otherServiceCollectionMode,
    otherServiceTransactionID,
    otherServicePaymentDate,
    otherServicePaymentStatus,
    otherServiceSurplus,
    otherServiceShortage,
    otherServiceRemarksOrComments,
    bedAllocationPatientLocation,
    bedAllocationRemarks,
    //Introducing New Fields END
    phone,
    complaintDate,
    preferredVisitDateAndTimeFrom,
    preferredVisitDateAndTimeFromTo,
    address1,
    address2,
    pinCode,
    offlineId,
  }) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "groupId": groupId,
      "group_name": name,
      "issue_no": issueNo,
      "group_status": groupStatus,
      "orgId": orgId,
      "syncStatus": syncStatus,
      "timestamp": timestamp,
      "high_priority": highPriority,
      "complainant": complainant,
      //Introducing New Fields START
      "patient_name": patientName,
      "patient_age": patientAge,
      "symptoms": symptoms,
      "since_how_many_days": sinceHowManyDays,
      "spo2_level": spo2Level,
      "is_on_oxygen_cylinder": isOnOxygenCylinder,
      "covid_result": covidResult,
      "hospital_preference": hospitalPreference,
      "attender_name": attenderName,
      "attender_mobileno": attenderMobileno,
      "relation_to_patient": relationToPatient,
      "srf_id": srfId,
      "bu_number": buNumber,
      "blood_group": bloodGroup,
      "type_of_bed_required": typeOfBedRequied,
      "other_service_patient_id": otherServicePatientID,
      "other_service_patient_name": otherServicePatientName,
      "other_service_location": otherServiceLocation,
      "other_service_phoneno": otherServicePhoneNo,
      "other_service_patient_address": otherServicePatientAddress,
      "other_service_status": otherServiceStatus,
      "other_service_coordinator_in_charge": otherServiceCoordinatorInCharge,
      "other_service_type_of_service": otherServiceTypeOfService,
      "other_service_date_time_of_service": otherServiceDateTimeOfService,
      "other_service_dates_if_any": otherServiceDatesIfAny,
      "other_service_vendor": otherServiceVendor,
      "other_service_consulting_doctor": otherServiceConsultingDoctor,
      "other_service_equipment_id": otherServiceEquipmentID,
      "other_service_equipment_type": otherServiceEquipmentType,
      "other_service_names_of_test": otherServiceNamesOfTest,
      "other_service_no_of_test_conducted": otherServiceNoOfTestConducted,
      "other_service_area_square_feet": otherServiceAreaSquareFeet,
      "other_service_no_of_equipment_issued": otherServiceNoOfEquipmentsIssued,
      "other_service_paid_or_free": otherServicePaidOrFree,
      "other_service_rate": otherServiceRate,
      "other_service_total_cost": otherServiceTotalCost,
      "other_service_collected_by": otherServiceCollectedBy,
      "other_service_amount_collected": otherServiceAmountCollected,
      "other_service_combined_payment_details": otherServiceCombinedPaymentDetails,
      "other_service_collection_mode" : otherServiceCollectionMode,
      "other_service_transaction": otherServiceTransactionID,
      "other_service_payment_date": otherServicePaymentDate,
      "other_service_payment_status": otherServicePaymentStatus,
      "other_service_surplus": otherServiceSurplus,
      "other_service_shortage" : otherServiceShortage,
      "other_service_remarks_or_comments": otherServiceRemarksOrComments,
      "bed_allocation_patient_location": bedAllocationPatientLocation,
      "bed_allocation_remarks": bedAllocationRemarks,
      //Introducing New Fields END
      "phone_no": phone,
      "complaint_date": complaintDate,
      "preferred_visit_date_and_time_from": preferredVisitDateAndTimeFrom,
      "preferred_visit_date_and_time_to": preferredVisitDateAndTimeFromTo,
      "address_1": address1,
      "address_2": address2,
      "pincode": pinCode,
      "offline_id": offlineId ?? uuid.v4()
    };
    return db.insert(groupTable, userData);
  }

  Future getAllGroupData() async {
    Database db = await instance.db;
    return db.query(groupTable);
  }

  Future getGroupDataFromOrganization(String organizationId) async {
    PrintString(organizationId);
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT * FROM $groupTable WHERE orgId = '$organizationId' ORDER BY timestamp DESC");
  }

  Future getGroupData(String Id) async {
    PrintString(Id);
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT * FROM $groupTable WHERE groupId = '$Id' ORDER BY timestamp DESC LIMIT 1");
  }

  Future updateGroupData({
    currentGroupId,
    newGroupId,
    name,
    issueNo,
    groupStatus,
    orgId,
    syncStatus,
    timestamp,
    int highPriority,
    complainant,
    //Introducing New Fields START
    patientName,
    patientAge,
    symptoms,
    sinceHowManyDays,
    spo2Level,
    isOnOxygenCylinder,
    covidResult,
    hospitalPreference,
    attenderName,
    attenderMobileno,
    relationToPatient,
    srfId,
    buNumber,
    bloodGroup,
    typeOfBedRequied,
    otherServicePatientID,
    otherServicePatientName,
    otherServiceLocation,
    otherServicePhoneNo,
    otherServicePatientAddress,
    otherServiceStatus,
    otherServiceCoordinatorInCharge,
    otherServiceTypeOfService,
    otherServiceDateTimeOfService,
    otherServiceDatesIfAny,
    otherServiceVendor,
    otherServiceConsultingDoctor,
    otherServiceEquipmentID,
    otherServiceEquipmentType,
    otherServiceNamesOfTest,
    otherServiceNoOfTestConducted,
    otherServiceAreaSquareFeet,
    otherServiceNoOfEquipmentsIssued,
    otherServicePaidOrFree,
    otherServiceRate,
    otherServiceTotalCost,
    otherServiceCollectedBy,
    otherServiceAmountCollected,
    otherServiceCombinedPaymentDetails,
    otherServiceCollectionMode,
    otherServiceTransactionID,
    otherServicePaymentDate,
    otherServicePaymentStatus,
    otherServiceSurplus,
    otherServiceShortage,
    otherServiceRemarksOrComments,
    bedAllocationPatientLocation,
    bedAllocationRemarks,
    //Introducing New Fields END
    phone,
    complaintDate,
    preferredVisitDateAndTimeFrom,
    preferredVisitDateAndTimeFromTo,
    address1,
    address2,
    pinCode,
    offlineId,
  }) async {
    Map<String, dynamic> userData = {
      "groupId": newGroupId,
      "group_name": name,
      "issue_no": issueNo,
      "group_status": groupStatus,
      "orgId": orgId,
      "syncStatus": syncStatus,
      "timestamp": timestamp,
      "high_priority": highPriority,
      "complainant": complainant,
      //Introducing New Fields START
      "patient_name": patientName,
      "patient_age": patientAge,
      "symptoms": symptoms,
      "since_how_many_days": sinceHowManyDays,
      "spo2_level": spo2Level,
      "is_on_oxygen_cylinder": isOnOxygenCylinder,
      "covid_result": covidResult,
      "hospital_preference": hospitalPreference,
      "attender_name": attenderName,
      "attender_mobileno": attenderMobileno,
      "relation_to_patient": relationToPatient,
      "srf_id": srfId,
      "bu_number": buNumber,
      "blood_group": bloodGroup,
      "type_of_bed_required": typeOfBedRequied,
      "other_service_patient_id": otherServicePatientID,
      "other_service_patient_name": otherServicePatientName,
      "other_service_location": otherServiceLocation,
      "other_service_phoneno": otherServicePhoneNo,
      "other_service_patient_address": otherServicePatientAddress,
      "other_service_status": otherServiceStatus,
      "other_service_coordinator_in_charge": otherServiceCoordinatorInCharge,
      "other_service_type_of_service": otherServiceTypeOfService,
      "other_service_date_time_of_service": otherServiceDateTimeOfService,
      "other_service_dates_if_any": otherServiceDatesIfAny,
      "other_service_vendor": otherServiceVendor,
      "other_service_consulting_doctor": otherServiceConsultingDoctor,
      "other_service_equipment_id": otherServiceEquipmentID,
      "other_service_equipment_type": otherServiceEquipmentType,
      "other_service_names_of_test": otherServiceNamesOfTest,
      "other_service_no_of_test_conducted": otherServiceNoOfTestConducted,
      "other_service_area_square_feet": otherServiceAreaSquareFeet,
      "other_service_no_of_equipment_issued": otherServiceNoOfEquipmentsIssued,
      "other_service_paid_or_free": otherServicePaidOrFree,
      "other_service_rate": otherServiceRate,
      "other_service_total_cost": otherServiceTotalCost,
      "other_service_collected_by": otherServiceCollectedBy,
      "other_service_amount_collected": otherServiceAmountCollected,
      "other_service_combined_payment_details": otherServiceCombinedPaymentDetails,
      "other_service_collection_mode" : otherServiceCollectionMode,
      "other_service_transaction": otherServiceTransactionID,
      "other_service_payment_date": otherServicePaymentDate,
      "other_service_payment_status": otherServicePaymentStatus,
      "other_service_surplus": otherServiceSurplus,
      "other_service_shortage" : otherServiceShortage,
      "other_service_remarks_or_comments": otherServiceRemarksOrComments,
      "bed_allocation_patient_location": bedAllocationPatientLocation,
      "bed_allocation_remarks": bedAllocationRemarks,
      //Introducing New Fields START
      "phone_no": phone,
      "complaint_date": complaintDate,
      "preferred_visit_date_and_time_from": preferredVisitDateAndTimeFrom,
      "preferred_visit_date_and_time_to": preferredVisitDateAndTimeFromTo,
      "address_1": address1,
      "address_2": address2,
      "pincode": pinCode,
      "offline_id": offlineId
    };
    Database db = await instance.db;
    return db.update(groupTable, userData,
        where: "groupId = ?", whereArgs: [currentGroupId]);
  }

  Future updateGroupVal(groupId, data) async {
    Database db = await instance.db;
    return db
        .update(groupTable, data, where: "groupId = ?", whereArgs: [groupId]);
  }

  Future updateReadCountGroupTable({groupId, count}) async {
    Database db = await instance.db;
    return db.update(
      groupTable,
      {"unread_message_count": count},
      where: "groupId = ?",
      whereArgs: [groupId],
    );
  }

  Future clearGroupTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $groupTable");
  }

  Future clearGroupTableWithTime(time) async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $groupTable WHERE timestamp < $time");
  }

  //=================== Group members table operations ==========================
  Future insertGroupMembers(
      {id,
      userId,
      userName,
      userFullName,
      userType,
      groupId,
      syncStatus,
      timeStamp}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "_id": id,
      "user_id": userId,
      "user_name": userName,
      "user_fullname": userFullName,
      "user_type": userType,
      "group_id": groupId,
      "sync_status": syncStatus,
      "time_stamp": timeStamp,
    };
    return db.insert(groupMembersTable, userData);
  }

  Future deleteGroupMembers({groupId}) async {
    Database db = await instance.db;
    return db
        .rawQuery("DELETE FROM $groupMembersTable WHERE group_id = '$groupId'");
  }

  Future updateGroupMembers(
      {id,
      userId,
      userName,
      userFullName,
      userType,
      groupId,
      syncStatus,
      timeStamp}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "_id": id,
      "user_id": userId,
      "user_name": userName,
      "user_fullname": userFullName,
      "user_type": userType,
      "group_id": groupId,
      "sync_status": syncStatus,
      "time_stamp": timeStamp,
    };
    return db.update(groupMembersTable, userData);
  }

  Future getAllGroupMembersData() async {
    Database db = await instance.db;
    return db.query(groupMembersTable);
  }

  Future getGroupMembersByGroupIdData(id) async {
    Database db = await instance.db;
    return db
        .rawQuery("SELECT * FROM $groupMembersTable WHERE group_id = '$id'");
  }

  Future getGroupMembersByIdData(groupId, id) async {
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT * FROM $groupMembersTable WHERE  group_id = '$groupId' and user_id = '$id' LIMIT 1");
  }

  Future deleteGroupMemberByGroupIdData(groupId, memberId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "DELETE FROM  $groupMembersTable WHERE group_id = '$groupId' and user_id = '$memberId'");
  }

  Future clearGroupMembersTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $groupMembersTable");
  }

  Future clearGroupMembersTableWithTime(time) async {
    Database db = await instance.db;
    return db
        .rawQuery("DELETE FROM $groupMembersTable WHERE time_stamp < $time");
  }

  //=================== chat message table operations ==========================
  Future insertChatMessage({
    id,
    userId,
    userName,
    userFullName,
    type,
    chatGroupId,
    text,
    file,
    localPath,
    syncStatus,
    timeStamp,
    offlineId,
  }) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "_id": id,
      "user_id": userId,
      "username": userName,
      "user_fullname": userFullName,
      "type": type,
      "chat_group_id": chatGroupId,
      "text": text,
      "file": file,
      "local_path": localPath,
      "sync_status": syncStatus,
      "time_stamp": timeStamp,
      "offline_id": offlineId ?? uuid.v4()
    };
    return db.insert(chatMessageTable, userData);
  }

  Future getAllChatMessages() async {
    Database db = await instance.db;
    return db.query(chatMessageTable);
  }

  Future getChatMessageByGroupIdData(id) async {
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT DISTINCT * FROM $chatMessageTable WHERE chat_group_id = '$id' ORDER BY time_stamp DESC");
  }

  Future clearChatMessagesTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $chatMessageTable");
  }

  Future clearChatMessagesTableWithTime(time) async {
    Database db = await instance.db;
    return db
        .rawQuery("DELETE FROM $chatMessageTable WHERE time_stamp < $time");
  }

  Future updateChatMessage({
    oldId,
    newMessageId,
    userId,
    userName,
    userFullName,
    type,
    chatGroupId,
    text,
    file,
    localPath,
    syncStatus,
    offlineId,
  }) async {
    Database db = await instance.db;
    Map<String, dynamic> messageData = {
      "_id": newMessageId,
      "user_id": userId,
      "username": userName,
      "user_fullname": userFullName,
      "type": type,
      "chat_group_id": chatGroupId,
      "text": text,
      "file": file,
      "local_path": localPath,
      "sync_status": syncStatus,
      "offline_id": offlineId,
    };
    return db.update(
      chatMessageTable,
      messageData,
      where: "_id = ?",
      whereArgs: [oldId],
    );
  }

  Future updateChatMessageVal(id, data) async {
    Database db = await instance.db;

    return db.update(
      chatMessageTable,
      data,
      where: "_id = ?",
      whereArgs: [id],
    );
  }

  //=================== Group tag table operations ==========================
  Future insertGroupTag({groupId, tagId, tagName, tagCategory}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "group_id": groupId,
      "tag_id": tagId,
      "tagName": tagName,
      "tag_category": tagCategory
    };
    return db.insert(groupTagTable, userData);
  }

  Future updateGroupTag({groupId, tagId, tagName, tagCategory}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "group_id": groupId,
      "tag_id": tagId,
      "tagName": tagName,
      "tag_category": tagCategory
    };
    return db.update(groupTagTable, userData);
  }

  Future getAllGroupTag() async {
    Database db = await instance.db;
    return db.query(groupTagTable);
  }

  Future getGroupTagByGroupIdData(id) async {
    Database db = await instance.db;
    return db.rawQuery("SELECT * FROM $groupTagTable WHERE group_id = '$id'");
  }

  Future clearGroupTagTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $groupTagTable");
  }

  Future clearGroupTags(groupId) async {
    Database db = await instance.db;
    return db
        .rawQuery("DELETE FROM $groupTagTable WHERE group_id = '$groupId'");
  }

  //=================== user tag table operations ==========================
  Future insertUserTag({userId, tagId, tagName, orgId}) async {
    Database db = await instance.db;
    Map<String, dynamic> userData = {
      "user_id": userId,
      "tag_id": tagId,
      "tagName": tagName,
      "orgId": orgId,
    };
    return db.insert(userTagTable, userData);
  }

  Future getUserTagFromUserId(id, orgId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT * FROM $userTagTable WHERE user_id = '$id' AND orgId = '$orgId'");
  }

  Future clearUserTagTable() async {
    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $userTagTable");
  }

  Future clearUserTags(userId, orgId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "DELETE FROM $userTagTable WHERE user_id = '$userId' AND orgId = '$orgId'");
  }

  Future getUserListMax(orgId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT MAX(timeStamp) FROM $userTable where orgId = '$orgId'");
  }

  Future getGroupListMax(orgId) async {
    Database db = await instance.db;
    return db.rawQuery(
        "SELECT MAX(timestamp) FROM $groupTable where orgId = '$orgId'");
  }

  Future getMessageListMax(orgId, excludeUserId) async {
    Database db = await instance.db;

    return db.rawQuery(
        "SELECT MAX($chatMessageTable.time_stamp) FROM $chatMessageTable INNER JOIN $groupTable ON $chatMessageTable.chat_group_id=$groupTable.groupId WHERE $groupTable.orgId = '$orgId' AND $chatMessageTable.user_id != '$excludeUserId'");
  }

  Future addLogMessage(String message, String level) async {
    //await _lock.synchronized(() async {
    try {
      AppProvider appProvider = AppProvider();
      String orgId = '';
      if (null != appProvider.selectedOrganization &&
          appProvider.selectedOrganization.containsKey('_id')) {
        orgId = appProvider.selectedOrganization['_id'];
      }

      Map<String, dynamic> logData = {
        "message": message,
        "level": level,
        "user_id": UserProvider().userId ?? '',
        "org_id": orgId,
      };

      Database db = await instance.db;
      await db.insert(logTable, logData);
    } on Exception catch (e) {
      // TODO
    }
    //});
  }

  Future getLogMessages({num timestamp}) async {
    String whereClause =
        timestamp != null ? " where time_stamp >= '$timestamp'" : '';

    Database db = await instance.db;
    return db.rawQuery("SELECT * FROM $logTable$whereClause");
  }

  Future clearLogMessages(timestamp) async {
    String whereClause = "where time_stamp < '$timestamp'";

    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $logTable $whereClause");
  }

  Future addMessageStatus({
    id,
    message_id,
    user_id,
    user_fullname,
    sent_time,
    read_time,
    group_id,
  }) async {
    Map<String, dynamic> data = {
      '_id': id,
      'message_id': message_id,
      'user_id': user_id,
      'user_full_name': user_fullname,
      'sent_time': sent_time,
      'read_time': read_time,
      'group_id': group_id,
      'time_stamp': DateTime.now().millisecondsSinceEpoch ~/ 1000
    };

    Database db = await instance.db;
    await db.insert(messageStatusTable, data);
  }

  Future getMessageStatus(messageId, userId) async {
    String whereClause =
        "where message_id = '$messageId' and user_id != '$userId'";

    Database db = await instance.db;
    return db.rawQuery("SELECT * FROM $messageStatusTable $whereClause");
  }

  Future clearAMessagesStatus(messageId) async {
    String whereClause = "where message_id = '$messageId'";

    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $messageStatusTable $whereClause");
  }

  Future clearMessagesStatus(timestamp) async {
    String whereClause =
        timestamp != null ? " where time_stamp >= '$timestamp'" : '';

    Database db = await instance.db;
    return db.rawQuery("DELETE FROM $messageStatusTable$whereClause");
  }
}
