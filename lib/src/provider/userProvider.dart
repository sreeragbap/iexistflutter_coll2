import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:iexist/src/screens/firstPage/firstSyncLoading.dart';
import 'package:iexist/src/syncFunction/latestDataSyncFunction.dart';
import 'package:iexist/src/syncFunction/syncFunction.dart';
import 'package:iexist/src/utilities/sharedPreferenceKeys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProvider extends ChangeNotifier {
  static final UserProvider _singleton = UserProvider._internal();
  factory UserProvider() => _singleton;
  UserProvider._internal();

  Map data = {};
  bool syncToRemote = false;
  bool firstSync = true;
  bool saving = false;
  bool autoDownload = false;
  bool disableNotification = false;
  bool syncLatestUpdatesClick = false;
  bool showLoading = false;
  String token,
      userId,
      userName,
      userNameFullName,
      profilePic,
      userEmail,
      address,
      pincode,
      userPhone;

  void setUserData() {
    this.token = data['token'];
    this.userId = data['_id'];
    this.userNameFullName = data['name'];
    this.userName = data['username'];
    this.profilePic = data['image'];
    this.userEmail = data['email'];
    this.address = data['address'];
    this.pincode = data['pincode'];
    this.userPhone = data['phone'];

    this.firstSync = data['firstSync'] ?? true;
    this.autoDownload = data['autoDownload'] ?? false;
    this.disableNotification = data['disableNotification'] ?? false;

    notifyListeners();
  }

  Future<void> reset() async {
    this.data = {};
    await save();
    setUserData();
  }

  getProfileBody() {
    var body = {...data};

    body['user_id'] = this.userId;

    var keysToExclude = [
      'firstSync',
      'autoDownload',
      'disableNotification',
      'token',
      '_id',
      'image',
    ];

    for (var key in keysToExclude) {
      body.remove(key);
    }

    return body;
  }

  Future<void> init({data}) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (data != null) {
      data['firstSync'] = firstSync;
      data['autoDownload'] = autoDownload;
      data['disableNotification'] = disableNotification;

      this.data = data;
      await save();
    } else {
      var cache = sharedPreferences.getString(completeUserProfileDataSaveData);
      this.data = cache != null ? json.decode(cache) : {};
    }

    setUserData();
  }

  Future<void> save() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.setString(
        completeUserProfileDataSaveData, json.encode(data));
  }

  Future<void> setPic({
    profilePic,
  }) async {
    this.profilePic = profilePic;

    data['image'] = profilePic;
    await save();

    notifyListeners();
  }

  void setFirstSyncStatus(bool status) {
    FirstSyncLoading.lock.synchronized(() {
      if (firstSync != status) {
        firstSync = status;
        setVal('firstSync', status);
      }
    });
  }

  void setSaving(bool status) {
    if (saving != status) {
      saving = status;
      notifyListeners();
    }
  }

  void setVal(key, val) async {
    if (key == "autoDownload") {
      autoDownload = val;
    } else if (key == "disableNotification") {
      disableNotification = val;
    } else if (key == "name") {
      userNameFullName = val;
    } else if (key == "address") {
      address = val;
    } else if (key == "email") {
      userEmail = val;
    } else if (key == "phone") {
      userPhone = val;
    } else if (key == "pincode") {
      pincode = val;
    }

    data[key] = val;
    await save();

    notifyListeners();
  }

  void setSyncToRemote(bool status) {
    SyncFunction.lock.synchronized(() {
      if (syncToRemote != status) {
        syncToRemote = status;
        notifyListeners();
      }
    });
  }

  void setSyncLatestUpdatesClick(bool status) {
    LatestDataSyncFunction.lock.synchronized(() {
      if (syncLatestUpdatesClick != status) {
        syncLatestUpdatesClick = status;
        showLoading = status;
        notifyListeners();
      }
    });
  }

  void setLoading(bool status) {
    LatestDataSyncFunction.lock.synchronized(() {
      if (showLoading != status) {
        showLoading = status;
        notifyListeners();
      }
    });
  }
}
