import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';

class ChatScreenProvider extends ChangeNotifier {
  static final ChatScreenProvider _singleton = ChatScreenProvider._internal();
  factory ChatScreenProvider() => _singleton;
  ChatScreenProvider._internal();

  Map groupDetails = {};
  List chatMessage = [];
  Map selectedMessage;
  List _groupMembersList = [];
  List _removedGroups = [];
  List get groupMembersList => _groupMembersList;
  set groupMembersList(List value) {
    _groupMembersList = value;
    notifyListeners();
  }

  String _currentChatGroupId;
  String get currentChatGroupId => _currentChatGroupId;
  set currentChatGroupId(String value) {
    if (currentChatGroupId != value) {
      _currentChatGroupId = value;
      refreshChatData();
      AppProvider().updateIssueGroupList();
    }
  }

  String getLastMessageId() {
    return chatMessage.isEmpty ? null : chatMessage.last['_id'];
  }

  void setChatMessageList(List chatMessageList) {
    chatMessage = chatMessageList;
    notifyListeners();
  }

  bool isMessageSelected(message) {
    return selectedMessage != null && selectedMessage['_id'] == message['_id'];
  }

  Future<List> getStatus(message) async {
    return await DBHelper.instance.getMessageStatus(message['_id'], message['user_id']);
  }

  bool isReadOnly(config) {
    List readonlyStatus = config['readonly_group_statuses'] ?? [];
    bool readOnly = readonlyStatus.contains(groupDetails['group_status']) ||
        _removedGroups.contains(groupDetails["groupId"]);

    return readOnly;
  }

  void copySelectedMessage() {
    Clipboard.setData(ClipboardData(text: selectedMessage['text']));
    selectedMessage = null;
  }

  void setSelectedMessage(message) {
    if (isMessageSelected(message)) {
      selectedMessage = null;
    } else {
      selectedMessage = message;
    }
    notifyListeners();
  }

  void unSelectedMessage() {
    selectedMessage = null;
    notifyListeners();
  }

  int search(query, direction, {index = 0}) {
    var message;
    var newIndex;

    if (direction == 'up') {
      var length = index >= -1 ? index + 1 : index;

      var scope = chatMessage.skip(length);

      try {
        message = scope.firstWhere((element) =>
            element['type'] == 'TEXT' && element['text'].contains(query));
      } catch (e) {
        return null;
      }

      if (message != null) {
        newIndex = scope
            .toList()
            .indexWhere((element) => element['_id'] == message['_id']);
      } else {
        return null;
      }

      return newIndex + length;
    } else {
      var length = index > -1 ? index : 0;

      var scope = chatMessage.take(length);

      try {
        message = scope.lastWhere((element) =>
            element['type'] == 'TEXT' && element['text'].contains(query));
      } catch (e) {
        return null;
      }
      if (message != null) {
        newIndex = scope
            .toList()
            .indexWhere((element) => element['_id'] == message['_id']);
      } else {
        return null;
      }

      return newIndex;
    }
  }

  Future<void> refreshChatData() async {
    if (null != currentChatGroupId && currentChatGroupId.isNotEmpty) {
      List data = await DBHelper.instance.getGroupData(currentChatGroupId);
      if (data.isNotEmpty) {
        groupDetails = data[0];
      }

      List chatList = await DBHelper.instance
          .getChatMessageByGroupIdData('$currentChatGroupId');
      PrintString(chatList);
      setChatMessageList(chatList);

      groupMembersList = await DBHelper.instance
          .getGroupMembersByGroupIdData('$currentChatGroupId');

      notifyListeners();
    } else {
      _groupMembersList = [];
      chatMessage = [];
      groupDetails = {};
      selectedMessage = null;
    }
  }

  FutureOr<void> markChatGroupRead(String groupId) async {
    await DBHelper.instance
        .updateReadCountGroupTable(groupId: groupId, count: 0);

    notifyListeners();
  }

  Future<void> removeUserFromGroup(groupId) async {
    await DBHelper.instance
        .deleteGroupMemberByGroupIdData(groupId, UserProvider().userId);
    _removedGroups.add(groupId);
    notifyListeners();
  }

  void reset() {
    _removedGroups.clear();
    notifyListeners();
  }
}
