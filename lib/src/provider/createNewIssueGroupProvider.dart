import 'package:flutter/material.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/ui_helper.dart';
import 'package:iexist/src/utilities/user_permission_map_keys.dart';
import 'package:toast/toast.dart';

class CreateNewIssueGroupProvider extends ChangeNotifier {
  static final CreateNewIssueGroupProvider _singleton =
      CreateNewIssueGroupProvider._internal();
  factory CreateNewIssueGroupProvider() => _singleton;
  CreateNewIssueGroupProvider._internal();

  DBHelper _dbHelper = DBHelper.instance;
  UiHelper uiHelper = UiHelper();

  TabController tabController;
  String status;
  TextEditingController nameController = TextEditingController();
  String groupName = '';
  String groupId;
  String copyOf;
  String address1 = '';
  String address2 = '';
  //New Field Introduced START
  String patientName = '';
  String patientAge = '';
  String symptoms = '';
  String sinceHowManyDays = '';
  String spo2Level = '';
  String isOnOxygenCylinder = '';
  String covidResult = '';
  String hospitalPreference = '';
  String attenderName = '';
  String attenderMobileno = '';
  String relationToPatient = '';
  String srfId = '';
  String buNumber = '';
  String bloodGroup = '';
  String typeOfBedRequied = '';
  String otherServicePatientID = '';
  String otherServicePatientName = '';
  String otherServiceLocation = '';
  String otherServicePhoneNo = '';
  String otherServicePatientAddress = '';
  String otherServiceStatus = '';
  String otherServiceCoordinatorInCharge = '';
  String otherServiceTypeOfService = '';
  DateTime otherServiceDateTimeOfService;
  String otherServiceDatesIfAny = '';
  String otherServiceVendor = "";
  String otherServiceConsultingDoctor = "";
  String otherServiceEquipmentID = "Select";
  String otherServiceEquipmentType = "";
  String otherServiceNamesOfTest = "";
  String otherServiceNoOfTestConducted = '';
  String otherServiceAreaSquareFeet = "";
  String otherServiceNoOfEquipmentsIssued = "";
  String otherServicePaidOrFree = "Select";
  String otherServiceRate = "";
  String otherServiceTotalCost = "";
  String otherServiceCollectedBy = ""; 
  String otherServiceAmountCollected = "";
  String otherServiceCombinedPaymentDetails = "";
  String otherServiceCollectionMode = "";
  String otherServiceTransactionID = '';
  DateTime otherServicePaymentDate;
  String otherServicePaymentStatus = "";
  String otherServiceSurplus = "";
  String otherServiceShortage = "";
  String otherServiceRemarksOrComments = "";
  String bedAllocationPatientLocation = "";
  String bedAllocationRemarks = "";
  //New Field Introduced END
  bool highPriority = false;

  DateTime complaintDate,
      preferredVisitDataAndTimeFrom,
      preferredVisitDataAndTimeTo;
  String complainant, phone, pinCode, issueNo;
  String offlineId;

  void setGroupName(name) {
    groupName = name;
    notifyListeners();
  }

  ///================================ group members operation ====================
  List selectedGroupMembers = [];
  List tmpSelectedGroupMembers = [];
  List allGroupMembers = [];
  List hiddenGroupMembers = [];
  List searchResultGroupMembers = [];
  bool selectAllUserCheck = false;
  bool groupMembersLoading = false;

  ///
  Future<void> setAllGroupMembers(List groupMembersList,
      AppProvider appProvider, UserProvider userProvider) async {
    allGroupMembers = [];
    await Future.wait(List.generate(
      groupMembersList.length,
      (index) async {
        groupMembersLoading = true;
        List tag = await _dbHelper.getUserTagFromUserId(
            groupMembersList[index]['userId'],
            appProvider.selectedOrganization['_id']);
        allGroupMembers.add(
          {
            'user_id': '${groupMembersList[index]['userId']}',
            'user_name': '${groupMembersList[index]['userName']}',
            'user_fullname': '${groupMembersList[index]['userFullName']}',
            'organization_id': appProvider.selectedOrganization['_id'],
            'organization_name': appProvider.getOrgName(userProvider.data),
            'added_by_id': userProvider.userId,
            'added_by_name': userProvider.userNameFullName,
            'user_type': 'MEMBER',
            'members_tag': tag,
//                await _dbHelper.getUserTagFromUserId(groupMembersList[index]['userId']),
          },
        );
      },
    ));

    searchResultGroupMembers = allGroupMembers;
    groupMembersLoading = false;
    resetAllGroupFilterTags();
    notifyListeners();
  }

  setSearchResultGroupMembers() {
    searchResultGroupMembers = allGroupMembers;
    resetAllGroupFilterTags();
    notifyListeners();
  }

  void getSearchResultGroupMembers(String val) {
    searchResultGroupMembers = allGroupMembers
        .where((element) =>
            _isMatchingWithMemberName(element, val) ||
            _isMatchingWithMemberTag(element, val))
        .toList();

    resetAllGroupFilterTags();
    notifyListeners();
  }

  void getSearchResultTags(String val) {
    searchResultTags = allGroupTags
        .where((tag) =>
            tag['tagName'].toLowerCase().startsWith(val.toLowerCase()) ||
            tag['tagName'].toLowerCase().contains(" " + val.toLowerCase()))
        .toList();

    resetAllGroupFilterTags();
    notifyListeners();
  }

  dynamic getMemberWithId(String userId) {
    return allGroupMembers.firstWhere((element) => element["user_id"] == userId,
        orElse: () => null);
  }

  bool _isMatchingWithMemberName(dynamic member, String pattern) {
    String memberName = member['user_fullname'].toString().toLowerCase();

    return memberName.startsWith(pattern.toLowerCase()) ||
        memberName.contains(" " + pattern.toLowerCase());
  }

  bool _isMatchingWithMemberTag(dynamic member, String pattern) {
    return member['members_tag'].any((tag) => tag["tagName"]
        .toString()
        .toLowerCase()
        .startsWith(pattern.toLowerCase()));
  }

  List get filteredSearchResultGroupMembers {
    return searchResultGroupMembers
        .where((element) =>
            groupFilterTags.isEmpty ||
            element['members_tag'].length == 0 ||
            element['members_tag'].any((tag) => selectedGroupFilterTags
                .any((filterTag) => areTagsSame(filterTag, tag))))
        .toList();
  }

  bool isEmptyMembers() {
    return selectedGroupMembers.isEmpty && hiddenGroupMembers.isEmpty;
  }

  List allMembers() {
    return selectedGroupMembers + hiddenGroupMembers;
  }

  String saveSelectedMembers(userProvider, appProvider) {
    var removed = selectedGroupMembers
        .toSet()
        .difference(tmpSelectedGroupMembers.toSet())
        .toList();

    if (removed.isNotEmpty) {
      for (var member in removed) {
        if (appProvider
            .getDefaultUsers(userProvider.data)
            .any((dMember) => dMember['_id'] == member['user_id'])) {
          if (!appProvider.isTheCurrentUserPrivilegedFor(
              UserPermissionMapKeys.canRemoveDefaultUser, userProvider.data)) {
            return 'You do not have the privilege remove default participants';
          }
        }
      }
    }

    selectedGroupMembers.clear();
    selectedGroupMembers = [...tmpSelectedGroupMembers];
    tmpSelectedGroupMembers.clear();

    return null;
  }

  void setSelectedMembers(Map member, AppProvider appProvider,
      UserProvider userProvider, BuildContext context) {
    if (tmpSelectedGroupMembers.contains(member)) {
      if (appProvider.isTheCurrentUserPrivilegedFor(
              UserPermissionMapKeys.canRemoveUsersInGroup, userProvider.data) ||
          !selectedGroupMembers.contains(member)) {
        tmpSelectedGroupMembers.remove(member);
        selectAllUserCheck = false;
      } else {
        Toast.show(
          'You do not have the privilege remove participants',
          context,
          backgroundColor: Colors.red,
        );
      }
    } else {
      if (appProvider.isTheCurrentUserPrivilegedFor(
              UserPermissionMapKeys.canAddUsersInGroup, userProvider.data) ||
          selectedGroupMembers.contains(member)) {
        tmpSelectedGroupMembers.add(member);
        if (tmpSelectedGroupMembers.length == allGroupMembers.length) {
          selectAllUserCheck = true;
        }
      } else {
        Toast.show(
          'You do not have the privilege add participants',
          context,
          backgroundColor: Colors.red,
        );
      }
    }

    notifyListeners();
  }

  void initTempSelectedMembers() {
    tmpSelectedGroupMembers = [...selectedGroupMembers];
  }

  void initSelectedMembers(Map member) {
    if (selectedGroupMembers.contains(member)) {
      selectedGroupMembers.remove(member);
      selectAllUserCheck = false;
    } else {
      selectedGroupMembers.add(member);
      if (selectedGroupMembers.length == allGroupMembers.length) {
        selectAllUserCheck = true;
      }
    }

    notifyListeners();
  }

  void setHiddenMembers(Map member) {
    if (hiddenGroupMembers.contains(member)) {
      hiddenGroupMembers.remove(member);
    } else {
      hiddenGroupMembers.add(member);
    }

    notifyListeners();
  }

  void selectAllGroupMembers(
      AppProvider appProvider, UserProvider userProvider) {
    for (int i = 0; i < filteredSearchResultGroupMembers.length; i++) {
      var member = filteredSearchResultGroupMembers[i];

      if (appProvider.isTheCurrentUserPrivilegedFor(
              UserPermissionMapKeys.canAddUsersInGroup, userProvider.data) ||
          selectedGroupMembers.contains(member)) {
        if (!tmpSelectedGroupMembers.contains(member)) {
          tmpSelectedGroupMembers.add(member);
        }
      }
    }

    if (tmpSelectedGroupMembers.length == allGroupMembers.length) {
      selectAllUserCheck = true;
    }
    notifyListeners();
  }

  void deSelectAllGroupMembers(
      AppProvider appProvider, UserProvider userProvider) {
    selectAllUserCheck = false;
    for (int i = 0; i < filteredSearchResultGroupMembers.length; i++) {
      var member = filteredSearchResultGroupMembers[i];
      if (appProvider.isTheCurrentUserPrivilegedFor(
              UserPermissionMapKeys.canRemoveUsersInGroup, userProvider.data) ||
          !selectedGroupMembers.contains(member)) {
        if (tmpSelectedGroupMembers.contains(member)) {
          tmpSelectedGroupMembers.remove(member);
        }
      }
    }

    notifyListeners();
  }

  ///================================ group tag operation ====================
  List allGroupTags = [];
  List searchResultTags = [];
  List selectedGroupTags = [];
  List groupFilterTags = [];
  List selectedGroupFilterTags = [];

  ///
  void setAllGroupTags(List groupTags) {
    allGroupTags = groupTags;
    searchResultTags = groupTags;
    notifyListeners();
  }

  void setSelectedGroupTags(Map tag) {
    for (int i = 0; i < selectedGroupTags.length; i++) {
      if (selectedGroupTags[i]['tagId'].toString() == tag['tagId'].toString()) {
        selectedGroupTags.removeAt(i);
        notifyListeners();
        return;
      }
    }
    selectedGroupTags.add(tag);
    notifyListeners();
  }

  void resetAllGroupFilterTags() {
    setAllGroupFilterTags(allGroupTags);
  }

  void resetSearchResults() {
    searchResultGroupMembers = allGroupMembers;
  }

  void setAllGroupFilterTags(List groupTags) {
    //groupFilterTags contains those tags which are accociated with at least
    //one search result (member)
    groupFilterTags
      ..clear()
      ..addAll(groupTags.where((groupTag) => searchResultGroupMembers.any(
          (member) =>
              member['members_tag'].any((tag) => areTagsSame(tag, groupTag)))));

    //selectedGroupFilterTags reflects the current selection of the tags
    //on the UI. Initially all the tags will be selected.
    selectedGroupFilterTags
      ..clear()
      ..addAll(groupFilterTags);

    notifyListeners();
  }

  void setSelectedGroupFilterTags(Map tag) {
    for (int i = 0; i < selectedGroupFilterTags.length; i++) {
      if (areTagsSame(selectedGroupFilterTags[i], tag)) {
        selectedGroupFilterTags.removeAt(i);

        notifyListeners();
        return;
      }
    }

    selectedGroupFilterTags.add(tag);
    notifyListeners();
  }

  ///================================ clear all selected data ====================
  void clearAllData(AppProvider appProvider) {
    selectedGroupMembers = [];
    tmpSelectedGroupMembers = [];
    allGroupMembers = [];
    hiddenGroupMembers = [];
    searchResultGroupMembers = [];
    selectAllUserCheck = false;
    nameController?.clear();
    status = appProvider.selectedOrganizationTab[0]['name'];
    tabController?.index = 0;

    address1 = '';
    address2 = '';
    groupId = null;
    copyOf = null;
    issueNo = null;
    complainant = '';
    phone = '';

    highPriority = false;

    complaintDate = null;
//New Field Introduced START
    patientName = '';
    patientAge = '';
    symptoms = '';
    sinceHowManyDays = '';
    spo2Level = '';
    isOnOxygenCylinder = '';
    covidResult = '';
    hospitalPreference = '';
    attenderName = '';
    attenderMobileno = '';
    relationToPatient = '';
    srfId = '';
    buNumber = '';
    bloodGroup = '';
    typeOfBedRequied = '';
    otherServicePatientID = "";
    otherServicePatientName = "";
    otherServiceLocation = "";
    otherServicePhoneNo = "";
    otherServicePatientAddress = "";
    otherServiceStatus = '';
    otherServiceCoordinatorInCharge = '';
    otherServiceTypeOfService = '';
    otherServiceDateTimeOfService = null;
    otherServiceDatesIfAny = '';
    otherServiceVendor = "";
    otherServiceConsultingDoctor = "";
    otherServiceEquipmentID = "";
    otherServiceEquipmentType = "";
    otherServiceNamesOfTest = "";
    otherServiceNoOfTestConducted = '';
    otherServiceAreaSquareFeet = "";
    otherServiceNoOfEquipmentsIssued = "";
    otherServicePaidOrFree = "";
    otherServiceRate = "";
    otherServiceTotalCost = "";
    otherServiceCollectedBy = "";
    otherServiceAmountCollected = "";
    otherServiceCombinedPaymentDetails = "";
    otherServiceCollectionMode = "";
    otherServiceTransactionID = '';
    otherServicePaymentDate = null;
    otherServicePaymentStatus = "";
    otherServiceSurplus = "";
    otherServiceShortage = "";
    otherServiceRemarksOrComments = "";
    bedAllocationPatientLocation = "";
    bedAllocationRemarks = "";
//New Field Introduced END
    preferredVisitDataAndTimeFrom = null;
    preferredVisitDataAndTimeTo = null;
    pinCode = '';

    allGroupTags = [];
    searchResultTags = [];
    selectedGroupTags = [];

    groupFilterTags.clear();
    selectedGroupFilterTags.clear();
    offlineId = null;
    PrintString('cleared');
  }

  ///================================ clear all selected data ====================
  void initEditData(
      dynamic groupDetails, AppProvider appProvider, UserProvider userProvider,
      {copying = false}) async {
    String id = groupDetails['groupId'];

    List data = await _dbHelper.getGroupData(groupId);
    if (data.isNotEmpty) {
      groupDetails = data[0];
    }

    groupId = id;

    nameController.clear();
    nameController.text = groupDetails['group_name'];

    issueNo = groupDetails['issue_no'];

    var statusTab = appProvider.selectedOrganizationTab.firstWhere(
        (element) =>
            element['name'].toString().toLowerCase() ==
            groupDetails['group_status'].toString().toLowerCase(),
        orElse: () => null);

    status = statusTab != null ? statusTab['name'] : null;

    address1 = groupDetails['address_1'];

    complainant = groupDetails['complainant'];
    pinCode = groupDetails['pincode'];
    complaintDate = groupDetails['complaint_date'] != null
        ? DateTime.fromMillisecondsSinceEpoch(
            groupDetails['complaint_date'] * 1000)
        : null;

    if (copying) {
      groupId = null;
      copyOf = id;
      issueNo = null;
      status = appProvider.selectedOrganizationTab[0]['name'];
      nameController.text = "Copy of " + groupDetails['group_name'];
    } else {
      offlineId = groupDetails['offline_id'];
    }

    selectedGroupMembers = [];
    tmpSelectedGroupMembers = [];
    allGroupMembers = [];
    hiddenGroupMembers = [];
    searchResultGroupMembers = [];
    selectAllUserCheck = false;
    address2 = groupDetails['address_2'];

//Introducing New Fields START
    patientName = groupDetails['patient_name'];
    patientAge = groupDetails['patient_age'];
    symptoms = groupDetails['symptoms'];
    sinceHowManyDays = groupDetails['since_how_many_days'];
    spo2Level = groupDetails['spo2_level'];
    isOnOxygenCylinder = groupDetails['is_on_oxygen_cylinder'];
    covidResult = groupDetails['covid_result'];
    hospitalPreference = groupDetails['hospital_preference'];
    attenderName = groupDetails['attender_name'];
    attenderMobileno = groupDetails['attender_mobileno'];
    relationToPatient = groupDetails['relation_to_patient'];
    srfId = groupDetails['srf_id'];
    buNumber = groupDetails['bu_number'];
    bloodGroup = groupDetails['blood_group'];
    typeOfBedRequied = groupDetails['type_of_bed_required'];
    otherServicePatientID = groupDetails['other_service_patient_id'];
    otherServicePatientName = groupDetails['other_service_patient_name'];
    otherServiceLocation = groupDetails['other_service_location'];
    otherServicePhoneNo = groupDetails['other_service_phoneno'];
    otherServicePatientAddress = groupDetails['other_service_patient_address'];
    otherServiceStatus = groupDetails['other_service_status'];
    otherServiceCoordinatorInCharge = groupDetails['other_service_coordinator_in_charge'];
    otherServiceTypeOfService = groupDetails['other_service_type_of_service'];
    otherServiceDateTimeOfService = groupDetails['other_service_date_time_of_service'] != null
            ? DateTime.fromMillisecondsSinceEpoch(
                groupDetails['other_service_date_time_of_service'] * 1000)
            : null;
    otherServiceDatesIfAny = groupDetails['other_service_dates_if_any'];
    otherServiceVendor = groupDetails['other_service_vendor'];
    otherServiceConsultingDoctor = groupDetails['other_service_consulting_doctor'];
    otherServiceEquipmentID = groupDetails['other_service_equipment_id'];
    otherServiceEquipmentType = groupDetails['other_service_equipment_type'];
    otherServiceNamesOfTest = groupDetails['other_service_names_of_test'];
    otherServiceNoOfTestConducted = groupDetails['other_service_no_of_test_conducted'];
    otherServiceAreaSquareFeet = groupDetails['other_service_area_square_feet'];
    otherServiceNoOfEquipmentsIssued = groupDetails['other_service_no_of_equipment_issued'];
    otherServicePaidOrFree = groupDetails['other_service_paid_or_free'];
    otherServiceRate = groupDetails['other_service_rate'];
    otherServiceTotalCost = groupDetails['other_service_total_cost'];
    otherServiceCollectedBy = groupDetails['other_service_collected_by'];
    otherServiceAmountCollected = groupDetails['other_service_amount_collected'];
    otherServiceCombinedPaymentDetails = groupDetails['other_service_combined_payment_details'];
    otherServiceCollectionMode = groupDetails['other_service_collection_mode'];
    otherServiceTransactionID = groupDetails['other_service_transaction'];
    otherServicePaymentDate = groupDetails['other_service_payment_date'] != null
            ? DateTime.fromMillisecondsSinceEpoch(
                groupDetails['other_service_payment_date'] * 1000)
            : null;
    otherServicePaymentStatus = groupDetails['other_service_payment_status'];
    otherServiceSurplus = groupDetails['other_service_surplus'];
    otherServiceShortage = groupDetails['other_service_shortage'];
    otherServiceRemarksOrComments = groupDetails['other_service_remarks_or_comments'];
    bedAllocationPatientLocation = groupDetails['bed_allocation_patient_location'];
    bedAllocationRemarks = groupDetails['bed_allocation_remarks'];
//Introducing New Fields END
    phone = groupDetails['phone_no'];

    highPriority = groupDetails['high_priority'] == 1;
    preferredVisitDataAndTimeFrom =
        groupDetails['preferred_visit_date_and_time_from'] != null
            ? DateTime.fromMillisecondsSinceEpoch(
                groupDetails['preferred_visit_date_and_time_from'] * 1000)
            : null;
    preferredVisitDataAndTimeTo =
        groupDetails['preferred_visit_date_and_time_to'] != null
            ? DateTime.fromMillisecondsSinceEpoch(
                groupDetails['preferred_visit_date_and_time_to'] * 1000)
            : null;

    allGroupTags = [];
    searchResultTags = [];
    selectedGroupTags = [];

    groupFilterTags.clear();
    selectedGroupFilterTags.clear();

    List groupTagsSaved = await _dbHelper.getGroupTagByGroupIdData(id);

    for (var tag in groupTagsSaved) {
      selectedGroupTags.add({
        "groupId": tag["group_id"],
        "tagCategory": tag["tag_category"],
        "tagName": tag["tagName"],
        "tagId": tag["tag_id"],
      });
    }

    tabController?.index = 0;
  }

  void init(AppProvider appProvider, UserProvider userProvider) {
    if (appProvider.config['meta_data_prefill'] == "true") {
      address1 = userProvider.address ?? '';
      complainant = userProvider.userNameFullName;
      pinCode = userProvider.pincode ?? '';
      phone = userProvider.userPhone;
      complaintDate = DateTime.now();
    }
  }

  static bool areTagsSame(dynamic tag1, dynamic tag2) {
    return tag1['tagName'].toString().toLowerCase() ==
        tag2['tagName'].toString().toLowerCase();
  }
}
