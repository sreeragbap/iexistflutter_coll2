import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:iexist/src/utilities/sharedPreferenceKeys.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:iexist/src/database/dbHelper.dart';

class AppProvider extends ChangeNotifier {
  static final AppProvider _singleton = AppProvider._internal();
  factory AppProvider() => _singleton;
  AppProvider._internal();

  ///firebase messaging token
  String fcmDeviceId;

  ///============================= connection state =========================
  bool isOnline = true;
  bool apiError = false;
  var visibility = AppLifecycleState.resumed;
  SharedPreferences sharedPreferences;
  DBHelper dbInstance = DBHelper.instance;

  void setConnectionStatus(bool isOnline) {
    this.isOnline = isOnline;
    notifyListeners();
  }

  void setApiErrorStatus(bool isError) {
    this.apiError = isError;
    notifyListeners();
  }

  void setAppVisibility(state) {
    this.visibility = state;
    notifyListeners();
  }

  bool isAppinForeground() {
    return this.visibility == AppLifecycleState.resumed;
  }

  ///==================== Organization and profile ===========================

  List allOrganizations = [];
  Map selectedOrganization = {};
  List selectedOrganizationTab = [];
  Map config = {};

  Future<void> setOrgs({data}) async {
    sharedPreferences = await SharedPreferences.getInstance();

    if (data != null) {
      this.allOrganizations = data;
      await saveOrgData();
    } else {
      var cache = sharedPreferences.getString(organizationsSaveData);
      this.allOrganizations = cache != null ? json.decode(cache) : [];
    }

    await setSelectedOrganization(sharedPreferences.getString(
      selectedOrganizationSaveData,
    ));

    notifyListeners();
  }

  Future<void> saveOrgData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.setString(
        organizationsSaveData, json.encode(allOrganizations));
  }

  Future<void> setSelectedOrganization(organizationId) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.setString(
      selectedOrganizationSaveData,
      organizationId,
    );

    if (null != organizationId) {
      selectedOrganization = allOrganizations.firstWhere(
          (element) => element['_id'] == organizationId,
          orElse: () => null);

      if (null == selectedOrganization &&
          allOrganizations != null &&
          allOrganizations.isNotEmpty) {
        selectedOrganization = allOrganizations[0];
      }

      if (null != selectedOrganization) {
        selectedOrganizationTab = selectedOrganization['group_statuses'];
        config = selectedOrganization["config"] ?? {};
      }
    }

    if (null == selectedOrganization) {
      selectedOrganization = null;
      selectedOrganizationTab = [];
      config = {};
    }

    notifyListeners();
  }

  List getDefaultUsers(userData) {
    var org = getOrg(userData);
    return org['default_users'];
  }

  getOrg(userData) {
    var org = userData['organizations']
        .firstWhere((element) => element['_id'] == selectedOrganization['_id']);
    return org;
  }

  getOrgName(userData) {
    var org = getOrg(userData);
    return org['organization_name'];
  }

  List getPermissions(userData) {
    var org = getOrg(userData);

    List permissions = [];

    for (Map permission in org['permissions']) {
      permissions.add(permission['short_code']);
    }

    return permissions;
  }

  ///======================== issue page ======================================

  List issueGroupList = [];
  List filteredGroupList = [];

  //
  Future<void> updateIssueGroupList() async {
    List groupData = await DBHelper.instance
        .getGroupDataFromOrganization(selectedOrganization['_id']);
    issueGroupList = groupData;
    filteredGroupList = issueGroupList;
    notifyListeners();
  }

  String getLastGroupId() {
    return issueGroupList.isEmpty ? '' : issueGroupList.last['groupId'];
  }

  void setFilteredGroupList(List groups) {
    filteredGroupList = groups;
    notifyListeners();
  }

  bool _isMatchingWithGroupName(dynamic group, String pattern) {
    String groupName = group['group_name'].toString().toLowerCase();
    return groupName.startsWith(pattern.toLowerCase()) ||
        groupName.contains(" " + pattern.toLowerCase());
  }

  bool _isMatchingWithIssueNumber(dynamic group, String pattern) {
    String issueNo = group['issue_no'].toString().toLowerCase();
    return issueNo.startsWith(pattern.toLowerCase()) ||
        issueNo.contains(" " + pattern.toLowerCase()) ||
        issueNo.contains("-" + pattern.toLowerCase());
  }

  Future _isMatchingWithGroupTag(
      dynamic group, DBHelper dbHelper, String pattern) async {
    List tags = await dbHelper.getGroupTagByGroupIdData(group['groupId']);
    return tags.any((tag) {
      String tagName = tag["tagName"].toString().toLowerCase();
      return tagName.startsWith(pattern.toLowerCase()) ||
          tagName.contains(" " + pattern.toLowerCase());
    });
  }

  Future<void> getFilteredGroups(String val, DBHelper dbHelper) async {
    filteredGroupList = [];
    for (dynamic issue in issueGroupList) {
      bool anyTags = await _isMatchingWithGroupTag(issue, dbHelper, val);

      if (anyTags ||
          _isMatchingWithIssueNumber(issue, val) ||
          _isMatchingWithGroupName(issue, val)) {
        filteredGroupList.add(issue);
      }
    }

    notifyListeners();
  }

  bool isTheCurrentUserPrivilegedFor(String privilege, userData) {
    return getPermissions(userData).contains(privilege);
  }
}
