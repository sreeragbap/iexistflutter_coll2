import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:synchronized/synchronized.dart';

enum InitializationStatus { NotInitialized, Initializing, Initialized }

class FCMHandler {
  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  static final Lock _lock = Lock();
  static InitializationStatus _initializationStatus =
      InitializationStatus.NotInitialized;

  static MessageHandler onMessage;
  static MessageHandler onLaunch;
  static MessageHandler onResume;
  static MessageHandler onBackground;

  static void init(AppProvider appProvider) async {
    if (_initializationStatus != InitializationStatus.NotInitialized) return;

    await _lock.synchronized(() async {
      if (_initializationStatus != InitializationStatus.NotInitialized) return;

      _initializationStatus = InitializationStatus.Initializing;
      _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) async {
            print("onMessage: $message");
            if (onMessage != null) {
              onMessage(message);
            }
          },
          onBackgroundMessage: myBackgroundMessageHandler,
          onLaunch: (Map<String, dynamic> message) async {
            print("onLaunch: $message");
            if (onLaunch != null) {
              onLaunch(message);
            }
          },
          onResume: (Map<String, dynamic> message) async {
            print("onResume: $message");
            if (onResume != null) {
              onResume(message);
            }
          });
      await _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(
              sound: true, badge: true, alert: true, provisional: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });
      String token = await _firebaseMessaging.getToken();

      PrintString("firebasetoken : " + token);
      assert(token != null);

      appProvider.fcmDeviceId = token;

      _initializationStatus = InitializationStatus.Initialized;
    });
  }

  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    print("myBackgroundMessageHandler: $message");
    if (onBackground != null) {
      onBackground(message);
    }
  }
}
