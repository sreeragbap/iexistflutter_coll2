import 'package:iexist/src/database/dbHelper.dart';

class PrintString {
  final data;
  final level;
  static const String LogLevelInfo = 'info';
  static const String LogLevelError = 'error';
  static const String LogLevelFatal = 'fatal';
  static const String LogLevelAPI = 'api';

  PrintString(this.data, {this.level = LogLevelInfo}) {
    printString(data);
  }
  void printString(data) {
    print(data);
    DBHelper.instance.addLogMessage('$data', level);
  }
}
