import 'dart:io';

import 'package:dio/dio.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/api.dart';
import 'package:iexist/src/utilities/extensions.dart';
import 'package:iexist/src/utilities/newMessageNotificationManager.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:ext_storage/ext_storage.dart';

import 'package:ntp/ntp.dart';
import 'package:sqflite/sqflite.dart';
import 'package:video_compress/video_compress.dart';

Future<void> saveUserData(
    UserProvider userProvider, AppProvider appProvider, Map userData) async {
  Map completeUserProfile = userData['profile'];

  completeUserProfile['token'] = userData['token'];

  Map selectedOrganization = completeUserProfile['organizations'][0];

  await userProvider.init(data: completeUserProfile);

  await appProvider.setSelectedOrganization(selectedOrganization['_id']);

  userProvider.setFirstSyncStatus(false);
}

Future<int> getCurrentSec({local = false}) async {
  DateTime now = DateTime.now();

  if (!local) {
    try {
      now = await NTP.now().timeout(const Duration(seconds: 3));
    } catch (e) {}
  }

  return now.millisecondsSinceEpoch ~/ 1000;
}

void markGroupAsRead(userProvider, chatScreenProvider, groupId) async {
  Dio dio = Dio();
  var data = {
    "user_id": userProvider.userId,
    "group_id": groupId,
    "timestamp": DateTime.now().millisecondsSinceEpoch ~/ 1000
  };

  dio.options.headers['content-Type'] = 'application/json';
  dio.options.headers["Authorization"] = userProvider.token;
  try {
    var resp = await dio.postExtended('$setMessageReadStatus', data: data);
    Map respData = resp.data;
    if (respData['code'] == 'SUC') {
      chatScreenProvider.markChatGroupRead(groupId);
    }
  } catch (e) {
    PrintString(e);
  }
}

Future download(remotePath, type) async {
  String path;

  try {
    var p = remotePath.split('/');
    String fileName = p[p.length - 1];
    var sec = await getCurrentSec();

    path = await getStorageLoc() + "/${sec}_$fileName";

    PermissionStatus permissionStatus = await Permission.storage.request();
    if (permissionStatus != PermissionStatus.granted) return null;

    await Dio().download("$remotePath", path);

    if (type == 'IMAGE' || type == 'VIDEO') {
      final result = await ImageGallerySaver.saveFile(path);
    } else {
      String downloadPath = await ExtStorage.getExternalStoragePublicDirectory(
          ExtStorage.DIRECTORY_DOWNLOADS);
      await File(path).copy(downloadPath + "/${sec}_$fileName");
    }
  } catch (e) {
    PrintString('download error');
  } finally {
    return path;
  }
}

Future<String> getStorageLoc() async {
  Directory dir;
  if (Platform.isIOS) {
    dir = await getApplicationDocumentsDirectory();
  } else {
    dir = await getExternalStorageDirectory();
  }
  return dir.path;
}

Future<String> getStoragePath() async {
  return await getStorageLoc() + "/${await getCurrentSec(local: true)}";
}

Future<String> setThumbnailWithFile(String id, String path) async {
  final _flutterVideoCompress = VideoCompress;
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  var key = id + "_thumbnail";

  if (sharedPreferences.containsKey(key)) {
    return sharedPreferences.getString(key);
  }

  try {
    var data = await _flutterVideoCompress.getFileThumbnail(path,
        quality: 50, position: -1);
    await sharedPreferences.setString(key, data.path);

    return data.path;
  } catch (e) {
    return null;
  }
}

Future<void> insertGroupMessages(List messages, groupId,
    {update: false}) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  for (var message in messages) {
    if (!update) {
      var localPath = sharedPreferences.containsKey('${message['_id']}')
          ? sharedPreferences.getString('${message['_id']}')
          : null;

      if (UserProvider().autoDownload && message['file'] != null) {
        localPath = await download(message['file'], message['type']);

        if (message['file'] == 'VIDEO') {
          await setThumbnailWithFile(message['_id'], localPath);
        }
      }

      await AppProvider().dbInstance.insertChatMessage(
            id: message['_id'],
            userId: message['user_id'],
            syncStatus: 'complete',
            userFullName: message['user_fullname'],
            userName: message['username'],
            text: message['text'],
            chatGroupId: groupId,
            file: message['file'],
            localPath: localPath,
            timeStamp: message['timestamp'],
            type: message['chat_type'],
            offlineId: message['offline_id'],
          );
    }
    await AppProvider().dbInstance.clearAMessagesStatus(message['_id']);

    for (var message_status in message['message_statuses']) {
      await AppProvider().dbInstance.addMessageStatus(
            id: message_status['_id'],
            message_id: message_status['message_id'],
            user_id: message_status['user_id'],
            user_fullname: message_status['user_fullname'],
            sent_time: message_status['sent_time'],
            read_time: message_status['read_time'],
            group_id: groupId,
          );
    }
  }
}

//======================== group data operations =======================
Future<void> insertGroups(groupData, timeStamp, AppProvider appProvider,
    {orgId, showNotification = true}) async {
  Database db = await AppProvider().dbInstance.db;
  List groups = await db.rawQuery(
      "SELECT * FROM groupTable WHERE groupId = '${groupData['_id']}' OR offline_id = '${groupData['offline_id']}'");

  if (groups.isNotEmpty) {
    await insertGroupMessages(groupData['messages'], groupData['_id']);
    return false;
  }

  List groupMembers = groupData['members'] ?? [];
  List messages = groupData['messages'] ?? [];
  List groupTag = groupData['group_tags'];

  String groupId = groupData['_id'];

  await AppProvider().dbInstance.insertGroup(
        syncStatus: 'complete',
        groupId: groupId,
        orgId: orgId ?? appProvider.selectedOrganization['_id'],
        name: groupData['group_name'],
        issueNo: groupData['issue_no'],
        phone: groupData['phone_no'],
        pinCode: groupData['pincode'],
        groupStatus: groupData['group_status'],
        preferredVisitDateAndTimeFrom:
            groupData['preferred_visit_date_and_time_from'],
        preferredVisitDateAndTimeFromTo:
            groupData['preferred_visit_date_and_time_to'],
        highPriority: groupData['high_priority'] == true ? 1 : 0,
        complaintDate: groupData['complaint_date'],
        //Introducing New Fields START
        patientName: groupData['patient_name'],
        patientAge: groupData['patient_age'],
        symptoms: groupData['symptoms'],
        sinceHowManyDays: groupData['since_how_many_days'],
        spo2Level: groupData['spo2_level'],
        isOnOxygenCylinder: groupData['is_on_oxygen_cylinder'],
        covidResult: groupData['covid_result'],
        hospitalPreference: groupData['hospital_preference'],
        attenderName: groupData['attender_name'],
        attenderMobileno: groupData['attender_mobileno'],
        relationToPatient: groupData['relation_to_patient'],
        srfId: groupData['srf_id'],
        buNumber: groupData['bu_number'],
        bloodGroup: groupData['blood_group'],
        typeOfBedRequied: groupData['type_of_bed_required'],
        otherServicePatientID: groupData['other_service_patient_id'],
        otherServicePatientName: groupData['other_service_patient_name'],
        otherServiceLocation: groupData['other_service_location'],
        otherServicePhoneNo: groupData['other_service_phoneno'],
        otherServicePatientAddress: groupData['other_service_patient_address'],
        otherServiceStatus : groupData['other_service_status'],
        otherServiceCoordinatorInCharge: groupData['other_service_coordinator_in_charge'],
        otherServiceTypeOfService: groupData['other_service_type_of_service'],
        otherServiceDateTimeOfService : groupData['other_service_date_time_of_service'],
        otherServiceDatesIfAny : groupData['other_service_dates_if_any'],
        otherServiceVendor: groupData['other_service_vendor'],
        otherServiceConsultingDoctor: groupData['other_service_consulting_doctor'],
        otherServiceEquipmentID: groupData['other_service_equipment_id'],
        otherServiceEquipmentType: groupData['other_service_equipment_type'],
        otherServiceNamesOfTest: groupData['other_service_names_of_test'],
        otherServiceNoOfTestConducted : groupData['other_service_no_of_test_conducted'],
        otherServiceAreaSquareFeet : groupData['other_service_area_square_feet'],
        otherServiceNoOfEquipmentsIssued : groupData['other_service_no_of_equipment_issued'],
        otherServicePaidOrFree : groupData['other_service_paid_or_free'],
        otherServiceRate : groupData['other_service_rate'],
        otherServiceTotalCost : groupData['other_service_total_cost'],
        otherServiceCollectedBy : groupData['other_service_collected_by'],
        otherServiceAmountCollected : groupData['other_service_amount_collected'],
        otherServiceCombinedPaymentDetails : groupData['other_service_combined_payment_details'],
        otherServiceCollectionMode : groupData['other_service_collection_mode'],
        otherServiceTransactionID : groupData['other_service_transaction'],
        otherServicePaymentDate : groupData['other_service_payment_date'],
        otherServicePaymentStatus : groupData['other_service_payment_status'],
        otherServiceSurplus : groupData['other_service_surplus'],
        otherServiceShortage : groupData['other_service_shortage'],
        otherServiceRemarksOrComments : groupData['other_service_remarks_or_comments'],
        bedAllocationPatientLocation : groupData['bed_allocation_patient_location'],
        bedAllocationRemarks : groupData['bed_allocation_remarks'],
        //Introducing New Fields END
        //Introducing New Fields END
        address1: groupData['address_1'],
        address2: groupData['address_2'],
        timestamp: timeStamp,
        complainant: groupData['complainant'],
        offlineId: groupData['offline_id'],
      );

  await insertGroupMessages(messages.reversed.toList(), groupId);
  await syncGroupMembers(groupMembers, groupId, timeStamp);
  await insertGroupTag(groupTag, groupId);

  if (showNotification) {
    NewMessageNotificationManager.showNewMessageNotification(
        '${groupData['issue_no'] != null ? groupData['issue_no'] : ''} ${groupData['group_name']}'
            .trim(),
        'New Issue Group Created',
        groupData['groupId'].toString() + "_Created",
        null);
  }
}

Future<void> syncGroupMembers(List groupMembers, groupId, timeStamp) async {
  for (int i = 0; i < groupMembers.length; i++) {
    String action = 'add';

    if (groupMembers[i]['action'] != null) {
      action = groupMembers[i]['action'].toString().toLowerCase();
    }

    if (action == 'add') {
      List member = await AppProvider()
          .dbInstance
          .getGroupMembersByIdData(groupId, groupMembers[i]['user_id']);
      if (member.isEmpty) {
        await AppProvider().dbInstance.insertGroupMembers(
              id: groupMembers[i]['_id'],
              userName: groupMembers[i]['user_name'],
              userFullName: groupMembers[i]['user_fullname'],
              userId: groupMembers[i]['user_id'],
              groupId: groupId,
              syncStatus: 'complete',
              userType: groupMembers[i]['user_type'],
              timeStamp: timeStamp,
            );
      }
    } else if (action == 'delete') {
      await AppProvider()
          .dbInstance
          .deleteGroupMemberByGroupIdData(groupId, groupMembers[i]['user_id']);
    }
  }
}

Future<void> insertGroupTag(List groupTag, groupId) async {
  await AppProvider().dbInstance.clearGroupTags(groupId);

  for (int i = 0; i < groupTag.length; i++) {
    await AppProvider().dbInstance.insertGroupTag(
          groupId: groupId,
          tagId: groupTag[i]['_id'],
          tagName: groupTag[i]['tag_name'],
          tagCategory: groupTag[i]['tag_category'],
        );
  }
}
