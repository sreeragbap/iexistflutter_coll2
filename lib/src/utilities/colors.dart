import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF036056);
const Color buttonColor = Color(0xFF63D46A);
const Color backgroundColor = Color(0xFFFFFFFF);
const Color mainTextColor = Color(0xFFFFFFFF);
const Color subTextColor = Color(0xFFE6E6E6);
const Color errorTextColor = Color(0xFFFF0000);
const Color navigationTextColor = Color(0xFF747474);
const Color selectionColor = Color(0xffd7e0dd);
const Color chatInfoColor = Color(0xffededed);

const List<Color> colorPalette = [
  Color(0xFF036056),
  Color(0xFF0C6B58),
  Color(0xFF14755A),
  Color(0xFF1D805B),
  Color(0xFF268A5D),
  Color(0xFF2F955F),
  Color(0xFF379F61),
  Color(0xFF40AA63),
  Color(0xFF49B465),
  Color(0xFF52BF66),
  Color(0xFF5AC968),
  Color(0xFF63D46A),
];
