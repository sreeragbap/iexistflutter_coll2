import 'package:dio/dio.dart';

import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';

import 'package:iexist/src/screens/profile/profile_screen_ui.dart';
import 'package:iexist/src/utilities/api.dart';

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

extension DioExtension on Dio {
  Future<Response<T>> postExtended<T>(
    String path, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      PrintString(
          '-----post request-----\n$path\n$data\n-----end post request-----',
          level: PrintString.LogLevelAPI);
      Response<T> response = await this.post(path,
          data: data,
          queryParameters: queryParameters,
          options: options,
          cancelToken: cancelToken,
          onSendProgress: onSendProgress,
          onReceiveProgress: onReceiveProgress);

      AppProvider().setApiErrorStatus(false);
      PrintString(
          '-----post reponse-----\n$path\n$response\n-----end post response-----',
          level: PrintString.LogLevelAPI);
      _logOutOnInvalidToken(response);
      return response;
    } catch (e) {
      AppProvider().setApiErrorStatus(true);
      rethrow;
    }
  }

  Future<Response<T>> getExtended<T>(
    String path, {
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      PrintString('-----get request-----\n$path\n-----end get request-----',
          level: PrintString.LogLevelAPI);
      Response<T> response = await this.get(path,
          queryParameters: queryParameters,
          options: options,
          cancelToken: cancelToken,
          onReceiveProgress: onReceiveProgress);

      AppProvider().setApiErrorStatus(false);
      PrintString(
          '-----get response-----\n$path\n$response\n-----end get response-----',
          level: PrintString.LogLevelAPI);
      _logOutOnInvalidToken(response);
      return response;
    } catch (e) {
      AppProvider().setApiErrorStatus(true);
      rethrow;
    }
  }

  void _logOutOnInvalidToken(Response response) async {
    if (null != response &&
        null != response.data &&
        null != response.data["code"]) {
      if (response.data["code"] == APIErrorCodes.INV_TOKEN) {
        await ProfileScreen.logOut();
      }
    }
  }
}
