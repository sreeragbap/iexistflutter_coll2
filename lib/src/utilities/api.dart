// String baseUrl = 'https://api.phacsin.xyz';
// String baseUrl = 'https://fasttrackmeghalaya.in';
// String baseUrl = 'http://14.141.133.153:8000';
// String baseUrl = 'https://fasttrackmeghalaya.in';
String baseUrl = 'http://157.245.100.194:8000';

///=============== login api =======================
String loginApi = '$baseUrl/v1/user/login';
String resetPasswordApi = '$baseUrl/v1/user/sendResetPasswordLink';
String generateOTP = '$baseUrl/v1/user/generateOTP';

///============== initial sync api =================
String initialSyncApi = "$baseUrl/v1/core/completeDataSync";

///============== profile api =====================
String profileDataApi = '$baseUrl/v1/user/getUsers';
String profilePicUpdateApi = '$baseUrl/v1/user/changeUserProfilePicture';
String createOrEditUser = '$baseUrl/v1/user/createOrEditUser';

///================= chat group api =================
String getAllChatGroupApi =
    '$baseUrl/v1/user/manageChatGroup/OrganizationGroups';
String getChatGroups = '$baseUrl/v1/user/getChatGroups';

///================= create chat group ==========================
String createChatGroupApi = '$baseUrl/v1/user/manageChatGroup';

///================= update chat group ==========================
String updateChatGroupApi = '$baseUrl/v1/user/manageChatGroup/:id';

///==================== send  message ============================
String sendTextMessageApi = '$baseUrl/v1/chat/manageChat';

// For Image, Video, Audio sending one by one
String sendFileApi = '$baseUrl/v1/chat/addMultimediaChat';

String setMessageReadStatus = '$baseUrl/v1/chat/setMessageReadStatus';

String getChatMessages = '$baseUrl/v1/chat/getChatMessages';

///========================= sync delta ==============================
String getLatestUpdateApi = '$baseUrl/v1/core/syncDeltas';

///========================= group Statistics ==============================
String groupStatisticsApi =
    '$baseUrl/v1/reports/groupStatistics/:user_id?from_date=:from_date&to_date=:to_date&organization_id=:organization_id';

class APIErrorCodes {
  static const String INV_TOKEN = "INV_TOKEN";
}
