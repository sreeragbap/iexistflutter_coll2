class NotificationActions {
  static const String user_add = "user_add"; //- Adding a User
  static const String user_edit = "user_edit"; // Editing a User
  static const String user_del = "user_del"; // Deleting a User
  static const String tag_add = "tag_add"; // Adding a Tag
  static const String tag_edit = "tag_edit"; // Editing a Tag
  static const String tag_del = "tag_del"; // Deleting a Tag
  static const String group_add = "group_add"; // Adding a Group
  static const String group_edit = "group_edit"; // Editing a Group
  static const String group_del = "group_del"; // Deleting a Group
  static const String org_add = "org_add"; // Adding an Organization
  static const String org_edit = "org_edit"; // Editing an Organization
  static const String org_del = "org_del"; // Deleting an Organization
  static const String msg_add = "msg_add"; // Adding a message to a Group
  static const String user_disable = "user_disable"; // Disabling a user
  static const String group_member_rm =
      "group_member_rm"; // Removing a user from group - "$id" contains group_id

  static const List<String> org_actions = [org_add, org_edit, org_del];
}
