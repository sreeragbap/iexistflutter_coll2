class UserPermissionMapKeys {
  static const removeDefaultUserFromGroup = "REM_DEF_USER_FRM_GRP";
  static const canAddChatGroup = "CAN_ADD_CHAT_GRP";
  static const canPostTextMessageInGroup = "CAN_POST_TXT_MSG";
  static const canPostPictureMessageInGroup = "CAN_POST_PIC_GRP";
  static const canPostVideoMessageInGroup = "CAN_POST_VID_GRP";
  static const canPostVoiceMessageInGroup = "CAN_POST_VOC_GRP";
  static const canPostDocumentMessageInGroup = "CAN_POST_DOC_GRP";
  static const canDeletePictureMessageInGroup = "CAN_DL_PIC_GRP";
  static const canDeleteVideoMessageInGroup = "CAN_DL_VID_GRP";
  static const canDeleteVoiceMessageInGroup = "CAN_DL_AUD_GRP";
  static const canDeleteDocumentMessageInGroup = "CAN_DL_DOC_GRP";
  static const canRemoveUsersInGroup = "CAN_RM_USER_GRP";
  static const canAddUsersInGroup = "CAN_ADD_USER_GRP";
  static const canEditChatGroup = "CAN_EDIT_USER_GRP";  
  static const canViewMessageReadStatusInGroup = "CAN_VIEW_USR_RD_USER_GRP";
  static const canDeleteChatGroup = "CAN_DL_GRP";
  static const canViewGraphInDashboard = "CAN_VIEW_GRPH_DASH";
  static const canRemoveDefaultUser = "REM_DEF_USER_FRM_GRP";
  static const canDeleteMessage = "CAN_DEL_MSG";
}