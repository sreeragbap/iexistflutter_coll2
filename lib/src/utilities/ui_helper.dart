import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


// Used for Date String operations
class UiHelper {
  String getDate(String data) {
    var tm = DateTime.parse(data);
    return "${tm.day}/${tm.month}/${tm.year}";
    return new DateFormat("yyyy-MM-dd hh:mm").format(tm);
  }

  String getDateTime(String data) {
    /*var tm = DateTime.parse(data).toLocal();
    return tm.day.toString() +
        '/' +
        tm.month.toString() +
        '/' +
        tm.year.toString() +
        ' ' +
        tm.hour.toString() +
        ':' +
        tm.minute.toString();*/
    var date = DateTime.parse(data);
    TimeOfDay time = TimeOfDay(hour:date.hour,minute: date.minute);

    return "${date.day}/${date.month}/${date.year} ${date.hour}:${date.minute}";
    return new DateFormat("yyyy-MM-dd hh:mm").format(date);
  }
}
