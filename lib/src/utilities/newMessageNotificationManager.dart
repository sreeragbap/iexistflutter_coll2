import 'dart:async';

import 'package:flutter/material.dart';
import 'package:iexist/src/database/dbHelper.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/chatScreenPage.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:synchronized/synchronized.dart';
import '../../main.dart';
import '../widgets/newMessageNotification.dart';

class NewMessageNotificationManager {
  static void showNewMessageNotification(
    String message,
    String title,
    String id,
    String type,
  ) async {
    Completer completer = Completer();
    completer.future.then((value) {
      _removeNewMessageNotification(value);
    });

    await _removeNewMessageNotification(id);

    NewMessageNotification newMessageNotification = NewMessageNotification(
      id,
      message,
      title,
      completer,
      onTap: () => _onTapNotification(id),
      leading: _buildNotificationLeading(type),
    );

    await _lock.synchronized(() {
      _overlayEntryMap[id] = OverlayEntry(builder: (BuildContext context) {
        return newMessageNotification;
      });

      _newMessageNotificationMap[id] = newMessageNotification;
    });

    navigatorKey.currentState.overlay.insert(_overlayEntryMap[id]);
  }

  static _removeNewMessageNotification(String notificationId) async {
    await _lock.synchronized(() {
      if (_overlayEntryMap.containsKey(notificationId)) {
        _overlayEntryMap[notificationId].remove();
        _overlayEntryMap.remove(notificationId);
        _newMessageNotificationMap.remove(notificationId);
      }
    });
  }

  static dismissAllNewMessageNotifications() async {
    await _lock.synchronized(() async {
      List<Future> dismissFutureList = [];

      _overlayEntryMap.forEach((key, value) {
        dismissFutureList.add(_newMessageNotificationMap[key].dismiss());
      });

      await Future.wait(dismissFutureList);

      _overlayEntryMap.forEach((key, value) {
        _overlayEntryMap[key].remove();
      });

      _overlayEntryMap.clear();
      _newMessageNotificationMap.clear();
    });
  }

  static Lock _lock = new Lock();
  static Map<String, OverlayEntry> _overlayEntryMap = <String, OverlayEntry>{};
  static Map<String, NewMessageNotification> _newMessageNotificationMap =
      <String, NewMessageNotification>{};

  static _onTapNotification(String id) async {
    List group =
        await DBHelper.instance.getGroupData(id.replaceAll("_Created", ""));
    if (group.length > 0) {
      navigatorKey.currentState.push(MaterialPageRoute(
          builder: (context) => ChatScreenPage(
                groupDetails: group[0],
              )));
    }
  }

  static _buildNotificationLeading(String type) {
    return CircleAvatar(
      backgroundColor: backgroundColor,
      child: Icon(
        _getNotificationLeadingIcon(type),
        size: 40,
        color: primaryColor,
      ),
    );
  }

  static IconData _getNotificationLeadingIcon(String type) {
    Map<String, IconData> notificationLeadingIcons = {
      "AUDIO": Icons.audiotrack,
      "IMAGE": Icons.image,
      "TEXT": Icons.chat,
      "DOCUMENT": Icons.attach_file,
      "NOTIFICATION": Icons.announcement,
    };

    String chatType = type?.toUpperCase();
    if (notificationLeadingIcons.containsKey(chatType))
      return notificationLeadingIcons[chatType];

    return Icons.sms;
  }
}
