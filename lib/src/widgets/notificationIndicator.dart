import 'package:flutter/material.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:provider/provider.dart';

class NotificationIndicator extends StatefulWidget {
  NotificationIndicator({Key key}) : super(key: key);

  void show() {
    _state.show();
  }

  final _NotificationIndicatorState _state = _NotificationIndicatorState();

  @override
  _NotificationIndicatorState createState() => _state;
}

class _NotificationIndicatorState extends State<NotificationIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Color> _valueColor;
  bool _show = false;

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    _valueColor = Tween<Color>(
            begin: Colors.grey.withOpacity(0.5),
            end: Colors.grey.withOpacity(0.5))
        .animate(CurvedAnimation(curve: Curves.ease, parent: _controller));

    super.initState();

    UserProvider().addListener(() {
      setState(() {});
    });

    AppProvider().addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _controller = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Material(
        color: Colors.transparent,
        child: Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Row(children: [
              Visibility(
                visible: _show,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    radius: 5.0,
                    backgroundColor: buttonColor,
                  ),
                ),
              ),
              Expanded(
                child: Visibility(
                  visible:
                      Provider.of<UserProvider>(context).showLoading,
                  child: LinearProgressIndicator(
                    backgroundColor: primaryColor,
                    valueColor: _valueColor,
                  ),
                ),
              ),
              Visibility(
                visible: Provider.of<AppProvider>(context).apiError,
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: CircleAvatar(
                    radius: 5.0,
                    backgroundColor: errorTextColor,
                  ),
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }

  void show() {
    setState(() {
      _show = true;
    });

    Future.delayed(new Duration(seconds: 2), () {
      setState(() {
        _show = false;
      });
    });
  }
}
