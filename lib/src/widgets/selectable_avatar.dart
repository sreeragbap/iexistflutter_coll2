import 'package:flutter/material.dart';

class SelectableAvatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Stack(fit: StackFit.loose, children: <Widget>[
          new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      image: new ExactAssetImage(
                          'assets/images/user.jpg'),
                      fit: BoxFit.cover,
                    ),
                  )),
            ],
          ),
          Padding(
              padding: EdgeInsets.only(top: 40.0, left: 45.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new CircleAvatar(
                    backgroundColor: Colors.black54,
                    radius: 10.0,
                    child: new Icon(
                      Icons.close,
                      color: Colors.white,
                      size: 13.0,
                    ),
                  )
                ],
              )),
        ]),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.help_outline,color: Colors.black54,size: 20,),
            SizedBox(
              width: 20,
                height: 20,
                child: Checkbox(value: true, onChanged: (value) => {})),
            Text(
              "Vijay",
              style: TextStyle(color: Colors.black87),
            ),
          ],
        ),
      ],
    );
  }
}
