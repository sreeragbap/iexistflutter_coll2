import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          backgroundImage: AssetImage("assets/images/user.jpg"),
          radius: 30,
          backgroundColor: Colors.black12,
        ),
        SizedBox(height: 5,),
        Text("Vijay",style: TextStyle(color: Colors.black54),),
      ],
    );
  }
}
