import 'package:flutter/material.dart';

typedef void TagChipTap(int index);
typedef bool IsTagChipSelected(int index);
typedef String GetTagChipLabel(int index);

class TagChipsWrap extends StatelessWidget {
  const TagChipsWrap({
    Key key,
    @required this.tagList,
    @required this.tagChipTap,
    @required this.isTagChipSelected,
    @required this.getTagChipLabel,
  }) : super(key: key);

  final List tagList;
  final TagChipTap tagChipTap;
  final IsTagChipSelected isTagChipSelected;
  final GetTagChipLabel getTagChipLabel;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 4,
      runSpacing: 6,
      children: List.generate(tagList.length, (index) {
        return InkWell(
          onTap: () {
            tagChipTap(index);
          },
          child: Chip(
            backgroundColor:
                isTagChipSelected(index) ? Colors.green : Colors.grey.shade200,
            label: Text(
              getTagChipLabel(index),
              style: TextStyle(
                color: isTagChipSelected(index) ? Colors.white : Colors.black,
              ),
            ),
          ),
        );
      }),
    );
  }
}
