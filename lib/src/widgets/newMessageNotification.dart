import 'dart:async';

import 'package:flutter/material.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/utilities/colors.dart';
import 'package:iexist/src/utilities/newMessageNotificationManager.dart';
import 'package:provider/provider.dart';

class NewMessageNotification extends StatefulWidget {
  final String id;
  final String message;
  final String title;
  final Completer completer;
  final VoidCallback onTap;
  final Widget logo;
  final Widget leading;

  NewMessageNotification(this.id, this.message, this.title, this.completer,
      {this.onTap, this.logo, this.leading});

  final _NewMessageNotificationState _state = _NewMessageNotificationState();

  @override
  State<StatefulWidget> createState() => _state;

  Future<void> dismiss() async {
    await _state.dismiss(invokeCompleter: false);
  }
}

class _NewMessageNotificationState extends State<NewMessageNotification>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _position;
  UserProvider _userProvider;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    _position = Tween<Offset>(begin: Offset(0.0, -4.0), end: Offset.zero)
        .animate(CurvedAnimation(parent: _controller, curve: Curves.easeOut));

    _controller.forward();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  Future<void> dismiss({bool invokeCompleter = true}) async {
    await _controller.reverse();
    if (invokeCompleter == true) {
      widget.completer.complete(Future.value(widget.id));
    }
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);

    double systemUiPadding = MediaQuery.of(context).padding.top;
    return Visibility(
      visible: !_userProvider.disableNotification,
      child: Material(
        color: Colors.transparent,
        child: Align(
          alignment: Alignment.topCenter,
          child: SlideTransition(
            position: _position,
            child: Card(
              margin: EdgeInsets.all(0.0),
              shape: RoundedRectangleBorder(),
              child: Container(
                margin: EdgeInsets.only(top: systemUiPadding),
                height: 100 + systemUiPadding,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        leading: widget.leading,
                        onTap: () async {
                          await dismiss();
                          if (null != widget.onTap) {
                            widget.onTap();
                          }
                        },
                        title: Text(widget.title ?? ''),
                        subtitle: Text(
                          widget.message ?? '',
                          maxLines: 2,
                        ),
                        trailing: null != widget.logo
                            ? CircleAvatar(
                                radius: 24,
                                child: widget.logo,
                                backgroundColor: Colors.grey[50],
                              )
                            : CircleAvatar(
                                radius: 24,
                                backgroundColor: Colors.white,
                                backgroundImage:
                                    AssetImage('assets/images/logo.png'),
                              ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FlatButton(
                              child: Text(
                                "DISMISS",
                                style: TextStyle(color: primaryColor),
                              ),
                              onPressed: dismiss),
                          FlatButton(
                              child: Text("DISMISS ALL",
                                  style: TextStyle(color: primaryColor)),
                              onPressed: () {
                                NewMessageNotificationManager
                                    .dismissAllNewMessageNotifications();
                              })
                        ],
                      )
                    ]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
