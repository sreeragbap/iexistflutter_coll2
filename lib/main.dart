import 'package:flutter/material.dart';
import 'package:iexist/src/common/color_constants.dart';
import 'package:iexist/src/control/printString.dart';
import 'package:iexist/src/provider/appProvider.dart';
import 'package:iexist/src/provider/chatScreenProvider.dart';
import 'package:iexist/src/provider/createNewIssueGroupProvider.dart';
import 'package:iexist/src/provider/userProvider.dart';
import 'package:iexist/src/screens/issuePage/chatScreen/voice_message_player.dart';
import 'package:iexist/src/screens/splashScreen/splashScreen.dart';
import 'package:provider/provider.dart';

void main() async {
  FlutterError.onError = (FlutterErrorDetails errorDetails) async {
    PrintString(errorDetails);
  };

  runApp(
    MultiProvider(
      providers: [
        // User Profile API Response stored here
        ChangeNotifierProvider<UserProvider>.value(value: UserProvider()),
        ChangeNotifierProvider<AppProvider>.value(value: AppProvider()),
        ChangeNotifierProvider<CreateNewIssueGroupProvider>.value(
            value: CreateNewIssueGroupProvider()),
        ChangeNotifierProvider<ChatScreenProvider>.value(
            value: ChatScreenProvider()),
        ChangeNotifierProvider<VoiceMessagePlayer>.value(
            value: VoiceMessagePlayer()),
      ],
      child: MyApp(),
    ),
  );
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'FASTTRACK',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(ColorConstants.PRIMARY_COLOR),
        buttonColor: Color(ColorConstants.BUTTON_COLOR),
        accentColor: Color(ColorConstants.BUTTON_COLOR),
        scaffoldBackgroundColor: Color(ColorConstants.BACKGROUND_COLOR),
        /*visualDensity: VisualDensity.adaptivePlatformDensity,*/
      ),
      // Goes to splash screen
      home: SplashScreen(),
//      routes: {
//        'landingScreen': (context) => LandingScreen(),
//        'loginScreen': (context) => LoginScreen(),
//        'phoneLoginScreen': (context) => PhoneLoginScreen(),
//        'homeScreen': (context) => HomeScreen(),
//        'pinScreen': (context) => PhonePinScreen(),
//      },
    );
  }
}
